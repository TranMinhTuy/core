package ddd.hidden.port_adapter;

import ddd.hidden.Hidden;
import org.bson.Document;

import java.util.Optional;

public interface IHiddenRepository {
    Optional<Hidden> add(Hidden hidden);

    Optional<Hidden> update(String _id, Hidden account);

    Optional<Boolean> delete(String _id);

    Optional<Hidden> getById(String _id);

    Optional<Hidden> getByQuery(Document query);

}
