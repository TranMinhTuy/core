package ddd.hidden.application;

import ddd.common.ultis.WDException;
import ddd.hidden.Hidden;
import ddd.hidden.command.CommandHidden;
import ddd.hidden.port_adapter.IHiddenRepository;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.enumeration.StringContent;
import utils.exception.i18nException;

import java.util.Optional;

@Component
public class HiddenApplication implements IHiddenApplication {

    @Autowired
    private IHiddenRepository hiddenRepository;

    @Override
    public Optional<Hidden> create(CommandHidden commandHidden) throws Throwable {
        if (StringUtils.isAnyBlank(commandHidden.getName())) {
            throw new WDException(i18nException.param_not_valid);
        }
        Boolean status = false;
        if (commandHidden.getStatus() != null) {
            status = commandHidden.getStatus();
        }
        String name = StringContent.removeUnicode(commandHidden.getName());
        Document query = new Document("is_deleted", false).append("name", name);
        Optional<Hidden> hidden = hiddenRepository.getByQuery(query);
        if (hidden.isPresent()) {
            throw new WDException(i18nException.name_exist);
        }
        Hidden hiddenNew = Hidden.builder()
                .name(name)
                .status(status)
                .build();
        return hiddenRepository.add(hiddenNew);
    }

    @Override
    public Optional<Hidden> update(CommandHidden commandHidden) throws Throwable {
        if (StringUtils.isEmpty(commandHidden.getId())) {
            throw new WDException(i18nException.param_not_valid);
        }
        Hidden hidden = new Hidden();
        if (StringUtils.isNotEmpty(commandHidden.getName())) {
            String name = StringContent.removeUnicode(commandHidden.getName());
            Document query = new Document("is_deleted", false).append("name", name)
                    .append("_id", new Document("$ne", new ObjectId(commandHidden.getId())));
            if (hiddenRepository.getByQuery(query).isPresent()) {
                throw new WDException(i18nException.name_exist);
            }
            hidden.setName(name);
        }
        if (hidden.getStatus() != null) {
            hidden.setStatus(commandHidden.getStatus());
        }
        return hiddenRepository.update(commandHidden.getId(), hidden);
    }

    @Override
    public Optional<Hidden> get(String nameApp) throws Throwable {
        if (StringUtils.isEmpty(nameApp)) {
            throw new WDException(i18nException.param_not_valid);
        }
        String name = StringContent.removeUnicode(nameApp);
        Document query = new Document("is_deleted", false).append("name", name);
        return hiddenRepository.getByQuery(query);
    }
}
