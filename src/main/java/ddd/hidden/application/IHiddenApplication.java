package ddd.hidden.application;

import ddd.hidden.Hidden;
import ddd.hidden.command.CommandHidden;

import java.util.Optional;

public interface IHiddenApplication {
    Optional<Hidden> create(CommandHidden commandHidden) throws Throwable;

    Optional<Hidden> update(CommandHidden commandHidden) throws Throwable;

    Optional<Hidden> get(String name) throws Throwable;
}
