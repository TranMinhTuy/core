package ddd.hidden.command;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import eu.dozd.mongo.annotation.Entity;
import eu.dozd.mongo.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class CommandHidden {
    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    ObjectId _id;
    private String id;
    private String name;
    private Boolean status;
    private Boolean deleted;
}
