package ddd.email.central;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import utils.enumeration.EmailSendTypeEnum;
import utils.enumeration.StringType;
import utils.sendgrid.SendGridEmailRequestBody;
import utils.sendgrid.SendGridUtils;

import java.util.ArrayList;
import java.util.List;

@Component
public class EmailCentral implements IEmailCentral {
    private static final Logger LOGGER = LogManager.getLogger();
    private String KEY_SEND = "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";

    @Override
    public void sendEmail(String type, MailMessageInfo mailMessageInfo) {
        switch (type) {
            case StringType.CONFIRM_ORDER:
                sendMailOrder(mailMessageInfo);
                break;
        }
    }

    private void sendMailOrder(MailMessageInfo mailMessageInfo) {
        SendGridEmailRequestBody.Person from = SendGridEmailRequestBody.Person.builder()
                .name(mailMessageInfo.getFrom()).email(mailMessageInfo.getFrom_email()).build();
        List<SendGridEmailRequestBody.Person> to = new ArrayList<>();
        List<SendGridEmailRequestBody.Person> cc = new ArrayList<>();
        List<SendGridEmailRequestBody.Person> bcc = new ArrayList<>();
        for (MailMessageInfo.Receiver receiver : mailMessageInfo.getTo()) {
            SendGridEmailRequestBody.Person person = SendGridEmailRequestBody.Person.builder()
                    .name(receiver.getName())
                    .email(receiver.getAddress())
                    .build();
            switch (receiver.getType()) {
                case EmailSendTypeEnum.to:
                    to.add(person);
                    break;
                case EmailSendTypeEnum.cc:
                    cc.add(person);
                    break;
                case EmailSendTypeEnum.bcc:
                    bcc.add(person);
                    break;
            }
        }
        String response = SendGridUtils.sendEmail(KEY_SEND, from, to, cc, bcc, mailMessageInfo.getSubject(), mailMessageInfo.getContent());
        if (SendGridUtils.SENDGRID_RETRY.equals(response)) {
            if (mailMessageInfo.getRetry() > 3) {

            } else {
                LOGGER.info("sendThroughSendrid_System retrying");
                mailMessageInfo.setRetry(mailMessageInfo.getRetry() + 1);
            }
        } else if (StringUtils.isNotBlank(response)) {
            LOGGER.error("sendThroughSendrid_System error response: " + response);
        }
    }
}
