package ddd.email.central;

public interface IEmailCentral {
    void sendEmail(String type, MailMessageInfo mailMessageInfo);
}
