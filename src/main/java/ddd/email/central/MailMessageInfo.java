package ddd.email.central;

import lombok.*;

import java.util.List;

@Data
@Getter
@Setter
@AllArgsConstructor
@Builder
public class MailMessageInfo {
    private String from;
    private String from_email;
    private String subject;
    private List<Receiver> to;
    private List<String> attachment;
    private String content;
    private AccountTransport accounts_transport;
    private int retry;

//    @Override
//    public String getType() {
//        return this.getClass().getCanonicalName();
//    }

    @Data
    @Getter
    @Setter
    @AllArgsConstructor
    @Builder
    public static class Receiver {
        private String address;
        private String name;
        private String type; // to/cc/bcc

        public Receiver() {
        }
    }

    @Data
    @Getter
    @Setter
    @AllArgsConstructor
    @Builder
    public static class AccountTransport {
        private String account_id;
        @Builder.Default
        private String kind = "smtp"; // smtp/oauth2/third_party

        public AccountTransport() {
        }
    }
}
