package ddd.redis;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import ddd.football.Fixture;
import ddd.football.LeagueY;
import ddd.football.Odd;
import ddd.football.Standing;
import ddd.football.command.ResponseFixture;
import ddd.football.response.ResponseLiveSoccer;
import factory.provider.ObjectMapperProvider;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.KeyRedis;
import utils.TimeEnum;
import utils.http.OkHttpUtils;
import utils.redis.JedisMaster;

import java.util.*;

@Component
public class FootballRedis {
    @Autowired
    private JedisMaster jedisMaster;
    @Autowired
    private ObjectMapperProvider objectMapperProvider;
    @Autowired
    private CommonRedis commonRedis;

    public static int PRETTY_PRINT_INDENT_FACTOR = 4;

    private String URL_FIXTURE = "https://api-football-v1.p.rapidapi.com/v3/fixtures?date=";
    private String URL_ODD = "https://api-football-v1.p.rapidapi.com/v3/odds?date=";
    private String URL_LEAGUE = "https://api-football-v1.p.rapidapi.com/v3/leagues?season=";
    private String URL_STAND = "https://api-football-v1.p.rapidapi.com/v3/standings?league=";
    //    ====================================================||====================================================
    private String URL_LIVE_SOCCER = "https://api-football-v1.p.rapidapi.com/v3/fixtures?live=all";

    public Object getFixtures(String date) throws Throwable {
        String key = KeyRedis.FIXTURE + date;
        String data = jedisMaster.get(key);
        if (StringUtils.isNotEmpty(data)) {
            Fixture fixture = objectMapperProvider.parse(data, Fixture.class);
            if (fixture != null && fixture.getResponse() != null && fixture.getResponse().size() > 0) {
                LinkedHashMap<String, List<Object>> hashMap = new LinkedHashMap<>();
                for (ResponseFixture responseFixture : fixture.getResponse()) {
                    if (!hashMap.containsKey("league_" + responseFixture.getLeague().getId())) {
                        List<Object> list = new ArrayList<>();
                        list.add(responseFixture);
                        hashMap.put("league_" + responseFixture.getLeague().getId(), list);
                    } else {
                        hashMap.get("league_" + responseFixture.getLeague().getId()).add(responseFixture);
                    }
                }
                List<Object> results = new ArrayList<>();
                for (Map.Entry me : hashMap.entrySet()) {
                    HashMap<String, Object> result = new HashMap<>();
                    result.put("league", me.getValue());
                    results.add(result);
                }
                return results;
            }
        }
        String resultLeague = OkHttpUtils.synGetStringFB(URL_FIXTURE + date);
        if (StringUtils.isNotEmpty(resultLeague)) {
            Fixture fixture = JSONObject.parseObject(resultLeague, Fixture.class);
            if (fixture != null) {
                commonRedis.addRedis(key, fixture, TimeEnum.SECOND_OF_ONE_DAY);
            }
            if (fixture != null && fixture.getResponse() != null && fixture.getResponse().size() > 0) {
                LinkedHashMap<String, List<Object>> hashMap = new LinkedHashMap<>();
                for (ResponseFixture responseFixture : fixture.getResponse()) {
                    if (!hashMap.containsKey("league_" + responseFixture.getLeague().getId())) {
                        List<Object> list = new ArrayList<>();
                        list.add(responseFixture);
                        hashMap.put("league_" + responseFixture.getLeague().getId(), list);
                    } else {
                        hashMap.get("league_" + responseFixture.getLeague().getId()).add(responseFixture);
                    }
                }
                List<Object> results = new ArrayList<>();
                for (Map.Entry me : hashMap.entrySet()) {
                    HashMap<String, Object> result = new HashMap<>();
                    result.put("league", me.getValue());
                    results.add(result);
                }
                return results;
            }
        }
        return null;
    }

    public Odd getOdds(String date) throws Throwable {
        String key = KeyRedis.ODD + date;
        String data = jedisMaster.get(key);
        if (StringUtils.isNotEmpty(data)) {
            return objectMapperProvider.parse(data, Odd.class);
        }
        String result = OkHttpUtils.synGetStringFB(URL_ODD + date);
        if (StringUtils.isNotEmpty(result)) {
            Odd fixtureResult = JSONObject.parseObject(result, Odd.class);
            if (fixtureResult != null) {
                commonRedis.addRedis(key, fixtureResult, TimeEnum.SECOND_OF_ONE_DAY);
            }
            return fixtureResult;
        }
        return null;
    }

    public LeagueY getLeagues(String year) throws Throwable {
        String key = KeyRedis.LEAGUE + year;
        String data = jedisMaster.get(key);
        if (StringUtils.isNotEmpty(data)) {
            return objectMapperProvider.parse(data, LeagueY.class);
        }
        String result = OkHttpUtils.synGetStringFB(URL_LEAGUE + year);
        if (StringUtils.isNotEmpty(result)) {
            LeagueY fixtureResult = JSONObject.parseObject(result, LeagueY.class);
            if (fixtureResult != null) {
                commonRedis.addRedis(key, fixtureResult, TimeEnum.SECOND_OF_ONE_DAY);
            }
            return fixtureResult;
        }
        return null;
    }

    public Standing getStanding(String league, String season) throws Throwable {
        String key = KeyRedis.STAND + league + "_" + season;
        String data = jedisMaster.get(key);
        if (StringUtils.isNotEmpty(data)) {
            return objectMapperProvider.parse(data, Standing.class);
        }
        String url = URL_STAND + league + "&season=" + season;
        String result = OkHttpUtils.synGetStringFB(url);
        if (StringUtils.isNotEmpty(result)) {
            Standing fixtureResult = JSONObject.parseObject(result, Standing.class);
            if (fixtureResult != null) {
                commonRedis.addRedis(key, fixtureResult, TimeEnum.SECOND_OF_ONE_DAY);
            }
            return fixtureResult;
        }
        return null;
    }


    public ResponseLiveSoccer getLiveSoccer() {
        try {
            String key = KeyRedis.LIVE_SOCCER;
            String data = jedisMaster.get(key);
            if (StringUtils.isNotEmpty(data)) {
                return objectMapperProvider.parse(data, ResponseLiveSoccer.class);
            }
            String result = OkHttpUtils.synGetStringFB(URL_LIVE_SOCCER);
//            org.json.JSONObject xmlJSONObj = XML.toJSONObject(result);
//            String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
            if (StringUtils.isNotEmpty(result)) {
                ResponseLiveSoccer liveSoccer = JSONObject.parseObject(result, ResponseLiveSoccer.class);
                if (liveSoccer != null) {
                    commonRedis.addRedis(key, liveSoccer, TimeEnum.SECOND_OF_ONE_DAY);
                }
                return liveSoccer;
            }
        } catch (JSONException je) {
            System.out.println(je.toString());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }

    public void updateLiveSoccer() {
        try {
            String key = KeyRedis.LIVE_SOCCER;
            String data = jedisMaster.get(key);
            commonRedis.deleteRedis(key);
            String result = OkHttpUtils.synGetStringFB(URL_LIVE_SOCCER);
            if (StringUtils.isNotEmpty(result)) {
                ResponseLiveSoccer liveSoccer = JSONObject.parseObject(result, ResponseLiveSoccer.class);
                if (liveSoccer != null) {
                    commonRedis.addRedis(key, liveSoccer, TimeEnum.SECOND_OF_ONE_DAY);
                }
            }
        } catch (JSONException je) {
            System.out.println(je.toString());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

}
