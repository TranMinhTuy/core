package ddd.redis;

import ddd.common.ultis.WDException;
import factory.provider.ObjectMapperProvider;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.redis.JedisMaster;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Component
public class CommonRedis {
    @Autowired
    private JedisMaster jedisMaster;
    @Autowired
    private ObjectMapperProvider objectMapperProvider;

    public void addRedis(String key, List<String> data, int time) throws WDException {
        CompletableFuture.runAsync(() -> {
            jedisMaster.setWithExpireAfter(key, objectMapperProvider.writeValueAsString(data), time);
        });
    }

    public void addRedis(String key, Object data, int time) throws WDException {
        CompletableFuture.runAsync(() -> {
            jedisMaster.setWithExpireAfter(key, objectMapperProvider.writeValueAsString(data), time);
        });
    }

    public void addRedis(String key, long data, int time) throws WDException {
        CompletableFuture.runAsync(() -> {
            jedisMaster.setWithExpireAfter(key, objectMapperProvider.writeValueAsString(data), time);
        });
    }

    public void deleteRedis(String key) throws WDException {
        CompletableFuture.runAsync(() -> {
            String data = jedisMaster.get(key);
            if (StringUtils.isNotEmpty(key)) {
                jedisMaster.delete(key);
            }
        });
    }

}
