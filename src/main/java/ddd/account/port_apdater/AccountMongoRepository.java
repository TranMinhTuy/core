package ddd.account.port_apdater;

import ddd.account.Account;
import ioc.bootstrap.configuration.ENVConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.CollectionEnum;
import utils.DbNameEnum;
import utils.mongodb.KijiKokoroMongoDB;
import utils.mongodb.KijoKokoroMongoDBOperator;
import utils.mongodb.MongoDBOperator;
import utils.mongodb.build_query.KijiKokoroReflection;

import java.util.Optional;

@Component
public class AccountMongoRepository implements IAccountRepository {
    private final MongoDBOperator<Account> mongoDBOperator;
    private static Logger LOGGER = LogManager.getLogger();

    @Autowired
    public AccountMongoRepository(ENVConfig applicationConfig) {
        mongoDBOperator = new KijoKokoroMongoDBOperator<>(
                KijiKokoroMongoDB.getInstance(KijiKokoroMongoDB.MongoDBConfigBuilder.config()
                        .withConnectionURL(applicationConfig.getStringProperty(DbNameEnum.KEY))
                        .withDatabaseName(DbNameEnum.KU).build()),
                applicationConfig.getStringProperty(DbNameEnum.KEY),
                DbNameEnum.KU, CollectionEnum.ACCOUNT, Account.class
        );
    }

    @Override
    public Optional<Account> add(Account account) {
        try {
            return Optional.of(mongoDBOperator.insert(account));
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.empty();
        }
    }

    @Override
    public Optional<Boolean> delete(String _id) {
        try {
            Document query = new Document("_id", new ObjectId(_id));
            Document data = new Document("$set", new Document("is_deleted", true));
            return Optional.of(mongoDBOperator.update(query, data).getMatchedCount() == 1 ? true : false);
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.of(false);
        }
    }

    @Override
    public Optional<Boolean> update(String _id, Account account) {
        try {
            Document query = new Document("_id", new ObjectId(_id));
            Document data = KijiKokoroReflection.instance().buildQuerySet(account);
            return Optional.of(mongoDBOperator.update(query, data).getMatchedCount() == 1 ? true : false);
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.of(false);
        }
    }

    @Override
    public Optional<Account> getByQuery(Document query) {
        try {
            return Optional.ofNullable(mongoDBOperator.find(query, new Document(), new Document()));
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.empty();
        }
    }

}
