package ddd.account.port_apdater;

import ddd.account.Account;
import org.bson.Document;

import java.util.Optional;

public interface IAccountRepository {
    Optional<Account> add(Account account);

    Optional<Boolean> delete(String _id);

    Optional<Boolean> update(String _id, Account account);

    Optional<Account> getByQuery(Document query);
}
