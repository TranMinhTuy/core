package ddd.filestorage;

import java.io.InputStream;
import java.util.Map;
import java.util.Optional;

public interface FileStorageRepository {
    Boolean existsBucket(String bucketName);


    void setBucketPolicy(String bucketName, FireStorage.PostPolicy policy);

    void createBucket(String bucketName, String region);


    Map<String, String> setObjectPolicy(String bucketName, String objectId, FireStorage.PostPolicy policy);

    Optional<InputStream> getObject(String bucketName, String objectId);

    Optional<String> getEndPointWithTimeExpired(String bucketName, String objectId, Integer afterTime);

    Optional<Boolean> putObject(String region, String bucketName, String objectId, InputStream inputStream, String contentType);

    Optional<Boolean> removeObject(String region, String bucketName, String objectId);


    Optional<FireStorage.Object> statObject(String bucketName, String objectId);
}
