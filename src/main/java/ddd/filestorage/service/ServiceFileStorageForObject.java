/*
 * Copyright (c) 2019. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package ddd.filestorage.service;

import ddd.filestorage.FileStorageRepository;
import ddd.filestorage.FireStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.Optional;


@Component

public class ServiceFileStorageForObject implements IServiceFileStorageForObject {
    @Autowired
    private FileStorageRepository fileStorageRepository;

    @Override
    public Optional<FireStorage.Object> getDetailObject(String bucketName, String objectId) {
        return fileStorageRepository.statObject(bucketName, objectId);
    }

    @Override
    public Optional<Boolean> putObject(String region, String bucketName, String objectId, InputStream inputStream, String contentType) {
        return fileStorageRepository.putObject(region, bucketName, objectId, inputStream, contentType);
    }

    @Override
    public Optional<Boolean> removeObject(String region, String bucketName, String objectId) {
        return fileStorageRepository.removeObject(region, bucketName, objectId);
    }
}
