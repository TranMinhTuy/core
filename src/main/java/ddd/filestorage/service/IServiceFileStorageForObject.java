/*
 * Copyright (c) 2019. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package ddd.filestorage.service;

import ddd.filestorage.FireStorage;

import java.io.InputStream;
import java.util.Optional;

public interface IServiceFileStorageForObject {

    Optional<FireStorage.Object> getDetailObject(String bucketName, String objectId);

    Optional<Boolean> putObject(String region, String bucketName, String objectId, InputStream inputStream, String contentType);

    Optional<Boolean> removeObject(String region, String bucketName, String objectId);
}
