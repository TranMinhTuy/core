package ddd.filestorage;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import eu.dozd.mongo.annotation.Embedded;
import eu.dozd.mongo.annotation.Entity;
import eu.dozd.mongo.annotation.Id;
import lombok.*;
import org.bson.types.ObjectId;

import java.io.Serializable;
import java.util.Map;

@Data
@Getter
@Setter
@Entity
@AllArgsConstructor
@Builder
public class FireStorage implements Serializable {
    @Id
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId _id;
    private String business_type;
    private String tenant_id;
    private Bucket bucket;
    private Object object_info;

    public FireStorage() {
    }

    @Data
    @Getter
    @Setter
    @AllArgsConstructor
    @Builder
    @Embedded
    public static class Bucket implements Serializable {
        private String name;
        private String region;
        private Long creation_date;
        private Policy policy;

        public Bucket() {
        }

        @Data
        @Getter
        @Setter
        @AllArgsConstructor
        @Builder
        @Embedded
        public static class Policy implements Serializable {
            private String name;
            private String region;
            private Long creation_date;

            public Policy() {
            }
        }

        @Data
        @Getter
        @Setter
        @AllArgsConstructor
        @Builder
        @Embedded
        public static class LifeCycle implements Serializable {
            private String lifecycle;

            public LifeCycle() {
            }
        }
    }

    @Data
    @Getter
    @Setter
    @AllArgsConstructor
    @Builder
    @Embedded
    public static class Object implements Serializable {
        private String bucket_name;
        private String content_type;
        private Long creation_date;
        private String etag;
        private Long length;
        private String name;
        private String file_name;

        public Object() {
        }
    }

    @Data
    @Getter
    @Setter
    @AllArgsConstructor
    @Builder
    @Embedded
    public static class PostPolicy implements Serializable {
        private static final String ALGORITHM = "AWS4-HMAC-SHA256";
        private String bucket_name;
        private String content_encoding;
        private Long content_length;
        private String content_type;
        private Map<String, String> make_form_data;
        private Long expiration_date;

        public PostPolicy() {
        }
    }
}
