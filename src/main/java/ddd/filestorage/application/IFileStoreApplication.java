package ddd.filestorage.application;

import ddd.filestorage.FireStorage;

import java.io.InputStream;
import java.util.Optional;

public interface IFileStoreApplication {
    Optional<FireStorage> put(String region, String bucketName, String business_type, String objectId, InputStream inputStream, String contentType);

    Optional<Boolean> remove(String region, String bucketName, String objectId);


}
