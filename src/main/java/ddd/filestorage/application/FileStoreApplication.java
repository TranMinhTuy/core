package ddd.filestorage.application;

import ddd.filestorage.FireStorage;
import ddd.filestorage.service.IServiceFileStorageForObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.Optional;

@Component
public class FileStoreApplication implements IFileStoreApplication {
    @Autowired
    IServiceFileStorageForObject serviceFileStorageForObject;

    @Override
    public Optional<FireStorage> put(String region, String bucketName, String business_type, String objectId, InputStream inputStream, String contentType) {
        Boolean dsa = serviceFileStorageForObject.putObject(region, bucketName, objectId, inputStream, contentType).orElse(false);
        if (dsa) {
            FireStorage.Object object = serviceFileStorageForObject.getDetailObject(bucketName, objectId).orElse(null);
            FireStorage.Bucket bucket = FireStorage.Bucket.builder()
                    .name(bucketName)
                    .region(region)
                    .build();
            FireStorage fS = FireStorage.builder().tenant_id(bucketName)
                    .business_type(business_type)
                    .bucket(bucket)
                    .object_info(object).build();
            return Optional.of(fS);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Boolean> remove(String region, String bucketName, String objectId) {
        Boolean dsa = serviceFileStorageForObject.removeObject(region, bucketName, objectId).orElse(false);
        if (dsa) {

            return Optional.of(true);
        }
        return Optional.of(false);
    }


}
