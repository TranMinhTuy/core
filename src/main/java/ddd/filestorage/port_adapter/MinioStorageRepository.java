package ddd.filestorage.port_adapter;

import ddd.filestorage.FileStorageRepository;
import ddd.filestorage.FireStorage;
import io.minio.MinioClient;
import io.minio.ObjectStat;
import io.minio.PostPolicy;
import io.minio.errors.*;
import ioc.bootstrap.configuration.ENVConfig;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xmlpull.v1.XmlPullParserException;
import utils.storeage.StorageConfig;
import utils.storeage.StorageConnection;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Slf4j

@Component

public class MinioStorageRepository implements FileStorageRepository {

    ENVConfig applicationConfig;
    MinioClient minioClient;

    @Autowired
    public MinioStorageRepository(ENVConfig applicationConfig) throws InvalidPortException, NoSuchAlgorithmException, InvalidEndpointException {
        this.applicationConfig = applicationConfig;
        this.minioClient = StorageConnection.getConnect(StorageConfig.StorageConfigBuilder.storageConfig()
                .withConnectionURL(applicationConfig.getStringProperty("url_minio", "http://103.27.236.117:9000"))
                .withAccessKey(applicationConfig.getStringProperty("key_minio", "GUR8fw6BYfsTaXCqRCnv"))
                .withSecretKey(applicationConfig.getStringProperty("password_minio", "R5t6GeQ3mMZPSWhAqwQVHAUCAZ5rgr9Am9z9xZnTq"))
                .build()
        );
    }

    @Override
    public Boolean existsBucket(String bucketName) {
        try {
            return minioClient.bucketExists(bucketName);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public void setBucketPolicy(String bucketName, FireStorage.PostPolicy policy) {

    }

    @Override
    public void createBucket(String bucketName, String region) {

    }


    @Override
    public Map<String, String> setObjectPolicy(String bucketName, String objectId, FireStorage.PostPolicy policy) {
        try {
            PostPolicy postPolicy = new PostPolicy(
                    bucketName,
                    objectId,
                    DateTime.now().plusMillis(policy.getExpiration_date().intValue())
            );
            Map<String, String> formData = minioClient.presignedPostPolicy(postPolicy);
            return formData;
        } catch (Throwable e) {
            log.error("setObjectPolicy", e);
        }
        return new HashMap<>();
    }

    @Override
    public Optional<InputStream> getObject(String bucketName, String objectId) {
        try {
            return Optional.of(minioClient.getObject(bucketName, objectId));
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<String> getEndPointWithTimeExpired(String bucketName, String objectId, Integer afterTime) {
        try {
            return Optional.of(minioClient.presignedGetObject(bucketName, objectId, afterTime));
        } catch (InvalidBucketNameException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InsufficientDataException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoResponseException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        } catch (InternalException e) {
            e.printStackTrace();
        } catch (InvalidExpiresRangeException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Boolean> putObject(String region, String bucketName, String objectName, InputStream inputStream, String contentType) {
        try {
            if (!minioClient.bucketExists(bucketName)) {
                minioClient.makeBucket(bucketName, region);
                log.info("Create Bucket {} on region {}", bucketName, region);
            }
            minioClient.putObject(bucketName, objectName, inputStream, contentType);
            return Optional.of(true);
        } catch (InvalidBucketNameException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoResponseException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        } catch (InternalException e) {
            e.printStackTrace();
        } catch (InvalidArgumentException e) {
            e.printStackTrace();
        } catch (InsufficientDataException e) {
            e.printStackTrace();
        } catch (RegionConflictException e) {
            e.printStackTrace();
        }
        return Optional.of(false);
    }

    @Override
    public Optional<Boolean> removeObject(String region, String bucketName, String objectName) {
        try {
            if (!minioClient.bucketExists(bucketName)) {
                minioClient.makeBucket(bucketName, region);
                log.info("Create Bucket {} on region {}", bucketName, region);
            }
            minioClient.removeObject(bucketName, objectName);
            return Optional.of(true);
        } catch (InvalidBucketNameException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoResponseException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        } catch (InternalException e) {
            e.printStackTrace();
        } catch (InvalidArgumentException e) {
            e.printStackTrace();
        } catch (InsufficientDataException e) {
            e.printStackTrace();
        } catch (RegionConflictException e) {
            e.printStackTrace();
        }
        return Optional.of(false);
    }

    @Override
    public Optional<FireStorage.Object> statObject(String bucketName, String objectId) {
        ObjectStat objectStat = null;
        try {
            objectStat = minioClient.statObject(bucketName, objectId);
            FireStorage.Object result = FireStorage.Object.builder()
                    .bucket_name(objectStat.bucketName())
                    .content_type(objectStat.contentType())
                    .creation_date(objectStat.createdTime().getTime())
                    .etag(objectStat.etag())
                    .length(objectStat.length())
                    .name(objectStat.name())
                    .build();
            return Optional.of(result);
        } catch (InvalidBucketNameException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InsufficientDataException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoResponseException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        } catch (InternalException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
