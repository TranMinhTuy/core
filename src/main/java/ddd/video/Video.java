package ddd.video;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import eu.dozd.mongo.annotation.Entity;
import eu.dozd.mongo.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Video {
    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    ObjectId _id;
    @Builder.Default
    private Boolean is_deleted = false;
    private Long created_date;
    private Long last_updated_date;
    private String name;
    private String video_id;
    private String description;
    private String url;
    @JsonIgnore
    private String app_name;
}
