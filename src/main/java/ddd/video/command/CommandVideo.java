package ddd.video.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommandVideo {
    private String _id;
    private String name;
    private String description;
    private Long time;
    private String url;
    private String app_name;
}
