package ddd.video.port_adapter;

import ddd.video.Video;

import java.util.List;
import java.util.Optional;

public interface IVideoRepository {
    Optional<Video> add(Video video);

    Optional<Video> update(String _id, Video video);

    Optional<Boolean> delete(String _id);

    Optional<Video> getById(String _id);

    List<Video> getMany(String appName, Integer page, Integer size);

    long count(String appName);
}
