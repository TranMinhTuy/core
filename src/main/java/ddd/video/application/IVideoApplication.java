package ddd.video.application;

import ddd.video.Video;
import ddd.video.command.CommandVideo;

import java.util.Optional;

public interface IVideoApplication {
    Optional<Video> createVideo(CommandVideo commandVideo) throws Throwable;

    Optional<Video> updateVideo(CommandVideo commandVideo) throws Throwable;

    Optional<Boolean> deleteVideo(String _id) throws Throwable;

    Optional<Object> getVideo(String appName, Integer page, Integer size) throws Throwable;
}
