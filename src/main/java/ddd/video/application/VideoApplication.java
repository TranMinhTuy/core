package ddd.video.application;

import ddd.common.rest.Paginated;
import ddd.common.ultis.WDException;
import ddd.video.Video;
import ddd.video.command.CommandVideo;
import ddd.video.port_adapter.IVideoRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.exception.i18nException;

import java.util.List;
import java.util.Optional;

@Component
public class VideoApplication implements IVideoApplication {
    @Autowired
    private IVideoRepository videoRepository;

    @Override
    public Optional<Video> createVideo(CommandVideo commandVideo) throws Throwable {
        if (StringUtils.isAnyBlank(commandVideo.getApp_name(), commandVideo.getName(), commandVideo.getUrl())) {
            throw new WDException(i18nException.param_not_valid);
        }
        String[] arrays = commandVideo.getUrl().split("=");
        String videoId = null;
        if (arrays != null && arrays.length > 1) {
            videoId = arrays[1];
        }
        
        if (StringUtils.isEmpty(videoId)) {
            throw new WDException(i18nException.error_processing);
        }

        Video video = Video.builder()
                .app_name(commandVideo.getApp_name())
                .description(commandVideo.getDescription())
                .url(commandVideo.getUrl())
                .video_id(videoId)
                .name(commandVideo.getName())
                .created_date(System.currentTimeMillis())
                .last_updated_date(System.currentTimeMillis())
                .build();
        return videoRepository.add(video);
    }

    @Override
    public Optional<Video> updateVideo(CommandVideo commandVideo) throws Throwable {
        return Optional.empty();
    }

    @Override
    public Optional<Boolean> deleteVideo(String _id) throws Throwable {
        if (StringUtils.isEmpty(_id)) {
            throw new WDException(i18nException.param_not_valid);
        }
        return videoRepository.delete(_id);
    }

    @Override
    public Optional<Object> getVideo(String appName, Integer page, Integer size) throws Throwable {
        long total = videoRepository.count(appName);
        List<Video> videos = videoRepository.getMany(appName, page, size);
        return Optional.of(new Paginated<>(videos, page, size, total));
    }
}
