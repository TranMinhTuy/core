package ddd.common.domain.model;

import ddd.common.domain.model.query.PageRequest;
import ddd.common.rest.Paginated;

import java.io.Serializable;
import java.util.List;

/**
 * @param <T>
 * @param <ID>
 */
public interface BaseRepository<T, ID extends Serializable>{
    T getById(ID id);
    List<T> findAllWithOutPaging();
    Paginated<T> findAllWithPagingAndSorting(PageRequest pageRequest);
}

