package ddd.common.port_adapter.persistence.jpa;

import ddd.common.domain.model.BaseRepository;

import java.io.Serializable;

public abstract class JpaBaseRepositoryFactory<T ,ID extends Serializable> implements BaseRepository<T,ID>{

}
