package ddd.common.context;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class VIHATSaaSDataContext {
    private String tenant_id;
    private String agent_id;
    private String full_name;
    private String contact_id;
    private String request_id;
    private List<String> roles;
    public VIHATSaaSDataContext() {
    }
}

