package ddd.common.context;

public class VIHATSaaSContext
{
    public static final ThreadLocal<VIHATSaaSDataContext> thead = new ThreadLocal<VIHATSaaSDataContext>();
    public static void setObject(VIHATSaaSDataContext objectContext){
        thead.set(objectContext);
    }
    public static void unset(){
        thead.remove();
    }
    public static VIHATSaaSDataContext get(){
        return thead.get();
    }
}
