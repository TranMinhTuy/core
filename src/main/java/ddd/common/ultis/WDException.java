package ddd.common.ultis;

public class WDException extends Exception {
    public WDException() {
    }

    public WDException(String s) {
        super(s, null, false, false);
    }

    public WDException(String s, Throwable throwable) {
        super(s, throwable, false, false);
    }

    public WDException(Throwable throwable) {
        super(throwable.getMessage(), throwable, false, false);
    }

    public WDException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
