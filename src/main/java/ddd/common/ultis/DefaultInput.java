package ddd.common.ultis;

public class DefaultInput {
    public static Integer getPage(Integer page) {
        if (page == null || page < 1) {
            page = 1;
        }
        return page;
    }

    public static Integer getSize(Integer size) {
        if (size == null || size <= 0) {
            size = 5;
        } else if (size > 50) {
            size = 50;
        }
        return size;
    }
}
