package ddd.order.application;

import ddd.order.Order;
import ddd.order.comand.CommandOrder;

import java.util.Optional;

public interface IOrderApplication {
    Optional<Order> createOrder(CommandOrder commandOrder) throws Throwable;

    Optional<Order> updateOrder(CommandOrder commandOrder) throws Throwable;

    Optional<Boolean> deleteOrder(String _id) throws Throwable;

    Optional<Order> cancelOrder(String _id) throws Throwable;

    Optional<Object> getOrder(String userId, String status, Integer page, Integer size) throws Throwable;
}
