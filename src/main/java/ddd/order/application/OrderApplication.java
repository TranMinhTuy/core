package ddd.order.application;

import ddd.command.OrderItem;
import ddd.common.ultis.WDException;
import ddd.item.Item;
import ddd.item.port_adapter.IItemRepository;
import ddd.order.Order;
import ddd.order.comand.CommandOrder;
import ddd.order.port_adapter.IOrderRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.enumeration.StringType;
import utils.exception.i18nException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Component
public class OrderApplication implements IOrderApplication {

    @Autowired
    private IOrderRepository orderRepository;
    @Autowired
    private IItemRepository itemRepository;

    private void updateItem(List<OrderItem> items, Boolean add) {
        for (OrderItem orderItem : items) {
            Optional<Item> item = itemRepository.getById(orderItem.getItem_id());
            if (item.isPresent()) {
                Item itemUpdate = new Item();
                itemUpdate.setLast_updated_date(System.currentTimeMillis());
                Integer sold = 0;
                if (item.get().getSold() != null && item.get().getSold() > 0) {
                    sold = item.get().getSold();
                }
                if (add) {
                    sold += orderItem.getQuantity();
                } else {
                    sold -= orderItem.getQuantity();
                }
                if (sold < 0) {
                    sold = 0;
                }
                itemUpdate.setSold(sold);
                itemRepository.update(orderItem.getItem_id(), itemUpdate);
            }
        }
    }

    @Override
    public Optional<Order> createOrder(CommandOrder commandOrder) throws Throwable {
        if (StringUtils.isAnyBlank(commandOrder.getUser_id(), commandOrder.getPayment())) {
            throw new WDException(i18nException.param_not_valid);
        }
        if (commandOrder.getItems() == null || commandOrder.getItems().size() <= 0 || commandOrder.getPayment() == null) {
            throw new WDException(i18nException.param_not_valid);
        }
        List<OrderItem> orderItems = new ArrayList<>();
        double totalGoods = 0, totalDiscount = 0;
        int shippingFee = 0;
        if (commandOrder.getShipping_fee() != null) {
            shippingFee = commandOrder.getShipping_fee();
        }
        for (OrderItem orderItem : commandOrder.getItems()) {
            Optional<Item> itemTemp = itemRepository.getById(orderItem.getItem_id());
            if (itemTemp.isPresent()) {
//                OrderItem or
                Item item = itemTemp.get();
                Integer priceDiscount = 0;
                if (item.getDiscount() != null && item.getDiscount() > 0 && item.getDiscount() < 100) {
                    priceDiscount = item.getPrice() * item.getDiscount() / 100;
                }
                totalDiscount = priceDiscount * orderItem.getQuantity();
                totalGoods = item.getPrice() * orderItem.getQuantity();
                orderItems.add(OrderItem.builder()
                        .discount(item.getDiscount())
                        .price(item.getPrice())
                        .image(item.getImage())
                        .item_id(item.get_id().toHexString())
                        .quantity(orderItem.getQuantity())
                        .name(item.getName())
                        .build());
            }
        }

        Order order = Order.builder()
                .created_date(System.currentTimeMillis())
                .last_updated_date(System.currentTimeMillis())
                .info_user(commandOrder.getInfo_user())
                .payment(commandOrder.getPayment())
                .shipping_fee(shippingFee)
                .status(StringType.WAITING)
                .user_id(commandOrder.getUser_id())
                .items(orderItems)
                .total_discount(totalDiscount)
                .total_price_goods(totalGoods)
                .description(commandOrder.getDescription())
                .total(shippingFee + totalGoods - totalDiscount)
                .build();
        Optional<Order> orderResult = orderRepository.add(order);
        if (orderResult.isPresent()) {
            CompletableFuture.runAsync(() -> {
                updateItem(orderItems, true);
            });
        }
        return orderResult;
    }

    @Override
    public Optional<Order> updateOrder(CommandOrder commandOrder) throws Throwable {
        if (StringUtils.isAnyBlank(commandOrder.get_id(), commandOrder.getStatus())) {
            throw new WDException(i18nException.param_not_valid);
        }
        Optional<Order> orderTemp = orderRepository.getById(commandOrder.get_id());
        if (!orderTemp.isPresent()) {
            throw new WDException(i18nException.not_found_order);
        }
        Order orderUpdate = new Order();
        orderUpdate.setLast_updated_date(System.currentTimeMillis());
        orderUpdate.setStatus(commandOrder.getStatus());
        Optional<Order> result = orderRepository.update(commandOrder.get_id(), orderUpdate);
        if (result.isPresent() && StringType.CANCELED.equals(commandOrder.getStatus())) {
            CompletableFuture.runAsync(() -> {
                updateItem(result.get().getItems(), true);
            });
        }
        return result;
    }

    @Override
    public Optional<Boolean> deleteOrder(String _id) throws Throwable {
        return Optional.empty();
    }

    @Override
    public Optional<Order> cancelOrder(String _id) throws Throwable {
        if (StringUtils.isEmpty(_id)) {
            throw new WDException(i18nException.param_not_valid);
        }
        Optional<Order> orderTemp = orderRepository.getById(_id);
        if (orderTemp.isPresent()) {
            if (StringType.WAITING.equals(orderTemp.get().getStatus())) {
                Order orderUpdate = new Order();
                orderUpdate.setStatus(StringType.CANCELED);
                orderUpdate.setLast_updated_date(System.currentTimeMillis());
                Optional<Order> orderResult = orderRepository.update(_id, orderUpdate);
                if (orderResult.isPresent()) {
                    CompletableFuture.runAsync(() -> {
                        updateItem(orderResult.get().getItems(), false);
                    });
                }
                return orderResult;
            }
            throw new WDException(i18nException.cannot_cancel_order);
        }
        throw new WDException(i18nException.not_found_order);
    }

    @Override
    public Optional<Object> getOrder(String userId, String status, Integer page, Integer size) throws Throwable {
        return Optional.empty();
    }
}
