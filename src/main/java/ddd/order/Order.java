package ddd.order;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import ddd.command.AddressPayment;
import ddd.command.OrderItem;
import eu.dozd.mongo.annotation.Entity;
import eu.dozd.mongo.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Order {
    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    ObjectId _id;
    private Long created_date;
    private Long last_updated_date;
    @Builder.Default
    private Boolean is_deleted = false;

    private String user_id;
    private Integer shipping_fee;
    private Double total;
    private Double total_discount;
    private Double total_price_goods;
    private String status;
    private String description;
    private String payment;
    private List<OrderItem> items;
    private AddressPayment info_user;
}
