package ddd.order.port_adapter;

import ddd.order.Order;
import org.bson.Document;

import java.util.List;
import java.util.Optional;

public interface IOrderRepository {
    Optional<Order> add(Order order);

    Optional<Order> update(String _id, Order order);

    Optional<Boolean> delete(String _id);

    Optional<Order> getById(String _id);

    Optional<Order> getByQuery(Document query);

    long countByQuery(Document query);

    List<Order> getByQuery(Document query, Integer page, Integer size);
}
