package ddd.order.comand;

import ddd.command.AddressPayment;
import ddd.command.OrderItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommandOrder {
    private String _id;
    private String user_id;
    private String description;
    private Integer shipping_fee;
    private Double total;
    private Double total_discount;
    private Double total_price_goods;
    private String status;
    private String payment;
    private List<OrderItem> items;
    private AddressPayment info_user;
}
