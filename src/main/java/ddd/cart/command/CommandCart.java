package ddd.cart.command;

import ddd.command.ItemCart;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommandCart {
    private String _id;
    private Boolean isAll;
    private String user_id;
    private ItemCart cart;
}
