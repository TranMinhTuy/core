package ddd.cart.port_apdater;

import ddd.cart.Cart;
import org.bson.Document;

import java.util.Optional;

public interface ICartRepository {
    Optional<Cart> add(Cart cart);

    Optional<Cart> update(String _id, Cart cart);

    Optional<Boolean> delete(String _id);

    Optional<Cart> getById(String _id);

    Optional<Cart> getByQuery(Document query);

}
