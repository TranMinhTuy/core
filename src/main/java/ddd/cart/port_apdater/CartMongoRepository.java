package ddd.cart.port_apdater;

import ddd.cart.Cart;
import ioc.bootstrap.configuration.ENVConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.CollectionEnum;
import utils.DbNameEnum;
import utils.mongodb.KijiKokoroMongoDB;
import utils.mongodb.KijoKokoroMongoDBOperator;
import utils.mongodb.MongoDBOperator;
import utils.mongodb.build_query.KijiKokoroReflection;

import java.util.Optional;

@Component
public class CartMongoRepository implements ICartRepository {
    private final MongoDBOperator<Cart> mongoDBOperator;
    private static Logger LOGGER = LogManager.getLogger();

    @Autowired
    public CartMongoRepository(ENVConfig applicationConfig) {
        mongoDBOperator = new KijoKokoroMongoDBOperator<>(
                KijiKokoroMongoDB.getInstance(KijiKokoroMongoDB.MongoDBConfigBuilder.config()
                        .withConnectionURL(applicationConfig.getStringProperty(DbNameEnum.KEY))
                        .withDatabaseName(DbNameEnum.KU).build()),
                applicationConfig.getStringProperty(DbNameEnum.KEY),
                DbNameEnum.KU, CollectionEnum.CART, Cart.class);
    }

    @Override
    public Optional<Cart> add(Cart cart) {
        try {
            return Optional.of(mongoDBOperator.insert(cart));
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.empty();
        }
    }

    @Override
    public Optional<Cart> update(String _id, Cart cart) {
        try {
            Document query = new Document("_id", new ObjectId(_id));
            Document data = KijiKokoroReflection.instance().buildQuerySet(cart);
            return Optional.of(mongoDBOperator.findOneAndUpdate(query, data));
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.empty();
        }
    }

    @Override
    public Optional<Boolean> delete(String _id) {
        try {
            Document query = new Document("_id", new ObjectId(_id));
            Document data = new Document("$set", new Document("is_deleted", true));
            return Optional.of(mongoDBOperator.update(query, data).getMatchedCount() == 1 ? true : false);
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.of(false);
        }
    }

    @Override
    public Optional<Cart> getById(String _id) {
        try {
            Document query = new Document("_id", new ObjectId(_id));
            query.append("is_deleted", false);
            return Optional.ofNullable(mongoDBOperator.find(query, new Document(), new Document()));
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.empty();
        }
    }

    @Override
    public Optional<Cart> getByQuery(Document query) {
        try {
            return Optional.ofNullable(mongoDBOperator.find(query, new Document(), new Document()));
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.empty();
        }
    }
}
