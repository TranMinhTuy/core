package ddd.cart.application;

import ddd.cart.Cart;
import ddd.cart.command.CommandCart;
import ddd.cart.port_apdater.ICartRepository;
import ddd.command.ItemCart;
import ddd.common.ultis.WDException;
import ddd.item.Item;
import ddd.item.port_adapter.IItemRepository;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.exception.i18nException;

import java.util.*;

@Component
public class CartApplication implements ICartApplication {
    @Autowired
    private ICartRepository cartRepository;
    @Autowired
    private IItemRepository itemRepository;

    private Object getCart(Cart cart) {
        int totalPrice = 0;
        List<Object> items = new ArrayList<>();
        if (cart.getItems() != null && cart.getItems().size() > 0) {
            for (ItemCart itemCart : cart.getItems()) {
                Optional<Item> item = itemRepository.getById(itemCart.getItem_id());
                if (item.isPresent()) {
                    if (item.get().getDiscount() != null && item.get().getDiscount() > 0 && item.get().getDiscount() < 100) {
                        Integer priceDiscount = (item.get().getDiscount() * item.get().getPrice()) / 100;
                        totalPrice += (item.get().getPrice() - priceDiscount) * itemCart.getQuantity();
                    } else {
                        totalPrice += item.get().getPrice() * itemCart.getQuantity();
                    }
                    HashMap<String, Object> cartItem = new HashMap<>();
                    cartItem.put("image", item.get().getImage());
                    cartItem.put("price", item.get().getPrice());
                    cartItem.put("discount", item.get().getDiscount());
                    cartItem.put("quantity", itemCart.getQuantity());
                    items.add(cartItem);
                }
            }
        }
        HashMap<String, Object> result = new HashMap<>();
        result.put("total", totalPrice);
        result.put("items", items);
        return result;
    }

    private Object getCartEmpty() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("total", 0);
        result.put("items", new ArrayList<>());
        return result;
    }

    @Override
    public Optional<Object> onChangeCart(CommandCart commandCart) throws Throwable {
        if (StringUtils.isAnyBlank(commandCart.getUser_id())) {
            throw new WDException(i18nException.param_not_valid);
        }
        if (commandCart.getCart() == null) {
            throw new WDException(i18nException.param_not_valid);
        }
        Document query = new Document("is_deleted", false).append("user_id", commandCart.getUser_id());
        Optional<Cart> cartTemp = cartRepository.getByQuery(query);
        if (cartTemp.isPresent()) {
            Cart cart = cartTemp.get();

            List<ItemCart> items = new ArrayList<>();
            if (cart.getItems() != null && cart.getItems().size() > 0) {
                for (ItemCart itemCart : cart.getItems()) {
                    if (itemCart.getItem_id().equals(commandCart.getCart().getItem_id())) {
                        items.add(commandCart.getCart());
                    } else {
                        items.add(itemCart);
                    }
                }
            } else {
                items.add(commandCart.getCart());
            }
            Cart cartUpdate = new Cart();
            cartUpdate.setItems(items);
            Optional<Cart> result = cartRepository.update(cart.get_id().toHexString(), cartUpdate);
            if (result.isPresent()) {
                return Optional.of(getCart(result.get()));
            }
        } else {
            Cart cartUser = Cart.builder()
                    .user_id(commandCart.getUser_id())
                    .items(Arrays.asList(commandCart.getCart()))
                    .created_date(System.currentTimeMillis())
                    .last_updated_date(System.currentTimeMillis())
                    .build();
            Optional<Cart> result = cartRepository.add(cartUser);
            if (result.isPresent()) {
                return Optional.of(getCart(result.get()));
            }
        }
        throw new WDException(i18nException.error_processing);
    }

    @Override
    public Optional<Object> deleteCart(CommandCart commandCart) throws Throwable {
        if (commandCart.getIsAll() == null) {
            throw new WDException(i18nException.param_not_valid);
        }
        Document query = new Document("is_deleted", false).append("user_id", commandCart.getUser_id());
        Optional<Cart> cartTemp = cartRepository.getByQuery(query);
        if (!cartTemp.isPresent()) {
            throw new WDException(i18nException.not_found_cart);
        }
        if (commandCart.getIsAll()) {
            Cart cartUpdate = new Cart();
            cartUpdate.setItems(new ArrayList<>());
            cartRepository.update(cartTemp.get().get_id().toHexString(), cartUpdate);
            return Optional.of(getCartEmpty());
        }
        if (cartTemp.get().getItems() != null && cartTemp.get().getItems().size() > 0) {
            List<ItemCart> items = new ArrayList<>();
            for (ItemCart itemCart : cartTemp.get().getItems()) {
                if (!itemCart.getItem_id().equals(commandCart.getCart().getItem_id())) {
                    items.add(itemCart);
                }
            }
            Cart cartUpdate = new Cart();
            cartUpdate.setItems(items);
            Optional<Cart> cartResult = cartRepository.update(cartTemp.get().get_id().toHexString(), cartUpdate);
            if (cartResult.isPresent()) {
                return Optional.of(getCart(cartResult.get()));
            }
        }
        return Optional.of(getCartEmpty());
    }

    @Override
    public Optional<Object> getCart(String userId) throws Throwable {
        Document query = new Document("is_deleted", false).append("user_id", userId);
        Optional<Cart> cartTemp = cartRepository.getByQuery(query);
        if (cartTemp.isPresent() && cartTemp.get().getItems() != null && cartTemp.get().getItems().size() > 0) {
            return Optional.of(getCart(cartTemp.get()));
        }
        return Optional.of(getCartEmpty());
    }
}
