package ddd.cart.application;

import ddd.cart.command.CommandCart;

import java.util.Optional;

public interface ICartApplication {
    Optional<Object> onChangeCart(CommandCart commandCart) throws Throwable;

    Optional<Object> deleteCart(CommandCart commandCart) throws Throwable;

    Optional<Object> getCart(String userId) throws Throwable;
}
