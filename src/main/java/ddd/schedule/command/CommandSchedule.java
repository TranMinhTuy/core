package ddd.schedule.command;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import eu.dozd.mongo.annotation.Entity;
import eu.dozd.mongo.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class CommandSchedule {
    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    ObjectId id;
    private String _id;
    private Boolean is_deleted;
    private Long created_date;
    private Long last_updated_date;
    private String full_name;
    private String user_id;
    private Long time;
    private String email;
    private String phone;
    private String address;
    private String note;
    private Integer number;
}
