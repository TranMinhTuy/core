package ddd.schedule.application;

import ddd.common.rest.Paginated;
import ddd.common.ultis.WDException;
import ddd.schedule.Schedule;
import ddd.schedule.command.CommandSchedule;
import ddd.schedule.port_adapter.IScheduleRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.exception.i18nException;

import java.util.List;
import java.util.Optional;

@Component
public class ScheduleApplication implements IScheduleApplication {

    @Autowired
    private IScheduleRepository scheduleRepository;

    @Override
    public Optional<Schedule> createSchedule(CommandSchedule commandSchedule) throws Throwable {
        if (StringUtils.isAnyBlank(commandSchedule.getAddress(), commandSchedule.getEmail(), commandSchedule.getFull_name(), commandSchedule.getPhone())) {
            throw new WDException(i18nException.param_not_valid);
        }
        if (commandSchedule.getTime() == null || System.currentTimeMillis() > commandSchedule.getTime()) {
            throw new WDException(i18nException.time_invalid);
        }
        Schedule schedule = Schedule.builder()
                .created_date(System.currentTimeMillis())
                .last_updated_date(System.currentTimeMillis())
                .time(commandSchedule.getTime())
                .full_name(commandSchedule.getFull_name())
                .address(commandSchedule.getAddress())
                .email(commandSchedule.getEmail())
                .phone(commandSchedule.getPhone())
                .number(commandSchedule.getNumber())
                .note(commandSchedule.getNote())
                .user_id(commandSchedule.getUser_id())
                .is_deleted(false)
                .build();
        return scheduleRepository.add(schedule);
    }

    @Override
    public Optional<Schedule> updateSchedule(CommandSchedule commandSchedule) throws Throwable {
        if (StringUtils.isEmpty(commandSchedule.get_id())) {
            throw new WDException(i18nException.param_not_valid);
        }

        Schedule scheduleUpdate = new Schedule();
        if (commandSchedule.getTime() != null && commandSchedule.getTime() > System.currentTimeMillis()) {
            scheduleUpdate.setTime(commandSchedule.getTime());
        }
        scheduleUpdate.setLast_updated_date(System.currentTimeMillis());
        if (StringUtils.isNotEmpty(commandSchedule.getFull_name())) {
            scheduleUpdate.setFull_name(commandSchedule.getFull_name());
        }
        if (StringUtils.isNotEmpty(commandSchedule.getNote())) {
            scheduleUpdate.setNote(commandSchedule.getNote());
        }
        if (StringUtils.isNotEmpty(commandSchedule.getEmail())) {
            scheduleUpdate.setEmail(commandSchedule.getEmail());
        }
        if (StringUtils.isNotEmpty(commandSchedule.getPhone())) {
            scheduleUpdate.setPhone(commandSchedule.getPhone());
        }
        if (StringUtils.isNotEmpty(commandSchedule.getAddress())) {
            scheduleUpdate.setAddress(commandSchedule.getAddress());
        }
        if (commandSchedule.getIs_deleted() != null) {
            scheduleUpdate.setIs_deleted(commandSchedule.getIs_deleted());
        }
        if (commandSchedule.getNumber() != null && commandSchedule.getNumber() > 0) {
            scheduleUpdate.setNumber(commandSchedule.getNumber());
        }

        return scheduleRepository.update(commandSchedule.get_id(), scheduleUpdate);
    }

    @Override
    public Optional<Object> getSchedule(String user_id, Integer page, Integer size) throws Throwable {
        if (StringUtils.isEmpty(user_id)) {
            throw new WDException(i18nException.param_not_valid);
        }
        long total = scheduleRepository.count(user_id);
        List<Schedule> schedules = scheduleRepository.getByUser(user_id, page, size);

        return Optional.of(new Paginated<>(schedules, page, size, total));
    }
}
