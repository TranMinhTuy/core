package ddd.schedule.application;

import ddd.schedule.Schedule;
import ddd.schedule.command.CommandSchedule;

import java.util.Optional;

public interface IScheduleApplication {
    Optional<Schedule> createSchedule(CommandSchedule commandSchedule) throws Throwable;

    Optional<Schedule> updateSchedule(CommandSchedule commandSchedule) throws Throwable;

    Optional<Object> getSchedule(String user_id, Integer page, Integer size) throws Throwable;
}
