package ddd.schedule.port_adapter;

import ddd.schedule.Schedule;

import java.util.List;
import java.util.Optional;

public interface IScheduleRepository {
    Optional<Schedule> add(Schedule schedule);

    Optional<Schedule> update(String _id, Schedule schedule);

    Optional<Boolean> delete(String _id);

    Optional<Schedule> getById(String _id);

    List<Schedule> getByUser(String user_id, Integer page, Integer size);

    long count(String user_id);
}
