package ddd.schedule.port_adapter;

import ddd.schedule.Schedule;
import ioc.bootstrap.configuration.ENVConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.CollectionEnum;
import utils.DbNameEnum;
import utils.mongodb.KijiKokoroMongoDB;
import utils.mongodb.KijoKokoroMongoDBOperator;
import utils.mongodb.MongoDBOperator;
import utils.mongodb.build_query.KijiKokoroReflection;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class ScheduleMongoRepository implements IScheduleRepository {
    private final MongoDBOperator<Schedule> mongoDBOperator;
    private static Logger LOGGER = LogManager.getLogger();

    @Autowired
    public ScheduleMongoRepository(ENVConfig applicationConfig) {
        mongoDBOperator = new KijoKokoroMongoDBOperator<>(
                KijiKokoroMongoDB.getInstance(KijiKokoroMongoDB.MongoDBConfigBuilder.config()
                        .withConnectionURL(applicationConfig.getStringProperty(DbNameEnum.KEY))
                        .withDatabaseName(DbNameEnum.KU).build()),
                applicationConfig.getStringProperty(DbNameEnum.KEY),
                DbNameEnum.KU, CollectionEnum.SCHEDULE, Schedule.class);
    }

    @Override
    public Optional<Schedule> add(Schedule schedule) {
        try {
            return Optional.of(mongoDBOperator.insert(schedule));
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.empty();
        }
    }

    @Override
    public Optional<Schedule> update(String _id, Schedule schedule) {
        try {
            Document query = new Document("_id", new ObjectId(_id));
            Document data = KijiKokoroReflection.instance().buildQuerySet(schedule);
            return Optional.of(mongoDBOperator.findOneAndUpdate(query, data));
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.empty();
        }
    }

    @Override
    public Optional<Boolean> delete(String _id) {
        try {
            Document query = new Document("_id", new ObjectId(_id));
            Document data = new Document("$set", new Document("is_deleted", true));
            return Optional.of(mongoDBOperator.update(query, data).getMatchedCount() == 1 ? true : false);
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.of(false);
        }
    }

    @Override
    public Optional<Schedule> getById(String _id) {
        try {
            Document query = new Document("_id", new ObjectId(_id));
            query.append("is_deleted", false);
            return Optional.ofNullable(mongoDBOperator.find(query, new Document(), new Document()));
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.empty();
        }
    }

    @Override
    public List<Schedule> getByUser(String user_id, Integer page, Integer size) {
        try {
            Document query = new Document("user_id", user_id).append("is_deleted", false);
            return mongoDBOperator.findMany(query, new Document("created_date", -1), (page - 1) * size, size);
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return new ArrayList<>();
        }
    }

    @Override
    public long count(String user_id) {
        try {
            Document query = new Document("user_id", user_id).append("is_deleted", false);
            return mongoDBOperator.count(query);
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return 0;
        }
    }
}
