package ddd.branch.port_adapter;

import ddd.branch.Branch;
import org.bson.Document;

import java.util.List;
import java.util.Optional;

public interface IBranchRepository {
    Optional<Branch> add(Branch branch);

    Optional<Branch> update(String _id, Branch branch);

    Optional<Boolean> delete(String _id);

    Optional<Branch> getById(String _id);

    Optional<Branch> getByQuery(Document query);

    long countByQuery(Document query);

    List<Branch> getByQuery(Document query, Integer page, Integer size);
}
