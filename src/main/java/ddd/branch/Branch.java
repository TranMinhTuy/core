package ddd.branch;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import eu.dozd.mongo.annotation.Entity;
import eu.dozd.mongo.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Branch {
    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    ObjectId _id;
    private Long created_date;
    private Long last_updated_date;
    @Builder.Default
    private Boolean is_deleted = false; //true deleted
    @JsonIgnore
    private String app_name;
    @JsonIgnore
    private String name_unsigned;
    private String name;
    private String content;
    private String description;
    private String address;
    private Double latitude;
    private Double longitude;
}
