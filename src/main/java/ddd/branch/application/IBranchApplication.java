package ddd.branch.application;

import ddd.branch.Branch;
import ddd.branch.command.CommandBranch;

import java.util.Optional;

public interface IBranchApplication {
    Optional<Branch> createBranch(CommandBranch commandBranch) throws Throwable;

    Optional<Branch> updateBranch(CommandBranch commandBranch) throws Throwable;

    Optional<Boolean> deleteBranch(String _id) throws Throwable;

    Optional<Object> getBranch(String appName, Integer page, Integer size) throws Throwable;
}
