package ddd.branch.application;

import ddd.branch.Branch;
import ddd.branch.command.CommandBranch;
import ddd.branch.port_adapter.IBranchRepository;
import ddd.common.rest.Paginated;
import ddd.common.ultis.WDException;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.enumeration.StringContent;
import utils.exception.i18nException;

import java.util.List;
import java.util.Optional;

@Component
public class BranchApplication implements IBranchApplication {

    @Autowired
    private IBranchRepository branchRepository;

    @Override
    public Optional<Branch> createBranch(CommandBranch commandBranch) throws Throwable {
        if (StringUtils.isAnyBlank(commandBranch.getApp_name(), commandBranch.getName(), commandBranch.getAddress())) {
            throw new WDException(i18nException.param_not_valid);
        }
        if (commandBranch.getLatitude() == null || commandBranch.getLongitude() == null) {
            throw new WDException(i18nException.param_not_valid);
        }

        String nameUnsigned = StringContent.removeUnicode(commandBranch.getName());
        Branch branch = Branch.builder()
                .address(commandBranch.getAddress())
                .app_name(commandBranch.getApp_name())
                .content(commandBranch.getContent())
                .last_updated_date(System.currentTimeMillis())
                .created_date(System.currentTimeMillis())
                .name(commandBranch.getName())
                .name_unsigned(nameUnsigned)
                .longitude(commandBranch.getLongitude())
                .latitude(commandBranch.getLatitude())
                .description(commandBranch.getDescription())
                .build();
        return branchRepository.add(branch);
    }

    @Override
    public Optional<Branch> updateBranch(CommandBranch commandBranch) throws Throwable {
        if (StringUtils.isEmpty(commandBranch.get_id())) {
            throw new WDException(i18nException.param_not_valid);
        }
        Branch branch = new Branch();
        branch.setName(commandBranch.getName());
        branch.setAddress(commandBranch.getAddress());
        branch.setContent(commandBranch.getContent());
        branch.setName_unsigned(StringContent.removeUnicode(commandBranch.getContent()));
        branch.setLast_updated_date(System.currentTimeMillis());
        branch.setDescription(commandBranch.getDescription());
        return branchRepository.update(commandBranch.get_id(), branch);
    }

    @Override
    public Optional<Boolean> deleteBranch(String _id) throws Throwable {
        if (StringUtils.isEmpty(_id)) {
            throw new WDException(i18nException.param_not_valid);
        }
        return branchRepository.delete(_id);
    }

    @Override
    public Optional<Object> getBranch(String appName, Integer page, Integer size) throws Throwable {
        if (StringUtils.isEmpty(appName)) {
            throw new WDException(i18nException.param_not_valid);
        }
        Document query = new Document("is_deleted", false).append("app_name", appName);
        long total = branchRepository.countByQuery(query);
        List<Branch> branches = branchRepository.getByQuery(query, page, size);
        return Optional.of(new Paginated<>(branches, page, size, total));
    }
}
