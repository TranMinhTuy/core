package ddd.branch.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommandBranch {
    private String _id;
    private String app_name;
    private String name;
    private String content;
    private String description;
    private String address;
    private Double latitude;
    private Double longitude;
}