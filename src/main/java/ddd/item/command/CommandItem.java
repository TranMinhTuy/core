package ddd.item.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommandItem {
    private String _id;
    private String app_name;
    private String name;
    private String evaluate;
    private String description;
    private Integer discount;
    private Integer price;
    private Integer inventory;
    private String original_price;
    private List<String> images;
    private String image;
}
