package ddd.item.port_adapter;

import ddd.item.Item;
import org.bson.Document;

import java.util.List;
import java.util.Optional;

public interface IItemRepository {
    Optional<Item> add(Item item);

    Optional<Item> update(String _id, Item item);

    Optional<Boolean> delete(String _id);

    Optional<Item> getById(String _id);

    Optional<Item> getByQuery(Document query);

    long countByQuery(Document query);

    List<Item> getByQuery(Document query, Integer page, Integer size);
}
