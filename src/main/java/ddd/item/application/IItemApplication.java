package ddd.item.application;

import ddd.item.Item;
import ddd.item.command.CommandItem;

import java.util.Optional;

public interface IItemApplication {
    Optional<Item> createItem(CommandItem commandItem) throws Throwable;

    Optional<Item> updateItem(CommandItem commandItem) throws Throwable;

    Optional<Boolean> deleteItem(String _id) throws Throwable;

    Optional<Object> getItem(String appName, Integer page, Integer size) throws Throwable;
}
