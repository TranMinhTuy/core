package ddd.item.application;

import ddd.common.rest.Paginated;
import ddd.common.ultis.WDException;
import ddd.item.Item;
import ddd.item.command.CommandItem;
import ddd.item.port_adapter.IItemRepository;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.enumeration.StringContent;
import utils.exception.i18nException;

import java.util.List;
import java.util.Optional;

@Component
public class ItemApplication implements IItemApplication {
    @Autowired
    private IItemRepository itemRepository;

    @Override
    public Optional<Item> createItem(CommandItem commandItem) throws Throwable {
        if (StringUtils.isAnyBlank(commandItem.getApp_name(), commandItem.getName(),
                commandItem.getOriginal_price(), commandItem.getImage())) {
            throw new WDException(i18nException.param_not_valid);
        }
        Item item = Item.builder()
                .created_date(System.currentTimeMillis())
                .last_updated_date(System.currentTimeMillis())
                .app_name(commandItem.getApp_name())
                .description(commandItem.getDescription())
                .name(commandItem.getName())
                .name_unsigned(StringContent.removeUnicode(commandItem.getName()))
                .discount(commandItem.getDiscount())
                .image(commandItem.getImage())
                .images(commandItem.getImages())
                .evaluate(0)
                .price(commandItem.getPrice())
                .sold(0)
                .inventory(commandItem.getInventory())
                .build();
        return itemRepository.add(item);
    }

    @Override
    public Optional<Item> updateItem(CommandItem commandItem) throws Throwable {
        return Optional.empty();
    }

    @Override
    public Optional<Boolean> deleteItem(String _id) throws Throwable {
        if (StringUtils.isEmpty(_id)) {
            throw new WDException(i18nException.param_not_valid);
        }
        return itemRepository.delete(_id);
    }

    @Override
    public Optional<Object> getItem(String appName, Integer page, Integer size) throws Throwable {
        if (StringUtils.isEmpty(appName)) {
            throw new WDException(i18nException.param_not_valid);
        }
        Document query = new Document("is_deleted", false).append("app_name", appName);
        long total = itemRepository.countByQuery(query);
        List<Item> items = itemRepository.getByQuery(query, page, size);
        return Optional.of(new Paginated<>(items, page, size, total));
    }
}
