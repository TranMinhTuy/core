package ddd.item;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import eu.dozd.mongo.annotation.Entity;
import eu.dozd.mongo.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Item {
    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    ObjectId _id;
    private Long created_date;
    private Long last_updated_date;
    @Builder.Default
    private Boolean is_deleted = false;
    @JsonIgnore
    private String app_name;
    @JsonIgnore
    private String name_unsigned;
    //
    private String name;
    private double evaluate;
    private String description;
    private Integer discount;
    private Integer price;
    private Integer inventory;
    private Integer sold;
    private List<String> images;
    private String image;

}
