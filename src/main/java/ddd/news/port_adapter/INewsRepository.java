package ddd.news.port_adapter;

import ddd.news.News;
import org.bson.Document;

import java.util.List;
import java.util.Optional;

public interface INewsRepository {
    Optional<News> add(News news);

    Optional<News> update(String _id, News news);

    Optional<Boolean> delete(String _id);

    Optional<News> getById(String _id);

    Optional<News> getByQuery(Document query);

    long countByQuery(Document query);

    List<News> getByQuery(Document query, Integer page, Integer size);
}
