package ddd.news.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommandNews {
    private String _id;
    private String title;
    private String content;
    private String description;
    private Long time;
    private String url;
    private String type;
    private String app_name;
}
