package ddd.news;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import eu.dozd.mongo.annotation.Entity;
import eu.dozd.mongo.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class News {
    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    ObjectId _id;
    @Builder.Default
    private Boolean is_deleted = false;
    private Long created_date;
    private Long last_updated_date;

    private String title;
    private String content;
    private String description;
    private Long time;
    private String url;
    private String type;

    @JsonIgnore
    private String app_name;
}
