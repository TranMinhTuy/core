package ddd.news.application;

import ddd.common.rest.Paginated;
import ddd.common.ultis.WDException;
import ddd.news.News;
import ddd.news.command.CommandNews;
import ddd.news.port_adapter.INewsRepository;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.exception.i18nException;

import java.util.List;
import java.util.Optional;

@Component
public class NewsApplication implements INewsApplication {
    @Autowired
    private INewsRepository newsRepository;

    @Override
    public Optional<News> createNews(CommandNews commandNews) throws Throwable {
        if (StringUtils.isAnyBlank(commandNews.getContent(), commandNews.getTitle(), commandNews.getApp_name(), commandNews.getType())) {
            throw new WDException(i18nException.param_not_valid);
        }
        News news = News.builder()
                .title(commandNews.getTitle())
                .created_date(System.currentTimeMillis())
                .last_updated_date(System.currentTimeMillis())
                .content(commandNews.getContent())
                .description(commandNews.getDescription())
                .url(commandNews.getUrl())
                .time(commandNews.getTime())
                .app_name(commandNews.getApp_name())
                .type(commandNews.getType())
                .build();
        return newsRepository.add(news);
    }

    @Override
    public Optional<News> updateNews(CommandNews commandNews) throws Throwable {
        if (StringUtils.isEmpty(commandNews.get_id())) {
            throw new WDException(i18nException.param_not_valid);
        }
        Optional<News> newsTemp = newsRepository.getById(commandNews.get_id());
        if (!newsTemp.isPresent()) {
            throw new WDException(i18nException.not_found_news);
        }
        News newsUpdate = new News();

        newsUpdate.setLast_updated_date(System.currentTimeMillis());
        newsUpdate.setTime(commandNews.getTime());
        newsUpdate.setTitle(commandNews.getTitle());
        newsUpdate.setContent(commandNews.getContent());
        newsUpdate.setDescription(commandNews.getDescription());
        newsUpdate.setUrl(commandNews.getUrl());

        return newsRepository.update(commandNews.get_id(), newsUpdate);

    }

    @Override
    public Optional<Boolean> deleteNews(String _id) throws Throwable {
        if (StringUtils.isNotEmpty(_id)) {
            throw new WDException(i18nException.param_not_valid);
        }
        return newsRepository.delete(_id);
    }

    @Override
    public Optional<Object> getNews(Integer page, Integer size) throws Throwable {
        Document query = new Document("is_deleted", false);
        long total = newsRepository.countByQuery(query);
        List<News> newsList = newsRepository.getByQuery(query, page, size);
        return Optional.of(new Paginated<>(newsList, page, size, total));
    }


}
