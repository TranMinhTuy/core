package ddd.news.application;

import ddd.news.News;
import ddd.news.command.CommandNews;

import java.util.Optional;

public interface INewsApplication {
    Optional<News> createNews(CommandNews commandNews) throws Throwable;

    Optional<News> updateNews(CommandNews commandNews) throws Throwable;

    Optional<Boolean> deleteNews(String _id) throws Throwable;

    Optional<Object> getNews(Integer page, Integer size) throws Throwable;

}
