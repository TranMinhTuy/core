package ddd.evaluate.command;

import ddd.command.Feedback;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommandEvaluate {
    private String object_id;
    private Integer evaluate;
    private String content;
    private String user_id;
    private List<String> images;
    private Feedback feedback;
}
