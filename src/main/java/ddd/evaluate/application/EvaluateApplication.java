package ddd.evaluate.application;

import ddd.common.rest.Paginated;
import ddd.common.ultis.WDException;
import ddd.evaluate.Evaluate;
import ddd.evaluate.command.CommandEvaluate;
import ddd.evaluate.port_adapter.IEvaluateRepository;
import ddd.item.port_adapter.IItemRepository;
import ddd.users.User;
import ddd.users.port_adapter.IUserRepository;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.exception.i18nException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Component
public class EvaluateApplication implements IEvaluateApplication {
    @Autowired
    private IEvaluateRepository evaluateRepository;
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private IItemRepository itemRepository;

    private void updateEvaluateItem(String item_id) {
        Document query = new Document("is_deleted", false).append("object_id", item_id);
        long total = evaluateRepository.countByQuery(query);
    }

    @Override
    public Optional<Evaluate> createEvaluate(CommandEvaluate commandEvaluate) throws Throwable {
        if (StringUtils.isAnyBlank(commandEvaluate.getObject_id(), commandEvaluate.getUser_id())) {
            throw new WDException(i18nException.param_not_valid);
        }
        if (StringUtils.isEmpty(commandEvaluate.getContent()) && (commandEvaluate.getImages() == null || commandEvaluate.getImages().size() <= 0)) {
            throw new WDException(i18nException.param_not_valid);
        }
        Evaluate evaluate = Evaluate.builder()
                .created_date(System.currentTimeMillis())
                .last_updated_date(System.currentTimeMillis())
                .content(commandEvaluate.getContent())
                .user_id(commandEvaluate.getUser_id())
                .object_id(commandEvaluate.getObject_id())
                .evaluate(commandEvaluate.getEvaluate())
                .images(commandEvaluate.getImages())
                .build();
        return evaluateRepository.add(evaluate);
    }

    @Override
    public Optional<Evaluate> updateEvaluate(CommandEvaluate commandEvaluate) throws Throwable {
        return Optional.empty();
    }

    @Override
    public Optional<Boolean> deleteEvaluate(String _id) throws Throwable {
        return Optional.empty();
    }

    @Override
    public Optional<Object> getEvaluate(String objectId, Integer page, Integer size) throws Throwable {
        Document query = new Document("is_deleted", false).append("object_id", objectId);
        long total = evaluateRepository.countByQuery(query);
        List<Evaluate> evaluates = evaluateRepository.getByQuery(query, page, size);
        List<Object> results = new ArrayList<>();
        if (evaluates != null && evaluates.size() > 0) {
            for (Evaluate evaluate : evaluates) {

                HashMap<String, Object> result = new HashMap<>();
                result.put("content", evaluate.getContent());
                result.put("created_date", evaluate.getCreated_date());
                result.put("images", evaluate.getImages());
                result.put("evaluate", evaluate.getEvaluate());
                result.put("feedback", evaluate.getFeedback());
                Optional<User> userTemp = userRepository.getById(evaluate.getUser_id());
                HashMap<String, Object> infoUser = new HashMap<>();
                infoUser.put("_id", null);
                infoUser.put("avatar", null);
                infoUser.put("fullName", null);
                if (userTemp.isPresent()) {
                    infoUser.put("_id", userTemp.get().get_id());
                    infoUser.put("avatar", userTemp.get().getAvatar());
                    infoUser.put("fullName", userTemp.get().getFullName());
                }
                result.put("info_user", infoUser);
                results.add(result);
            }
        }
        return Optional.of(new Paginated<>(results, page, size, total));
    }
}
