package ddd.evaluate.application;

import ddd.evaluate.Evaluate;
import ddd.evaluate.command.CommandEvaluate;

import java.util.Optional;

public interface IEvaluateApplication {
    Optional<Evaluate> createEvaluate(CommandEvaluate commandEvaluate) throws Throwable;

    Optional<Evaluate> updateEvaluate(CommandEvaluate commandEvaluate) throws Throwable;

    Optional<Boolean> deleteEvaluate(String _id) throws Throwable;

    Optional<Object> getEvaluate(String objectId, Integer page, Integer size) throws Throwable;
}
