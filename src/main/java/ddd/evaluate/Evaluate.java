package ddd.evaluate;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import ddd.command.Feedback;
import eu.dozd.mongo.annotation.Entity;
import eu.dozd.mongo.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Evaluate {
    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    ObjectId _id;
    private Long created_date;
    private Long last_updated_date;
    @Builder.Default
    private Boolean is_deleted = false;

    private String object_id;
    private String user_id;
    private Integer evaluate;
    private String content;
    private List<String> images;
    private Feedback feedback;
}
