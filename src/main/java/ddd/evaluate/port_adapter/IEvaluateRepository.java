package ddd.evaluate.port_adapter;

import ddd.evaluate.Evaluate;
import org.bson.Document;

import java.util.List;
import java.util.Optional;

public interface IEvaluateRepository {
    Optional<Evaluate> add(Evaluate evaluate);

    Optional<Evaluate> update(String _id, Evaluate evaluate);

    Optional<Boolean> delete(String _id);

    Optional<Evaluate> getById(String _id);

    Optional<Evaluate> getByQuery(Document query);

    long countByQuery(Document query);

    List<Evaluate> getByQuery(Document query, Integer page, Integer size);
}
