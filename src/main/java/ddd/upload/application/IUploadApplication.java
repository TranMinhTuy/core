package ddd.upload.application;

import ddd.upload.command.CommandFileUpload;

import java.util.List;
import java.util.Optional;

public interface IUploadApplication {
    Optional<Object> uploadFile(List<CommandFileUpload> fileStores, Boolean isImage) throws Exception;
}
