package ddd.upload.application;

import ddd.common.ultis.WDException;
import ddd.upload.Upload;
import ddd.upload.command.CommandFileUpload;
import org.springframework.stereotype.Component;
import utils.exception.i18nException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UploadApplication implements IUploadApplication {
    public static boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public Optional<Object> uploadFile(List<CommandFileUpload> fileStores, Boolean isImage) throws Exception {
        if (fileStores == null || fileStores.size() <= 0) {
            throw new WDException(i18nException.param_not_valid);
        }
        List<Object> result = new ArrayList<>();
        int index = 0;
        for (CommandFileUpload commandFileStore : fileStores) {
            Integer size = Math.toIntExact(commandFileStore.getSize());
            Integer name = index;
            if (isNumeric(commandFileStore.getName())) {
                name = Integer.parseInt(commandFileStore.getName());
            }
            Upload uploadFile = Upload.builder()
                    .created_date(System.currentTimeMillis())
                    .file_name(commandFileStore.getFileName())
                    .index(name)
                    .is_active(true)
                    .type_info(commandFileStore.getType_info())
                    .type(commandFileStore.getType())
                    .url(commandFileStore.getUrl())
                    .size(size)
                    .build();
            result.add(uploadFile);
            index++;
        }
        if (result.size() == 1) {
            return Optional.of(result.get(0));
        }
        return Optional.of(result);
    }
}
