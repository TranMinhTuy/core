package ddd.upload.command;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import eu.dozd.mongo.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommandFileUpload {
    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    ObjectId _id;
    private String url;
    private String name;
    private String fileName;
    private String type;
    private String type_info;
    private Long size;
    private Integer index;
    private Integer width;
    private Integer height;

}
