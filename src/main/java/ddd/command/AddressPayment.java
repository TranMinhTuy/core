package ddd.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddressPayment {
    private String _id;
    private String phone;
    private String name;
    private AddressType province;
    private AddressType ward;
    private String address;
}
