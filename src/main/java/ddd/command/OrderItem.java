package ddd.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderItem {
    private String item_id;
    private String name;
    private String image;
    private Integer price;
    private Integer discount;
    private Integer quantity;
}
