package ddd.football.application;

import ddd.football.LeagueY;
import ddd.football.Odd;
import ddd.football.Standing;
import ddd.redis.FootballRedis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.TimeUtils;

import java.util.Optional;

@Component
public class FootballApplication implements IFootballApplication {

    private String URL = "https://api-football-v1.p.rapidapi.com/v3/fixtures?date=";
    @Autowired
    private FootballRedis footballRedis;

    @Override
    public Optional<Object> getFixtures() throws Throwable {
        String date = TimeUtils.convertTimeToString(System.currentTimeMillis());
        return Optional.ofNullable(footballRedis.getFixtures(date));
    }

    @Override
    public Optional<Odd> getOdds() throws Throwable {
        String date = TimeUtils.convertTimeToString(System.currentTimeMillis());
        return Optional.ofNullable(footballRedis.getOdds(date));
    }

    @Override
    public Optional<LeagueY> getLeagues() throws Throwable {
        String year = TimeUtils.getYear(System.currentTimeMillis());
        return Optional.ofNullable(footballRedis.getLeagues(year));
    }

    @Override
    public Optional<Standing> getStanding(String league, String season) throws Throwable {
        return Optional.ofNullable(footballRedis.getStanding(league, season));
    }

    @Override
    public Optional<Object> getLiveSoccer() throws Throwable {
        return Optional.ofNullable(footballRedis.getLiveSoccer());
    }

}
