package ddd.football.application;

import ddd.football.LeagueY;
import ddd.football.Odd;
import ddd.football.Standing;

import java.util.Optional;

public interface IFootballApplication {
    Optional<Object> getFixtures() throws Throwable;

    Optional<Odd> getOdds() throws Throwable;

    Optional<LeagueY> getLeagues() throws Throwable;

    Optional<Standing> getStanding(String league, String season) throws Throwable;

    Optional<Object> getLiveSoccer() throws Throwable;
}
