package ddd.football.command;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import eu.dozd.mongo.annotation.Embedded;
import eu.dozd.mongo.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embedded
@Builder
public class ResponseOdd {
    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    ObjectId _id;
    private InfoFixture fixture;
    private League league;
    private List<Bookmaker> bookmakers;
}
