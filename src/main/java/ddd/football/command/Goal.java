package ddd.football.command;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import eu.dozd.mongo.annotation.Embedded;
import eu.dozd.mongo.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embedded
@Builder
public class Goal {
    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    ObjectId _id;
    private Integer id;
    private Integer home;
    private Integer away;
}
