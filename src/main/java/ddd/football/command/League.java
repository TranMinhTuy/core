package ddd.football.command;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import eu.dozd.mongo.annotation.Entity;
import eu.dozd.mongo.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class League {
    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    ObjectId _id;
    private Integer id;
    private String name;
    private String country;
    private String logo;
    private String flag;
    private String type;
    private Integer season;
    private String round;
    private List<List<Stand>> standings;
}
