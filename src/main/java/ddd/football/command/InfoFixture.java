package ddd.football.command;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import eu.dozd.mongo.annotation.Entity;
import eu.dozd.mongo.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class InfoFixture {
    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    ObjectId _id;
    private Integer id;
    private String referee;
    private String timezone;
    private String date;
    private Long timestamp;
    private Period periods;
    private Venue venue;
    private Status status;
}
