package ddd.users.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommandUser {
    private String _id;

    private String avatar;
    private String cover;
    private String phone;
    private String user_id;
    private String url_id;
    private String kind;
    private String email;
    private String password;
    private String current_pass;
    private String username;
    private String fullName;
    private String address;
    private Integer gender;

}
