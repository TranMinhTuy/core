package ddd.users.application;

import ddd.users.User;
import ddd.users.authentication.LoginToken;
import ddd.users.command.CommandUser;

import java.util.Optional;

public interface IUserApplication {

    Optional<LoginToken> login(CommandUser commandUser) throws Exception;

    Optional<LoginToken> register(CommandUser commandUser) throws Exception;

    Optional<User> info(String user) throws Exception;

    Optional<LoginToken> refreshToken(String access_token, String refresh_token) throws Exception;

    Optional<Boolean> updatePassword(CommandUser commandUser) throws Exception;

    Optional<User> updateInfo(CommandUser commandUser) throws Exception;
}
