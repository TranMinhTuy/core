package ddd.users.application;

import ddd.account.Account;
import ddd.account.port_apdater.IAccountRepository;
import ddd.common.ultis.WDException;
import ddd.users.User;
import ddd.users.authentication.JWTTokenData;
import ddd.users.authentication.LoginToken;
import ddd.users.authentication.VihatAuthJWTAuth;
import ddd.users.command.CommandUser;
import ddd.users.port_adapter.IUserRepository;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.crypto.SHA512;
import utils.enumeration.AuthenticationKind;
import utils.exception.i18nException;

import java.util.Optional;

@Component
public class UserApplication implements IUserApplication {

    @Autowired
    private IAccountRepository accountRepository;
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private VihatAuthJWTAuth vihatAuthJWTAuth;


    @Override
    public Optional<LoginToken> login(CommandUser commandUser) throws Exception {
        if (StringUtils.isAnyBlank(commandUser.getKind(), commandUser.getPassword(), commandUser.getEmail())) {
            throw new WDException(i18nException.param_not_valid);
        }
        Document queryAccount = new Document("identity", commandUser.getEmail()).append("is_deleted", false);
        Optional<Account> account = accountRepository.getByQuery(queryAccount);
        if (!account.isPresent()) {
            throw new WDException(i18nException.email_not_registered);
        }
        Document query = new Document("email", commandUser.getEmail()).append("is_deleted", false);
        Optional<User> user = userRepository.getByQuery(query);
        if (!account.isPresent()) {
            throw new WDException(i18nException.email_not_registered);
        }
        if (SHA512.valueOf(commandUser.getPassword()).equals(account.get().getPassword())) {
            return vihatAuthJWTAuth.createLoginToken(JWTTokenData.builder()
                    .user_id(user.get().get_id().toHexString())
                    .build());
        }
        throw new WDException(i18nException.password_incorrect);
    }

    @Override
    public Optional<LoginToken> register(CommandUser commandUser) throws Exception {
        if (StringUtils.isAnyBlank(commandUser.getPassword(), commandUser.getEmail(),
                commandUser.getUsername(), commandUser.getPhone())) {
            throw new WDException(i18nException.param_not_valid);
        }
        Document query = new Document("email", commandUser.getEmail()).append("is_deleted", false);
        Optional<User> user = userRepository.getByQuery(query);
        if (user.isPresent()) {
            throw new WDException(i18nException.email_registered);
        }
        User userNew = User.builder()
                .created_date(System.currentTimeMillis())
                .email(commandUser.getEmail())
                .username(commandUser.getUsername())
                .phone(commandUser.getPhone())
                .fullName(commandUser.getUsername())
                .is_deleted(false)
                .build();
        Optional<User> userResult = userRepository.add(userNew);
        if (userResult.isPresent()) {
            Account account = Account.builder()
                    .kind(AuthenticationKind.INTERNAL)
                    .identity(commandUser.getEmail())
                    .password(SHA512.valueOf(commandUser.getPassword()))
                    .build();
            accountRepository.add(account);
            return vihatAuthJWTAuth.createLoginToken(JWTTokenData.builder()
                    .user_id(userResult.get().get_id().toHexString())
                    .build());
        }
        throw new WDException(i18nException.error_processing);
    }

    @Override
    public Optional<User> info(String user_id) throws Exception {
        if (StringUtils.isAllBlank(user_id)) {
            throw new WDException(i18nException.param_not_valid);
        }
        Document query = new Document("_id", new ObjectId(user_id)).append("is_deleted", false);
        Optional<User> user = userRepository.getByQuery(query);
        if (user.isPresent()) {
            return user;
        }
        throw new WDException(i18nException.not_found_user);
    }

    @Override
    public Optional<LoginToken> refreshToken(String access_token, String refresh_token) throws Exception {
        Optional<LoginToken> loginToken = vihatAuthJWTAuth.refreshToken(access_token, refresh_token);
        return loginToken;
    }

    @Override
    public Optional<Boolean> updatePassword(CommandUser commandUser) throws Exception {
        if (StringUtils.isAnyBlank(commandUser.getUser_id(), commandUser.getPassword(), commandUser.getCurrent_pass())) {
            throw new WDException(i18nException.param_not_valid);
        }

        Document query = new Document("_id", new ObjectId(commandUser.getUser_id())).append("is_deleted", false);
        Optional<User> user = userRepository.getByQuery(query);
        if (!user.isPresent()) {
            throw new WDException(i18nException.not_found_user);
        }
        Document queryAccount = new Document("identity", user.get().getEmail()).append("is_deleted", false);
        Optional<Account> account = accountRepository.getByQuery(queryAccount);
        if (!account.isPresent()) {
            throw new WDException(i18nException.email_not_registered);
        }
        if (SHA512.valueOf(commandUser.getCurrent_pass()).equals(account.get().getPassword())) {
            Account accountUpdate = new Account();
            accountUpdate.setPassword(SHA512.valueOf(commandUser.getPassword()));
            return accountRepository.update(account.get().get_id().toHexString(), accountUpdate);
        }
        throw new WDException(i18nException.password_incorrect);
    }

    @Override
    public Optional<User> updateInfo(CommandUser commandUser) throws Exception {
        if (StringUtils.isAnyBlank(commandUser.getUser_id())) {
            throw new WDException(i18nException.param_not_valid);
        }
        User userUpdate = new User();
        userUpdate.setAvatar(commandUser.getAvatar());
        userUpdate.setPhone(commandUser.getPhone());
        userUpdate.setUsername(commandUser.getUsername());
        userUpdate.setFullName(commandUser.getFullName());
        userUpdate.setAddress(commandUser.getAddress());
        userUpdate.setLast_updated_date(System.currentTimeMillis());
        userUpdate.setGender(commandUser.getGender());
        return userRepository.update(commandUser.getUser_id(), userUpdate);
    }

}
