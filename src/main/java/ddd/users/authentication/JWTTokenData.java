package ddd.users.authentication;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class JWTTokenData {
    String user_id;
    String type;
    Boolean logged_partner;
}
