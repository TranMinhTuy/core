/*
 * Copyright  2019. VIHAT
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  NOTICE: THIS FILE HAS BEEN MODIFIED BY <MY EXAMPLE COMPANY> UNDER COMPLIANCE WITH THE APACHE 2.0 LICENCE FROM THE ORIGINAL WORK
 *  OF THE COMPANY <EXAMPLE PREVIOUS COMPANY>. THE FOLLOWING IS THE COPYRIGHT OF THE ORIGINAL DOCUMENT:
 *
 *  Copyright 2016 Example Previous Company
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  Contributors:
 *
 */

package ddd.users.authentication;

import ddd.common.ultis.WDException;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jwt.JWTOptions;
import ioc.bootstrap.configuration.ENVConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.Base64Convert;
import utils.data.RandomUtils;
import utils.vertx.agent_authen.JWTAuthProvider;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

@Component
public class VihatAuthJWTAuth {
    private static Logger LOGGER = LogManager.getLogger();
    @Autowired
    JWTAuthProvider jwtAuthProvider;
    @Autowired
    private ENVConfig applicationConfig;
    @Autowired
    private RandomUtils randomUtils;

    private String generateToken(JsonObject data) {
        return jwtAuthProvider.getJwtAuth().generateToken(data
                .put("sub", "widdy")
                .put("exp", System.currentTimeMillis() / 1000 + 24 * 3600) // after 24 hour
                .put("iat", System.currentTimeMillis() / 1000), new JWTOptions().setAlgorithm("RS256"));
    }

    public Optional<LoginToken> createLoginToken(JWTTokenData jwtTokenData) {
        try {
            String tokenBase64 = Base64Convert.objectToBase64(jwtTokenData);
            String accessToken = generateToken(new JsonObject()
                    .put("user_id", jwtTokenData.getUser_id())
                    .put("type", jwtTokenData.getType())
                    .put("logged_partner", jwtTokenData.getLogged_partner())
                    .put("soft_key", randomUtils.randomString("mix", 16))
            );
            StringBuilder refreshToken = new StringBuilder();
            refreshToken.append(applicationConfig.getStringProperty("application.auth.domain"));
            refreshToken.append("/user/refresh_token?access_token=");
            refreshToken.append(accessToken);
            refreshToken.append("&refresh_token=");
            refreshToken.append(URLEncoder.encode(tokenBase64, StandardCharsets.UTF_8.name()));
            return Optional.of(LoginToken.builder()
                    .refresh_token(refreshToken.toString())
                    .access_token(accessToken)
                    .build());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            LOGGER.info(throwable.getMessage());
            return Optional.empty();
        }
    }

    public Optional<LoginToken> refreshToken(String accessToken, String refreshToken) throws WDException {
        JWTTokenData jwtRefreshTokenData = Base64Convert.base64ToObject(refreshToken, JWTTokenData.class);
        return createLoginToken(JWTTokenData.builder()
                .user_id(jwtRefreshTokenData.getUser_id())
                .build());
    }

    private Boolean checkToken(String accessToken) {
        Promise<Boolean> promise = Promise.promise();
        jwtAuthProvider.getJwtAuth().authenticate(new JsonObject().put("jwt", accessToken), result -> {
                    if (result.succeeded()) {
                        promise.complete(true);
                    } else {
                        promise.complete(false);
                        LOGGER.error(result.cause());
                    }
                }
        );
        return promise.future().result();
    }
}
