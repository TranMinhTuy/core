package ddd.users.port_adapter;


import ddd.users.User;
import ioc.bootstrap.configuration.ENVConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.CollectionEnum;
import utils.DbNameEnum;
import utils.mongodb.KijiKokoroMongoDB;
import utils.mongodb.KijoKokoroMongoDBOperator;
import utils.mongodb.MongoDBOperator;
import utils.mongodb.build_query.KijiKokoroReflection;

import java.util.Optional;

@Component
public class UserMongoRepository implements IUserRepository {
    private final MongoDBOperator<User> mongoDBOperator;
    private static Logger LOGGER = LogManager.getLogger();

    @Autowired
    public UserMongoRepository(ENVConfig applicationConfig) {
        mongoDBOperator = new KijoKokoroMongoDBOperator<>(
                KijiKokoroMongoDB.getInstance(KijiKokoroMongoDB.MongoDBConfigBuilder.config()
                        .withConnectionURL(applicationConfig.getStringProperty(DbNameEnum.KEY))
                        .withDatabaseName(DbNameEnum.KU).build()),
                applicationConfig.getStringProperty(DbNameEnum.KEY),
                DbNameEnum.KU, CollectionEnum.USER, User.class
        );
    }

    @Override
    public Optional<User> add(User user) {
        try {
            return Optional.of(mongoDBOperator.insert(user));
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> update(String _id, User user) {
        try {
            Document query = new Document("_id", new ObjectId(_id));
            Document data = KijiKokoroReflection.instance().buildQuerySet(user);
            return Optional.of(mongoDBOperator.findOneAndUpdate(query, data));
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> getById(String _id) {
        try {
            Document query = new Document("_id", new ObjectId(_id));
            query.append("is_deleted", false);
            return Optional.ofNullable(mongoDBOperator.find(query, new Document(), new Document()));
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> getByQuery(Document query) {
        try {
            User user = mongoDBOperator.find(query, new Document(), new Document());
            return Optional.ofNullable(user);
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
            return Optional.empty();
        }
    }

}

