package ddd.users.port_adapter;

import ddd.users.User;
import org.bson.Document;

import java.util.Optional;

public interface IUserRepository {
    Optional<User> add(User user);

    Optional<User> update(String _id, User user);

    Optional<User> getById(String _id);

    Optional<User> getByQuery(Document query);
}
