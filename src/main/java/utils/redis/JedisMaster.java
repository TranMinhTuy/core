//package utils.redis;
//
//import ioc.bootstrap.configuration.ENVConfig;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import redis.clients.jedis.Jedis;
//import redis.clients.jedis.JedisPool;
//import redis.clients.jedis.JedisPoolConfig;
//
//import java.net.URI;
//import java.util.Set;
//
//@Component
//public class JedisMaster {
//    private static Logger LOGGER = LogManager.getLogger();
//    private static int PORT = 6379;
//    private static int TIMEOUT = 5 * 1000;
//    private static String PASS = "bxFj5pNyhY49XVfXby";
//
//    @Autowired
//    private ENVConfig envConfig;
//
//    //    private static final JedisPool POOL_INSTANCE = new JedisPool(getConfig(), HOST, PORT, TIMEOUT, PASS);
//    private JedisPool POOL_INSTANCE;
//
//    private JedisMaster() {
//    }
//
//    private JedisPool getJedisPool() {
//        if (POOL_INSTANCE == null) {
//            // block so other threads cannot come into while initialize
//            synchronized (JedisPool.class) {
//                // recheck again. maybe another thread has initialized before
//                if (POOL_INSTANCE == null) {
//                    String redisEnv = envConfig.getStringProperty("application.redis");
//                    URI uri = URI.create(redisEnv);
//                    String HOST_REDIS = envConfig.getStringProperty("application.redis_host");
//                    System.out.println(HOST_REDIS);
//                    POOL_INSTANCE = new JedisPool(getConfig(), HOST_REDIS, PORT, TIMEOUT, PASS);
//                }
//            }
//        }
//        return POOL_INSTANCE;
//    }
//
//    private static JedisPoolConfig getConfig() {
//        JedisPoolConfig poolConfig = new JedisPoolConfig();
//        poolConfig.setMaxTotal(10); // maximum active connections
//        poolConfig.setMaxIdle(10);  // maximum idle connections
//        return poolConfig;
//    }
//
//    public String get(String key) {
//        JedisPool pool = getJedisPool();
//        try (Jedis jedis = pool.getResource()) {
//            return jedis.get(key);
//        } catch (Exception ex) {
//            LOGGER.warn("get exception", ex);
//            throw ex;
//        }
//    }
//
//    public byte[] getByte(byte[] key) {
//        JedisPool pool = getJedisPool();
//        try (Jedis jedis = pool.getResource()) {
//            return jedis.get(key);
//        } catch (Exception ex) {
//            LOGGER.warn("get exception", ex);
//            throw ex;
//        }
//    }
//
//    public String set(String key, String value) {
//        JedisPool pool = getJedisPool();
//        try (Jedis jedis = pool.getResource()) {
//            return jedis.set(key, value);
//        } catch (Exception ex) {
//            LOGGER.warn("get exception", ex);
//            throw ex;
//        }
//    }
//
//    public void setWithExpireAfter(String key, String value, int expireSecond) {
//        JedisPool pool = getJedisPool();
//        try (Jedis jedis = pool.getResource()) {
//            jedis.set(key, value);
//            if (expireSecond > 0) {
//                jedis.expire(key, expireSecond);
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    public String setWithExpire(String key, String value, int expireSecond) {
//        JedisPool pool = getJedisPool();
//        try (Jedis jedis = pool.getResource()) {
//            String result = jedis.set(key, value);
//            if (expireSecond > 0) {
//                jedis.expire(key, expireSecond);
//            }
//            return result;
//        } catch (Exception ex) {
//            LOGGER.warn("get exception", ex);
//            throw ex;
//        }
//    }
//
//    public void deleteByPattern(String pattern) {
//        JedisPool pool = getJedisPool();
//        try (Jedis jedis = pool.getResource()) {
//            Set<String> keys = jedis.keys(pattern);
//            for (String key : keys) {
//                jedis.del(key);
//            }
//        } catch (Exception ex) {
//            LOGGER.warn("get exception", ex);
//            throw ex;
//        }
//    }
//
//    public void delete(String key) {
//        JedisPool pool = getJedisPool();
//        try (Jedis jedis = pool.getResource()) {
//            jedis.del(key);
//        } catch (Exception ex) {
//            LOGGER.warn("get exception", ex);
//            throw ex;
//        }
//    }
//
//    public String setByte(byte key[], byte value[]) {
//        JedisPool pool = getJedisPool();
//        try (Jedis jedis = pool.getResource()) {
//            return jedis.set(key, value);
//        } catch (Exception ex) {
//            LOGGER.warn("get exception", ex);
//            throw ex;
//        }
//    }
//
//    public void delByte(byte key[]) {
//        JedisPool pool = getJedisPool();
//        try (Jedis jedis = pool.getResource()) {
//            jedis.del(key);
//        } catch (Exception ex) {
//            LOGGER.warn("get exception", ex);
//            throw ex;
//        }
//    }
//
//    public void publish(String channel, String message) {
//        JedisPool pool = getJedisPool();
//        try (Jedis jedis = pool.getResource()) {
//            jedis.publish(channel, message);
//        } catch (Exception ex) {
//            throw ex;
//        }
//    }
//}

package utils.redis;

import ioc.bootstrap.configuration.ENVConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import utils.enumeration.ApplicationProperties;
import utils.enumeration.TimeEnum;

import java.net.URI;
import java.util.List;
import java.util.Set;

@Component
public class JedisMaster {
    //	private static final int TIMEOUT = 20 * 1000;
//	private static final int MAX_TOTAL = 10;
//	private static final int MAX_IDLE = 10;
    private static final int TIMEOUT = 30 * 1000;
    private static final int MAX_TOTAL = 300;
    private static final int MAX_IDLE = 20;

    public static final int DBINDEX_CAMPAIGN = 1;
    public static final int DBINDEX_MEETING = 2;

    @Autowired
    private ENVConfig envConfig;

    private JedisPool POOL_INSTANCE;

    private JedisMaster() {
    }

    private JedisPool getJedisPool() {
        if (POOL_INSTANCE != null) {
            return POOL_INSTANCE;
        }
        synchronized (JedisPool.class) {
            if (POOL_INSTANCE != null) {
                return POOL_INSTANCE;
            }
            String redisEnv = envConfig.getStringProperty(ApplicationProperties.REDIS);
            URI uri = URI.create(redisEnv);
            POOL_INSTANCE = new JedisPool(getConfig(), uri, TIMEOUT);
        }
        return POOL_INSTANCE;
    }

    private static JedisPoolConfig getConfig() {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(MAX_TOTAL); // maximum active connections
        poolConfig.setMaxIdle(MAX_IDLE); // maximum idle connections
        return poolConfig;
    }

    public String get(String key) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            return jedis.get(key);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public String get(String key, int dbIndex) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            jedis.select(dbIndex);
            return jedis.get(key);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public Boolean exists(String key) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            return jedis.exists(key);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public byte[] getByte(byte[] key) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            return jedis.get(key);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String set(String key, String value) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            return jedis.set(key, value);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String set(String key, String value, int dbIndex) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            jedis.select(dbIndex);
            return jedis.set(key, value);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public Long hset(String key, String field, String value) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            return jedis.hset(key, field, value);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String hget(String key, String field) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            return jedis.hget(key, field);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List<String> hvals(String key) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            return jedis.hvals(key);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String setWithoutExpire(String key, String value) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            return jedis.set(key, value);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public Boolean limitCall(String key, Long ccu, int expire) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            Long value = jedis.incr(key);
            if (value > ccu) {
                return false;
            }
            if (value == 1) {
                jedis.expire(key, expire);
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public Long lpush(String key, String value) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            Long result = jedis.lpush(key, value);
            jedis.expire(key, TimeEnum.SECOND_OF_ONE_MONTH);
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String rpop(String key) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            return jedis.rpop(key);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void setWithExpire(String key, String value, int unixTime) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            jedis.set(key, value);
            if (unixTime > 0) {
                jedis.expireAt(key, unixTime);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setWithExpireAfter(String key, String value, int expireSecond) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            jedis.set(key, value);
            if (expireSecond > 0) {
                jedis.expire(key, expireSecond);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setWithExpireAfter(String key, String value, int expireSecond, int dbIndex) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            jedis.select(dbIndex);
            jedis.set(key, value);
            if (expireSecond > 0) {
                jedis.expire(key, expireSecond);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void deleteByPattern(String pattern) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            Set<String> keys = jedis.keys(pattern);
            for (String key : keys) {
                jedis.del(key);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Set<String> getByPattern(String pattern) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            return jedis.keys(pattern);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public Set<String> getByPattern(String pattern, int dbIndex) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            jedis.select(dbIndex);
            return jedis.keys(pattern);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void delete(String key) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            jedis.del(key);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void delete(String key, int dbIndex) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            jedis.select(dbIndex);
            jedis.del(key);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String setByte(byte[] key, byte[] value) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            return jedis.set(key, value);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void setByteWithExpire(byte[] key, byte[] value, int expireSecond) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            jedis.set(key, value);
            if (expireSecond > 0) {
                jedis.expire(key, expireSecond);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void delByte(byte[] key) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            jedis.del(key);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void publish(String channel, String message) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            jedis.publish(channel, message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
