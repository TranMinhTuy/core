package utils.redis;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Component
public class ProviderRedisMaster {
    private static Logger LOGGER = LogManager.getLogger();
    private static String HOST = "125.212.247.31";
    private static int PORT = 7778;
    private static int TIMEOUT = 5 * 1000;
    private static final JedisPool POOL_INSTANCE = new JedisPool(getConfig(), HOST, PORT, TIMEOUT);

    private ProviderRedisMaster() {
    }

    private static class SingletonHelper {
        static final ProviderRedisMaster INSTANCE = new ProviderRedisMaster();
    }

    public static ProviderRedisMaster getInstance() {
        return ProviderRedisMaster.SingletonHelper.INSTANCE;
    }

    private JedisPool getJedisPool() {
        return POOL_INSTANCE;
    }

    private static JedisPoolConfig getConfig() {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(10); // maximum active connections
        poolConfig.setMaxIdle(10);  // maximum idle connections
        return poolConfig;
    }

    public String get(String key) {
        JedisPool pool = getJedisPool();
        try (Jedis jedis = pool.getResource()) {
            return jedis.get(key);
        } catch (Exception ex) {
            LOGGER.warn("get exception", ex);
            throw ex;
        }
    }
}
