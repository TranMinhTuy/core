package utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class TextResource {
    private static Logger LOGGER = LogManager.getLogger();

    /**
     * @param localeCode : en, vi. Default en
     * @return keyword if no object for the given key can be found
     */
    public static String getText(String keyword, String localeCode) {
        if (StringUtils.isBlank(keyword)) return keyword;
        Locale locale;
        ResourceBundle exampleBundle;
        switch (localeCode) {
            case "vi":
                locale = new Locale("vi", "VN");
                break;
            default:
                locale = Locale.US;
                break;
        }
//        if (locale.getLanguage().equals(Locale.US.getLanguage())) {
        exampleBundle = ResourceBundle.getBundle("language", locale);
//        } else {
//            exampleBundle = ResourceBundle.getBundle("language", locale, new UTF8Control());
//        }
        try {
            return exampleBundle.getString(keyword);
//            return new String(exampleBundle.getString(keyword).getBytes("UTF-8"));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return keyword;
        }
    }

//    public static String removeUnicode(String str) {
//        str = str.toLowerCase();
//        String temp = Normalizer.normalize(str, Normalizer.Form.NFD);
//        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
//        temp = temp.replaceAll("đ", "d");
//        return pattern.matcher(temp).replaceAll("");
//    }

    public static class UTF8Control extends ResourceBundle.Control {
        public ResourceBundle newBundle
                (String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
                throws IOException {
            // The below is a copy of the default implementation.
            String bundleName = toBundleName(baseName, locale);
            String resourceName = toResourceName(bundleName, "properties");
            ResourceBundle bundle = null;
            InputStream stream = null;
            if (reload) {
                URL url = loader.getResource(resourceName);
                if (url != null) {
                    URLConnection connection = url.openConnection();
                    if (connection != null) {
                        connection.setUseCaches(false);
                        stream = connection.getInputStream();
                    }
                }
            } else {
                stream = loader.getResourceAsStream(resourceName);
            }
            if (stream != null) {
                try {
                    // Only this line is changed to make it to read properties files as UTF-8.
                    bundle = new PropertyResourceBundle(new InputStreamReader(stream, StandardCharsets.UTF_8));
                } finally {
                    stream.close();
                }
            }
            return bundle;
        }
    }

}
