package utils.validator;

import org.apache.commons.validator.routines.EmailValidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OMIEmailValidator extends EmailValidator {
    private static final OMIEmailValidator OMI_EMAIL_VALIDATOR = new OMIEmailValidator(false);
    private static final Pattern MATCH_ASCII_PATTERN = Pattern.compile("^\\p{ASCII}+$");
    private static final Pattern EMAIL_PATTERN = Pattern.compile("^\\s*?(.+)@(.+?)\\s*$");

    public static OMIEmailValidator getInstance() {
        return OMI_EMAIL_VALIDATOR;
    }

    protected OMIEmailValidator(boolean allowLocal) {
        super(allowLocal);
    }

    @Override
    public boolean isValid(String email) {
        if (email == null) {
            return false;
        } else {
            Matcher asciiMatcher = MATCH_ASCII_PATTERN.matcher(email);
            if (!asciiMatcher.matches()) {
                return false;
            } else {
                Matcher emailMatcher = EMAIL_PATTERN.matcher(email);
                if (!emailMatcher.matches()) {
                    return false;
                } else if (email.endsWith(".")) {
                    return false;
                } else if (!this.isValidUser(emailMatcher.group(1))) {
                    return false;
                }
                return true;
            }
        }
    }
}
