package utils;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;

@Component
public class JavaMailUtils {

    /**
     * @param properties
     * @param to
     * @param cc
     * @param bcc
     * @param subject
     * @param body
     * @param sendFrom
     * @param username
     * @param password
     */
    public void send(Properties properties, List<String> to, List<String> cc,
                     List<String> bcc, String subject, String body, String sendFrom,
                     String username, String password) {
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        try {
            Message message = createMessage(session, to, cc, bcc, subject, body, sendFrom);
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public void send(String recipient, String subject, String body, String username, String password) {
        Session session = Session.getInstance(getProperties(), new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            Message message = createMessage(session, recipient, subject, body);
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    private Message createMessage(Session session, List<String> to, List<String> cc,
                                  List<String> bcc, String subject, String body, String sendFrom)
            throws MessagingException {
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(sendFrom));
        InternetAddress[] TOaddresses = to.stream().map(r -> {
            try {
                return new InternetAddress(r);
            } catch (AddressException e) {
                e.printStackTrace();
            }
            return null;
        }).filter(Objects::nonNull).toArray(InternetAddress[]::new);
        message.setRecipients(Message.RecipientType.TO, TOaddresses);
        if (CollectionUtils.isNotEmpty(cc)) {
            List<InternetAddress> CCaddresses = cc.stream().map(r -> {
                try {
                    return new InternetAddress(r);
                } catch (AddressException e) {
                    e.printStackTrace();
                }
                return null;
            }).filter(Objects::nonNull).collect(Collectors.toList());
            CCaddresses.forEach(a -> {
                try {
                    message.addRecipient(Message.RecipientType.CC, a);
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            });
        }
        if (CollectionUtils.isNotEmpty(bcc)) {
            List<InternetAddress> BCCaddresses = bcc.stream().map(r -> {
                try {
                    return new InternetAddress(r);
                } catch (AddressException e) {
                    e.printStackTrace();
                }
                return null;
            }).filter(Objects::nonNull).collect(Collectors.toList());
            BCCaddresses.forEach(a -> {
                try {
                    message.addRecipient(Message.RecipientType.CC, a);
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            });
        }
        message.setSubject(subject);
        message.setText(body, "utf-8", "html");
        return message;
    }

    private Message createMessage(Session session, String recipient, String subject, String body) throws MessagingException {
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress("An example <thatsme@yourhost.com>"));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
        message.setSubject(subject);
        message.setText(body, "utf-8", "html");
        return message;
    }

    private Properties getProperties() {
        Properties props = new Properties();
        props.put("mail.smtp.host", "YOUR_SMTP_EMAIL_HOST");
        props.put("mail.smtp.auth", "false");
        props.put("mail.smtp.starttls.enable", "false");
        props.put("mail.smtp.port", "25");
        return props;
    }

    private Properties getOAuth2Properties(String username, String oauth2_access_token) throws MessagingException {
        Properties props = new Properties();
        props.put("mail.imap.ssl.enable", "true"); // required for Gmail
        props.put("mail.imap.auth.mechanisms", "XOAUTH2");
        Session session = Session.getInstance(props);
        Store store = session.getStore("imap");
        store.connect("imap.gmail.com", username, oauth2_access_token);
        return props;
    }
}
