package utils.mq.amqp;

public interface Worker {
    void subscribe(String message);
}
