package utils;

import com.lowagie.text.*;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class PdfUtils {

    private void writeTableHeader(PdfPTable table, List<String> titles, Integer fontSize) throws IOException {
        PdfPCell cell = new PdfPCell();
        cell.setPadding(5);
        BaseFont unicode = BaseFont.createFont("/fonts/vuArialBold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font BaseFontUniCodeFont = new Font(unicode, fontSize, Font.NORMAL);
        for (String title : titles) {
            cell.setPhrase(new Phrase(title, BaseFontUniCodeFont));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);

            table.addCell(cell);
        }
    }

    public void writeTitlePdf(Document document, String title) throws IOException {
        BaseFont unicode = BaseFont.createFont("/fonts/vuArialBold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font BaseFontUniCodeFont = new Font(unicode, 16, Font.NORMAL);
        Paragraph p = new Paragraph(title, BaseFontUniCodeFont);
        p.setAlignment(Paragraph.ALIGN_CENTER);
        document.add(p);
    }


    public void writeContentPdf(Document document, String title, Integer fontSize, Integer alignment) throws IOException {
        BaseFont unicode = BaseFont.createFont("/fonts/vuArial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font BaseFontUniCodeFont = new Font(unicode, fontSize, Font.NORMAL);
        Paragraph p = new Paragraph(title, BaseFontUniCodeFont);
        if (alignment != null) {
            p.setAlignment(alignment);
        }
        document.add(p);
    }

    public void writeTableHeaderPdf(PdfPTable table, Float withPercentage, float widths[], Integer spacing, List<String> titles, Integer fontSize) throws IOException {
        table.setWidthPercentage(withPercentage);
        table.setWidths(widths);
        table.setSpacingBefore(spacing);
        writeTableHeader(table, titles, fontSize);
    }

    public void writeContentTablePdf(PdfPTable table, List<String> contents, Integer fontSize) throws IOException {
        PdfPCell cell = new PdfPCell();
        cell.setPadding(5);
        BaseFont unicode = BaseFont.createFont("/fonts/vuArial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font BaseFontUniCodeFont = new Font(unicode, fontSize, Font.NORMAL);
        for (String content : contents) {
            cell.setPhrase(new Phrase(content, BaseFontUniCodeFont));
            table.addCell(cell);
        }
    }
}
