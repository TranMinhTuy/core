package utils;

import ioc.bootstrap.configuration.ENVConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MapUtils {
    @Autowired
    private ENVConfig envConfig;

    public String search(String keyword, Double lat, Double lon) {
        StringBuilder stringBuilder = new StringBuilder();
        String url = "https://maps.googleapis.com/maps/api/place/textsearch/json?";
        stringBuilder.append(url);
        if (StringUtils.isNotEmpty(keyword)) {
            stringBuilder.append("query=");
            stringBuilder.append(keyword.trim().replace(" ", "+"));
        }
        //key api map
//        stringBuilder.append("&components=country:vn");
        stringBuilder.append("&language=vi");
        stringBuilder.append("&location=");
        stringBuilder.append(lat + "," + lon);
        stringBuilder.append("&key=");
        stringBuilder.append(envConfig.getStringProperty("key_google"));
        return stringBuilder.toString();
    }

    public String searchNearLocation(String type, Double lat, Double lon) {
        StringBuilder stringBuilder = new StringBuilder();
        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";//json kieu tra ve
        stringBuilder.append(url);
        //lat
        stringBuilder.append("&location=");
        stringBuilder.append(lat + "," + lon);
//        stringBuilder.append("&radius=");
//        stringBuilder.append(1500);
        //
        stringBuilder.append("&language:vi");
        //key api map
        stringBuilder.append("&type=");
        stringBuilder.append(type);
        //key api map
        stringBuilder.append("&rankby=distance");
        //key api map
        stringBuilder.append("&key=");
        stringBuilder.append(envConfig.getStringProperty("key_google"));
        return stringBuilder.toString();
    }

    public String getInfoByLocation(Double lat, Double lon) {
        StringBuilder stringBuilder = new StringBuilder();
        String url = "https://maps.googleapis.com/maps/api/geocode/json?";
        stringBuilder.append(url);
        stringBuilder.append("latlng=");
        stringBuilder.append(lat + "," + lon);
        stringBuilder.append("&key=");
        stringBuilder.append(envConfig.getStringProperty("key_google"));
        return stringBuilder.toString();
    }

    public String getDirections(Double origin_lat, Double origin_lng, Double destination_lat, Double destination_lng) {
        StringBuilder stringBuilder = new StringBuilder();
        String url = "https://maps.googleapis.com/maps/api/directions/json?";
        stringBuilder.append(url);
        stringBuilder.append("origin=");
        stringBuilder.append(origin_lat + "," + origin_lng);
        stringBuilder.append("&destination=");
        stringBuilder.append(destination_lat + "," + destination_lng);
        stringBuilder.append("&language=vi");
        stringBuilder.append("&key=");
        stringBuilder.append(envConfig.getStringProperty("key_google"));
        return stringBuilder.toString();
    }

    public String infoUserLocation(Double lat, Double lng) {
        StringBuilder stringBuilder = new StringBuilder();
        String url = "https://maps.googleapis.com/maps/api/geocode/json?";
        stringBuilder.append(url);
        stringBuilder.append("latlng=");
        stringBuilder.append(lat + "," + lng);
        stringBuilder.append("&sensor=true/false");
        stringBuilder.append("&key=");
        stringBuilder.append(envConfig.getStringProperty("key_google"));
        return stringBuilder.toString();
    }
}
