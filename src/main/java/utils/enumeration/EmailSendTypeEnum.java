package utils.enumeration;

public class EmailSendTypeEnum {
    public static final String to = "to";
    public static final String cc = "cc";
    public static final String bcc = "bcc";
}
