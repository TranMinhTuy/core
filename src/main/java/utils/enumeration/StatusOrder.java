package utils.enumeration;

public class StatusOrder {
    public static final String CANCELED = "-1";//HỦY ĐƠN HÀNG
    public static final String ZERO = "0";// CHỜ DUYỆT
    public static final String ONE = "1";//CHƯA TIẾP NHẬN
    public static final String TWO = "2";//ĐÃ TIẾP NHẬN CHƯA LẤY
    public static final String THREE = "3";//CHỜ LẤY HÀNG
    public static final String FOUR = "4";//ĐANG GIAO
    public static final String FIVE = "5";//ĐÃ GIAO
    public static final String SEVEN = "7";//CHỜ LẤY HÀNG
    public static final String EIGHT = "8";//CHỜ LẤY HÀNG
    public static final String NINE = "9";//ĐANG GIAO
    public static final String TEN = "10";//ĐANG GIAO
    public static final String TWELVE = "12";//CHỜ LẤY HÀNG
    public static final String TWENTY = "20";//TRẢ HÀNG
    public static final String TWENTY_ONE = "21";//TRẢ HÀNG

    public static String contentOrderByStatus(String status) {
        if (status.equals("-1")) {
            return "Đơn hàng #{order} đã được hủy bởi nhà bán hàng";
        }
        if (status.equals("5")) {
            return "Kiện hàng [{partner_id}] của đơn hàng #{order} đã giao thành công. Cho chúng tối biết đánh giá của bạn về sản phẩm nhé";
        }
        if (status.equals("2") || status.equals("1")) {
            return "Đơn hàng #{order} đã được xác nhận";
        }
        if (status.equals("3") || status.equals("7") || status.equals("8") || status.equals("12")) {
            return "Kiện hàng [{partner_id}] của đơn hàng #{order} đã được người bán giao cho đơn vị vận chuyển";
        }
        if (status.equals("4") || status.equals("9") || status.equals("10")) {
            return "Kiện hàng [{partner_id}] của đơn hàng #{order} đang được giao";
        }
        return "Đơn hàng #{order} của bạn đang được xử lý";
    }

    public static String titleOrderByStatus(String status) {
        if (status.equals("-1")) {
            return "Đã hủy";
        }
        if (status.equals("5")) {
            return "Đã giao";
        }
        if (status.equals("2") || status.equals("1")) {
            return "Đã xác nhận";
        }
        if (status.equals("3") || status.equals("7") || status.equals("8") || status.equals("12")) {
            return "Chờ lấy hàng";
        }
        if (status.equals("4") || status.equals("9") || status.equals("10")) {
            return "Đang giao";
        }
        return "Đang xử lý";
    }

}
