package utils.enumeration;

public class TransactionActionType {
    public static final String ADD = "add";
    public static final String SUBTRACT = "subtract";
}
