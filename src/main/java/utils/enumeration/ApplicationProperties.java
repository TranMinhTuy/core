package utils.enumeration;

public class ApplicationProperties {
    public static final String MONGODB = "application.mongodb";
    public static final String REDIS = "application.redis";
    public static final String RABBITMQ_USER = "rabbitmq.user";
    public static final String RABBITMQ_PASSWORD = "rabbitmq.password";
    public static final String RABBITMQ_ADDRESSES = "rabbitmq.addresses";
}