package utils.enumeration;

public class TenantStatusEnum {

    public static final String ACTIVATED = "activated";
    public static final String INACTIVATED = "inactivated";
}
