package utils.enumeration;

public class EmailTypeEnum {
    public static final String ASSIGN_SIP = "assign";
    public static final String REVOKE_SIP = "revoke";
    public static final String REMIND_EXPIRE = "remind";
}
