package utils.enumeration;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import io.vertx.core.MultiMap;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.springframework.stereotype.Component;

import java.text.Normalizer;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

@Component
public class StringContent {

    // content push notification
    //accept friend
    public static String AGREE_FRIEND = " đã chấp nhận lời mời kết bạn của bạn.";
    public static String NEW_MESSAGE = " gửi tới nhóm của bạn";
    //add point user report
    public static String REPORT_RIGHT = "Bạn vừa được cộng điểm sau khi gửi đúng cảnh báo";
    public static String CONFIRM_RIGHT = "Bạn vừa được cộng điểm sau khi xác nhận đúng cảnh báo";

    public static String CONFIRM_ACTIVE = "Mọi người đã xác nhận phản hồi của bạn là chính xác";
    public static String CONFIRM_INACTIVE = "Mọi người đã xác nhận phản hồi của bạn không chính xác";

    public static String ADD_POINT_FIRST_REPORT = "Cộng điểm khi phản hồi đúng đầu tiên ";
    public static String ADD_POINT_FIRST_REPORT_WEEK = "Cộng điểm khi phản hồi đúng đầu tiên trong tuần ";

    public static String CONFIRM_CHANGE_OIL = "Bạn đã đến hạn thay nhớt định kỳ. Bạn đã thay nhớt chưa";
    public static String CONFIRM_MAINTENANCE = "Bạn đã đến hạn bảo dưỡng xe định kỳ. Bạn đã đi bảo dưỡng xe chưa";
    public static String TITLE_CREATE_ORDER = "Cám ơn quý khách {fullName} đã đặt hàng tại Widdy!";
    public static String TITLE_SHIPMENT_ORDER = "Đơn hàng đã sẵn sàng giao đến quý khách !";

    public static String CONTENT_CREATE_ORDER = "Widdy rất vui thông báo đến quý khách đơn hàng #{code} của quý khách đã được tiếp nhận và đang trong quá trình xử lý.";
    public static String CONTENT_SHIPMENT_ORDER = "Widdy rất vui thông báo đơn hàng #{code} của quý khách đã sẵn sàng giao đến quý khách. Quý khách vui lòng bật chuông và lưu ý điện thoại để nhận cuộc gọi khi shipper đến giao hàng.";

    public static String SUBJECT_CREATE_ORDER = "Thông báo xác nhận đơn hàng";
    public static String SUBJECT_SHIPMENT_ORDER = "Cập nhật thông tin giao hàng cho đơn hàng #{code}";

    public static String DESCRIPTION_CREATE_ORDER = "Widdy sẽ thông báo tới quý khách ngay khi hàng chuẩn bị được giao.";

    public static String CONTENT_NOTIFY_WARNING = "Bạn đã bị khóa chức năng gửi cảnh báo [name] vì đã gửi quá [x] cảnh báo trong [y] phút. Chức năng sẽ được mở lại sau [z] phút, Xin cảm ơn.";
    public static String TITLE_NOTIFY_WARNING = "Thông báo khóa tính năng";
    public static String CONTENT_OTP = "Mã xác thực WIDDY của bạn là {code}";

    private static List<List<String>> avatarGradient = Arrays.asList(
            Arrays.asList("#36d1dc", "#00c2e7", "#00b1ef", "#0c9def", "#5b86e5"),
            Arrays.asList("#cb356b", "#ca355c", "#c7374e", "#c33a40", "#bd3f32"),
            Arrays.asList("#43c6ac", "#76d6a8", "#a2e5a5", "#cef3a7", "#f8ffae"),
            Arrays.asList("#7389dc", "#009fdd", "#00adc4", "#46b5a2", "#86b787"),
            Arrays.asList("#42a9bd", "#40bfca", "#47d5d2", "#5bead6", "#78ffd6"),
            Arrays.asList("#f2994a", "#f4a549", "#f4b148", "#f4bd49", "#f2c94c"),
            Arrays.asList("#4ac29a", "#67d1b2", "#83e1c8", "#a0f0de", "#bdfff3"),
            Arrays.asList("#9542b4", "#a360bd", "#b17dc5", "#be98cd", "#cbb4d4"),
            Arrays.asList("#4b79a1", "#599bba", "#72bccf", "#94dee1", "#bdfff3"),
            Arrays.asList("#fd746c", "#fe7b6a", "#ff8269", "#ff8968", "#ff9068")

    );

    public static String getNameWarningByType(String type) {
        switch (type) {
            case "jam":
                return "kẹt xe";
            case "danger":
                return "nguy hiểm";
            case "speed":
                return "tốc độ";
            case "accident":
                return "tai nạn";
            case "map_error":
                return "lỗi map";
            default:
                return "";

        }
    }

    public static String removeUnicode(String str, Boolean isToUpper) {
        if (StringUtils.isEmpty(str)) {
            return null;
        }
        str = str.toLowerCase();
        String temp = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        temp = temp.replaceAll("đ", "d");
        if (isToUpper) {
            temp = temp.toUpperCase();
        }
        return pattern.matcher(temp).replaceAll("");
    }

    public static String removeUnicode(String str) {
        if (StringUtils.isEmpty(str)) {
            return null;
        }
        str = str.toLowerCase();
        String temp = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        temp = temp.replaceAll("đ", "d");
        return pattern.matcher(temp).replaceAll("");
    }

    public static String documentToJSON(Document doc) {
        JSONObject ret = new JSONObject();
        doc.forEach((prop, value) -> ret.put(prop, value));
        String json = new Gson().toJson(ret);
        return json;
    }

    public String replaceString(String str, Integer index, Integer length) {
        if (index > 0) {
            return str.substring(0, index) + "***" + str.substring(length + index);
        }
        return "***" + str.substring(length);
    }

    public static String getUserId(RoutingContext routingContext) {
        return routingContext.user().principal().getString("user_id");
    }

    public static MultiMap getParams(RoutingContext routingContext) {
        return routingContext.request().params();
    }

}
