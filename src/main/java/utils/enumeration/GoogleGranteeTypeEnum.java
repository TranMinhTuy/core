package utils.enumeration;

public class GoogleGranteeTypeEnum {

    public static final String USER = "user";
    public static final String GROUP = "group";
    public static final String DOMAIN = "domain";
    public static final String ANYONE = "anyone";
}
