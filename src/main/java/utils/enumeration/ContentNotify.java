package utils.enumeration;

public class ContentNotify {
    public static String CONTENT_NOTIFY = "Gần bạn đang có quà tặng, cùng khám phá app Widdy để nhận quà ngay nhé";//voucher
    public static String CONTENT_NOTIFY_COMMENT = "Có một bình luận mới trong cảnh báo của bạn";//voucher
    public static String CONTENT_NOTIFY_REQUEST = " đã gửi cho bạn lời mời kết bạn";//request
    public static String CONTENT_NOTIFY_MESSAGE = " đã gửi {image} ảnh";//request
    public static String CONTENT_NOTIFY_RECORDER = " đã gửi một tin nhắn thoại";//request
    public static String CONTENT_NOTIFY_EMOTICON = " đã gửi một nhãn dán";//request
    public static String CONTENT_NOTIFY_ADD = "Bạn đã được thêm vào nhóm {name}";//request
    public static String CONTENT_NOTIFY_JOIN = "{full_name} đã yêu cầu tham gia nhóm {name}";//request
    public static String CONTENT_NOTIFY_LIKE_POST = "{userLike} thích bài đăng của bạn";//tag
    public static String CONTENT_NOTIFY_LIKE_COMMENT = "{userLike} thích bình luận của bạn";//tag
    public static String CONTENT_NOTIFY_REPLIED_COMMENT = "{userComment} đã trả lời bình luận của bạn";//tag
    public static String CONTENT_NOTIFY_COMMENT_POST_USER = "{userComment} đã bình luận bài viết";//tag
    public static String CONTENT_NOTIFY_COMMENT_POST_TAG = "{userComment} đã bình luận một bài viết mà bạn gắn thẻ";//tag

    public static String notificationCommentPost(String type, String name, long number) {
        if ("avatar".equals(type) || "cover".equals(type)) {
            if (number > 0) {
                return name + " và " + number + " người khác đã bình luận về ảnh của bạn";
            }
            return name + " đã bình luận về ảnh của bạn";
        }
        if (number > 0) {
            return name + " và " + number + " người khác đã bình luận bài viết của bạn";
        }
        return name + " đã bình luận bài viết của bạn";
    }

    public static String notificationLikePost(String type, String name, long number) {
        if ("avatar".equals(type) || "cover".equals(type)) {
            if (number > 0) {
                return name + " và " + number + " người khác thích ảnh của bạn";
            }
            return name + "  thích ảnh của bạn";
        }
        if (number > 0) {
            return name + " và " + number + " người khác thích bài viết của bạn";
        }
        return name + " thích bài viết của bạn";
    }
}
