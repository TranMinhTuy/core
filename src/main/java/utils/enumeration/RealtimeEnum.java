package utils.enumeration;

public class RealtimeEnum {
    public static String UPDATE_INFO_GROUP = "update_group";
    public static String ACCEPT_FRIEND = "accept_friend";
    public static String ADD_USER = "add_user";
    public static String UPDATE_SUBSIDY = "update_subsidy";
    public static String EVALUATE = "evaluate";
    public static String PRODUCT = "product";
    public static String VOUCHER = "voucher";
    public static String READ_MESSAGE = "read_message";

    public static String UPDATE_THEME = "update_theme";
    public static String DELETE_THEME = "delete_theme";

    public static String CREATE_POST = "create_post";
    public static String APPROVAL_POST = "approval_post";
    public static String DELETE_POST_WAITING_APPROVAL = "delete_post_waiting";
    public static String UPDATE_POST = "update_post";
    public static String DELETE_POST = "delete_post";

    public static String LIKE_POST = "like_post";
    public static String UPDATE_LIKE_POST = "update_like_post";
    public static String DELETE_LIKE_POST = "delete_like_post";
    public static String LIKE_COMMENT = "like_comment";
    public static String LIKE_IMAGE = "like_image";
    public static String COMMENT_POST = "comment_post";
    public static String COMMENT_IMAGE_POST = "comment_image";
    public static String UPDATE_COMMENT_DIARY = "update_comment_diary";
    public static String HANDLE_COMMENT_POST = "handle_comment_post";
    public static String DELETE_COMMENT_DIARY = "delete_comment_diary";

    public static String UPDATE_NOTIFICATION = "update_notification";
    public static String REMOVE_TAG = "remove_tag";
    public static String REMOVE_POST_TIME_LINE = "remove_post_time_line";
    public static String ADD_NOTIFICATION = "add_notification";
    public static String UPDATE_NOTIFY = "update_notify";
    public static String READ_ALL = "read_all";
    public static String DELETE_NOTIFY = "delete_notify";
    public static String HANDLE_ACCOUNT = "handle_account";
    public static String RECEIVE_POINT = "receive_point";
    public static String DELETE_VOUCHER_USER = "delete_voucher_user";
}
