package utils.enumeration;

public class AuthenticationKind {
    public static String INTERNAL = "internal";
    public static String GOOGLE = "google";
    public static String ZALO = "zalo";
    public static String FACEBOOK = "facebook";
    public static String APPLE = "apple";
}
