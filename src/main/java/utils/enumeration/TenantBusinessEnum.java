package utils.enumeration;

public class TenantBusinessEnum {

    public static final String NORMAL = "normal";
    public static final String ADMIN = "admin";
    public static final String BRANCH = "branch";
}
