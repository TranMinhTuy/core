package utils.enumeration;

public class GoogleRoleEnum {

    public static final String OWNER = "owner";
    public static final String ORGANIZER = "organizer";
    public static final String FILE_ORGANIZER = "fileOrganizer";
    public static final String WRITER = "writer";
    public static final String COMMENTER = "commenter";
    public static final String READER = "reader";

}
