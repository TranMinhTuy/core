package utils.enumeration;

public class ExcelType {
    public static String PRODUCT_ID = "ID sản phẩm";
    public static String VARIANT_ID = "ID thuộc tính";
    public static String CODE_PRODUCT = "Mã SP";
    public static String NAME_PRODUCT = "Tên SP";
    public static String SKU_VARIANT = "SKU phân loại SP";
    public static String CLASSIFICATION_ONE_NAME = "Thuộc tính 1";
    public static String CLASSIFICATION_ONE_VALUE = "Giá trị thuộc tính 1";
    public static String CLASSIFICATION_TWO_NAME = "Thuộc tính 2";
    public static String CLASSIFICATION_TWO_VALUE = "Giá trị thuộc tính 2";
    public static String ORIGINAL_PRICE = "Giá gốc";
    public static String PRICE = "Giá khuyến mãi";
    public static String DAY_START_PROMOTION = "Ngày bắt đầu KM";
    public static String DAY_END_PROMOTION = "Ngày kết thúc KM";
    public static String INVENTORY = "Tồn kho";
    public static String WEIGHT = "Khối lượng";
    public static String WIDTH = "Rộng";
    public static String LENGTH = "Dài";
    public static String HEIGHT = "Cao";
}
