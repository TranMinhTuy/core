package utils;

public class CollectionEnum {

    public static final String ACCOUNT = "account";
    public static final String USER = "users";
    public static final String HIDDEN = "hiddens";
    public static final String SCHEDULE = "schedules";
    public static final String NEWS = "news";
    public static final String VIDEO = "videos";
    public static final String BRANCH = "branches";
    public static final String ITEM = "items";
    public static final String CART = "carts";
    public static final String ORDER = "orders";
    public static final String EVALUATE = "evaluates";
}
