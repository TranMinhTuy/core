package utils;

public class KeyRedis {
    public static final String USER = "user_";
    public static final String VEHICLE = "vehicle_";
    public static final String SHOP = "shop_";
    public static final String PRODUCT_CART = "product_cart_";
    public static final String PRODUCT_DETAIL = "product_detail_";
    public static final String SUBSIDY = "subsidy_";
    public static final String PRODUCT_OUT_OF_STOCK = "product_out_of_stock_";
    public static final String TOTAL_PRODUCT_OUT_OF_STOCK = "total_product_out_of_stock_";
    public static final String PRODUCT_SHIPPING_FREE = "product_shipping_free_";
    public static final String TOTAL_PRODUCT_SHIPPING_FREE = "total_product_shipping_free_";
    public static final String FRIEND_USER = "friend_user_";
    public static final String FRIEND_IDS = "friend_ids_";
    public static final String TOTAL_FRIEND = "total_friend_";
    public static final String FRIEND_USER_OBJECT = "friend_user_object_";
    public static final String USER_INFO_MAP = "user_info_map_";
    //chat
    public static final String MESSAGE_GROUP = "message_group_";
    public static final String LAST_MESSAGE = "last_message_";
    public static final String LAST_MESSAGE_GROUP = "last_message_group_";
    public static final String TOTAL_MESSAGE_GROUP = "total_message_group_";
    public static final String LAST_COMMENT_POST = "last_comment_post_";
    public static final String GROUP = "group_";
    public static final String MESSAGE = "message_";
    public static final String POST = "post_";
    public static final String TOTAL_MESSAGE = "total_message_";
    public static final String TOTAL_UNREAD_MESSAGE = "total_unread_";
    public static final String USERS_GROUP = "users_group_";

    public static final String ODD = "odds_";
    public static final String FIXTURE = "fixture_";
    public static final String LEAGUE = "league_";
    public static final String STAND = "stand_";

    public static final String LIVE_SOCCER = "live_soccer";

}
