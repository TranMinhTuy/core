package utils.exception;

public class i18nException {

    public static String param_not_valid = "param_not_valid";
    public static String time_invalid = "time_invalid";
    public static String password_incorrect = "password_incorrect";
    public static String email_not_registered = "email_not_registered";
    public static String email_registered = "email_registered";
    public static String name_exist = "name_exist";
    public static String not_found_user = "not_found_user";
    public static String not_found_order = "not_found_order";
    public static String not_found_news = "not_found_news";
    public static String not_found_cart = "not_found_cart";

    public static String error_processing = "error_processing";
    public static String cannot_cancel_order = "cannot_cancel_order";

}
