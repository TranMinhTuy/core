package utils;

import ddd.filestorage.FireStorage;
import ddd.filestorage.application.IFileStoreApplication;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.UUID;

@Component
public class FileUtils {

    private String secret_key = "B3E090FCEFA0C57D5695AA09736F19";
    private String api_key = "777A6B04C63F5CBE17EBD29008DC80";
    private String brand_name = "verify";
    @Autowired
    IFileStoreApplication fileStoreApplication;

    public String uploadImages(RoutingContext routingContext, Set<FileUpload> uploads, String image_type) {
        try {
            StringBuilder stringBuilder = new StringBuilder();
            Integer count = 0;
            String imageString = "";
            for (FileUpload fu : uploads) {
                String objectName = DateFormatUtils.format(System.currentTimeMillis(), "yyyy/MM/dd") + "/";
                objectName += image_type + "/";
                objectName += UUID.randomUUID();
                objectName += "." + FilenameUtils.getExtension(fu.fileName());
                Buffer buffer = routingContext.vertx().fileSystem().readFileBlocking(fu.uploadedFileName());
                InputStream uploadedInputStream = new ByteArrayInputStream(buffer.getBytes());
                FireStorage fS = fileStoreApplication.put(StringEnum.REGION, StringEnum.BUCKET, image_type, objectName,
                        uploadedInputStream, fu.contentType()).orElse(null);
                uploadedInputStream.close();
                if (fS != null && fS.getObject_info() != null && fS.getObject_info().getName() != "") {
                    if (count != 0) {
                        imageString += "," + fS.getObject_info().getName();
                    } else {
                        imageString += fS.getObject_info().getName();
                    }
                }
                count += 1;
            }
            return imageString;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String uploadFile(RoutingContext routingContext, FileUpload fileUpload, String image_type) {
        try {

            String objectName = DateFormatUtils.format(System.currentTimeMillis(), "yyyy/MM/dd")
                    + "/" + image_type;
            objectName += "/" + UUID.randomUUID() + "." + FilenameUtils.getExtension(fileUpload.fileName());
            Buffer buffer = routingContext.vertx().fileSystem().readFileBlocking(fileUpload.uploadedFileName());
            InputStream uploadedInputStream = new ByteArrayInputStream(buffer.getBytes());
            FireStorage fS = fileStoreApplication.put(StringEnum.REGION, StringEnum.BUCKET, image_type, objectName,
                    uploadedInputStream, fileUpload.contentType()).orElse(null);
            uploadedInputStream.close();
            String imageString = null;
            if (fS != null && fS.getObject_info() != null && fS.getObject_info().getName() != "") {
                imageString = fS.getObject_info().getName();
            }
            return imageString;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String uploadTheme(RoutingContext routingContext, FileUpload fileUpload, String image_type) {
        try {
            String imageString = null;
            String objectName = "theme/" + image_type + "/" + UUID.randomUUID() + "." + FilenameUtils.getExtension(fileUpload.fileName());
            Buffer buffer = routingContext.vertx().fileSystem().readFileBlocking(fileUpload.uploadedFileName());
            InputStream uploadedInputStream = new ByteArrayInputStream(buffer.getBytes());
            FireStorage fS = fileStoreApplication.put(StringEnum.REGION, StringEnum.BUCKET, image_type, objectName,
                    uploadedInputStream, fileUpload.contentType()).orElse(null);
            uploadedInputStream.close();
            if (fS != null && fS.getObject_info() != null && fS.getObject_info().getName() != "") {
                imageString = fS.getObject_info().getName();
            }
            return imageString;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String uploadFileImageResize(InputStream imageBytes, String image_type, String typeImage, String url) {
        try {
            String imageString = null;
            FireStorage fS = fileStoreApplication.put(StringEnum.REGION, StringEnum.BUCKET, image_type, url,
                    imageBytes, "image/" + typeImage).orElse(null);
            imageBytes.close();
            if (fS != null && fS.getObject_info() != null && fS.getObject_info().getName() != "") {
                imageString = fS.getObject_info().getName();
            }
            return imageString;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    public String uploadFileResource(String type, String name, InputStream file, String type_file, String name_file) {
        try {
            String url = null;
            String objectName = DateFormatUtils.format(System.currentTimeMillis(), "yyyy/MM/dd")
                    + "/" + type;
//            UUID nameTemp = UUID.randomUUID();
            if (StringUtils.isNotEmpty(name_file)) {
                objectName += "/" + name_file + "." + FilenameUtils.getExtension(name);
            } else {
                objectName += "/" + UUID.randomUUID() + "." + FilenameUtils.getExtension(name);
            }

            FireStorage fS = fileStoreApplication.put(StringEnum.REGION, StringEnum.BUCKET, type, objectName,
                    file, type_file).orElse(null);
            file.close();
            if (fS != null && fS.getObject_info() != null && fS.getObject_info().getName() != "") {
                url = fS.getObject_info().getName();
            }
            return url;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Boolean removeFile(String url) {
        return fileStoreApplication.remove(StringEnum.REGION, StringEnum.BUCKET, url).orElse(null);
    }

}
