package utils;

import org.springframework.stereotype.Component;

@Component
public class SmsUtils {

    private String secret_key = "2831F96BF0FAF8EEB5DBB42996B07A";
    private String api_key = "92D861BE618631D7C0F6D4EF0612C1";
    private String brand_name = "WIDDY";

    public String sendSms(String phone, String content) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get");
        stringBuilder.append("?ApiKey=");
        stringBuilder.append(api_key);
        stringBuilder.append("&smsType=");
        stringBuilder.append("2");
        //sms key
        stringBuilder.append("&secretKey=");
        stringBuilder.append(secret_key);
        stringBuilder.append("&phone=");
        stringBuilder.append(phone);
        stringBuilder.append("&content=");
        stringBuilder.append(content);
        stringBuilder.append("&brandName=");
        stringBuilder.append(brand_name);
        return stringBuilder.toString();
    }
}
