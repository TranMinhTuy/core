package utils;

import org.bson.Document;
import org.springframework.stereotype.Component;
import utils.enumeration.StringType;

import java.util.Arrays;


@Component
public class DocumentUtils {
    //project
    public Document query_project_product() {
        Document document = new Document();
        document.append("_id", new Document("$toString", "$_id"));
        document.append("created_date", 1);
        document.append("shop_id", 1);
        document.append("cover", 1);
        document.append("name", 1);
        document.append("variants", 1);
        document.append("status", 1);
        document.append("code", 1);
        document.append("original_price", 1);
        document.append("price", 1);
        document.append("selling_price", 1);
        document.append("quantity", 1);
        document.append("inventory", 1);
        document.append("evaluate", 1);
        document.append("discount", 1);
        document.append("is_deleted", 1);
        document.append("is_promotion", 1);
        document.append("promotion_end_time", 1);
        document.append("promotion_start_time", 1);
        document.append("name_unsigned", 1);
        return document;
    }

    public Document query_project_promotion_list(String type) {
        Document document = new Document();
        document.append("_id", new Document("$toString", "$_id"));
        if (StringType.PROMOTION.equals(type)) {
            document.append("discount_max", 1);
            document.append("code", 1);
            document.append("quantity", 1);
            document.append("is_percent", 1);
        }
        document.append("image", 1);
        document.append("minimum_price", 1);
        document.append("shop_id", 1);
        document.append("end_time", 1);
        document.append("discount", 1);
        document.append("products", 1);
        document.append("start_time", 1);
        document.append("name", 1);
        document.append("created_date", 1);
        document.append("is_apply_all", 1);

        return document;
    }

    public Document query_project_promotion(String type) {
        Document document = new Document();
        document.append("_id", new Document("$toString", "$_id"));
        if (StringType.PROMOTION.equals(type)) {
            document.append("discount_max", 1);
            document.append("code", 1);
            document.append("quantity", 1);
            document.append("is_percent", 1);
        }
        document.append("image", 1);
        document.append("minimum_price", 1);
        document.append("shop_id", 1);
        document.append("end_time", 1);
        document.append("discount", 1);
        document.append("start_time", 1);
        document.append("name", 1);
        document.append("created_date", 1);
        document.append("is_apply_all", 1);
        return document;
    }

    public Document query_project_subsidy() {
        Document document = new Document();
        document.append("_id", new Document("$toString", "$_id"));
        document.append("image", 1);
        document.append("shop_id", 1);
        document.append("end_time", 1);
        document.append("products", 1);
        document.append("start_time", 1);
        document.append("name", 1);
        document.append("created_date", 1);
        return document;
    }

    public Document query_project_client(String type) {
        Document document = new Document();
        document.append("_id", 1);
        document.append("image", 1);
        document.append("minimum_price", 1);
        document.append("end_time", 1);
        document.append("start_time", 1);
        document.append("discount", 1);
        document.append("name", 1);
        if (StringType.PROMOTION.equals(type)) {
            document.append("discount_max", 1);
            document.append("is_percent", 1);
            document.append("code", 1);
        }
        return document;
    }

    public Document query_project_order() {
        Document document = new Document();
        document.append("_id", 1);
        document.append("address", 1);
        document.append("code", 1);
        document.append("total_price", 1);
        document.append("discount_price", 1);
        document.append("total_subsidy", 1);
        document.append("total_support_shipping", 1);
        document.append("shipping_fee", 1);
        document.append("is_deleted", 1);
        document.append("info_user", 1);
        document.append("user_id", 1);
        document.append("shops", 1);
        document.append("payment", 1);
        document.append("created_date", 1);
        document.append("estimated_delivery", 1);
        document.append("last_updated_date", 1);
        document.append("promotion", 1);
        document.append("status", 1);
        document.append("total_price_goods", 1);
        document.append("total_price_promotion_shop", 1);
        document.append("is_evaluate", new Document("$anyElementTrue", Arrays.asList("$evaluates")));
        return document;
    }

    //addfile
    public Document query_add_fields_promotion() {
        Document document = new Document();
        Document toObjectId = new Document("$toObjectId", "$$r.product_id");
        Document map = new Document("input", "$products");
        map.append("as", "r");
        map.append("in", toObjectId);
        document.append("products", new Document("$map", map));
        return document;
    }

    //lookup

    public Document query_lookup_product_evaluate() {
        Document document = new Document();
        document.append("from", "evaluates");
        document.append("localField", "_id");
        document.append("foreignField", "product_id");
        document.append("as", "evaluates");
        return document;
    }

    public Document query_lookup_promotion() {
        Document match = new Document();
        match.append("$expr", new Document("$in", Arrays.asList("$_id", new Document("$ifNull", Arrays.asList("$$products", Arrays.asList())))));

        Document project_product = query_project_product();

        Document document = new Document();
        document.append("from", "products");
        document.append("let", new Document("products", "$products"));
        document.append("pipeline", Arrays.asList(
                new Document("$match", match),
                new Document("$project", project_product)
        ));
        document.append("as", "products");
        return document;
    }
}
