package utils;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MustacheUtils<T> {
    private static final MustacheFactory mf = new DefaultMustacheFactory();

    public static MustacheFactory getMustacheFactory() {
        return mf;
    }

    public static String executeTemplate(String templateName, Map<String, Object> context) throws IOException {
        Mustache m = mf.compile(templateName);
        StringWriter writer = new StringWriter();
        m.execute(writer, context).flush();
        return writer.toString();
    }


    public static String executeMailOrder(String templateName, Map<String, Object> context) throws IOException {
        if (context == null) {
            context = new HashMap<>();
        }

        /** Mail Footer **/
        context.put("mailSupport", "hotro@widdy.vn");
        context.put("phoneSupport", "0901464566");

        return executeTemplate("email/" + templateName, context);
    }

    public static String executeString(String text, Map<String, Object> context) throws IOException {
        StringWriter writer = new StringWriter();
        Mustache m = mf.compile(new StringReader(text), "template" + new Date().getTime());
        m.execute(writer, context).flush();
        return writer.toString();
    }

    public static <T> String executeTemplate(String templateName, T entity) throws IOException {
        Mustache m = mf.compile(templateName);
        StringWriter writer = new StringWriter();
        m.execute(writer, entity).flush();
        return writer.toString();
    }

    public static <T> String executeTemplate(Reader templateName, Map<String, Object> context) throws IOException {
        Mustache m = mf.compile(templateName, "admin");
        StringWriter writer = new StringWriter();
        m.execute(writer, context).flush();
        return writer.toString();
    }

    public static <T> String executeTemplate(Reader templateName, T entity) throws IOException {
        Mustache m = mf.compile(templateName, "admin");
        StringWriter writer = new StringWriter();
        m.execute(writer, entity).flush();
        return writer.toString();
    }
}
