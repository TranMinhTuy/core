package utils.sendgrid;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.JSONUtils;
import utils.http.OkHttpUtils;

import java.util.Arrays;
import java.util.List;

public class SendGridUtils {
    private static Logger LOGGER = LogManager.getLogger();
    private static final String rootUrl = "https://api.sendgrid.com/v3";
    public static final String SENDGRID_RETRY = "sendgrid_retry";
    

    public static String sendEmail(String userToken, SendGridEmailRequestBody.Person from,
                                   List<SendGridEmailRequestBody.Person> to,
                                   List<SendGridEmailRequestBody.Person> cc,
                                   List<SendGridEmailRequestBody.Person> bcc,
                                   String subject, String content) {
        String response = null;
        try {
            SendGridEmailRequestBody sendGridEmailRequestBody = builtSendRequest(from, to,
                    cc, bcc, subject, content);
            response = OkHttpUtils.synPostJson(rootUrl + "/mail/send", userToken, JSONUtils.toJSON(sendGridEmailRequestBody));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            LOGGER.error(throwable);
        }
        return response;
    }

    private static SendGridEmailRequestBody builtSendRequest(SendGridEmailRequestBody.Person from,
                                                             List<SendGridEmailRequestBody.Person> to,
                                                             List<SendGridEmailRequestBody.Person> cc,
                                                             List<SendGridEmailRequestBody.Person> bcc,
                                                             String subject, String content) {
        SendGridEmailRequestBody sendGridEmailRequestBody = SendGridEmailRequestBody.builder()
                .personalizations(Arrays.asList(SendGridEmailRequestBody.Personalizations.builder()
                        .to(to)
                        .cc(CollectionUtils.isEmpty(cc) ? null : cc)
                        .bcc(CollectionUtils.isEmpty(bcc) ? null : bcc)
                        .subject(subject)
                        .build()))
                .from(from)
                .content(Arrays.asList(SendGridEmailRequestBody.Content.builder().value(content).build()))
                .build();
        return sendGridEmailRequestBody;
    }
}
