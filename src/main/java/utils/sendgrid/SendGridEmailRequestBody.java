package utils.sendgrid;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class SendGridEmailRequestBody {
    private List<Personalizations> personalizations;
    private Person from;
    private List<Content> content;
    private Attachments attachments;

    public SendGridEmailRequestBody() {
    }

    @Data
    @AllArgsConstructor
    @Builder
    public static class Personalizations implements Serializable {
        private List<Person> to;
        private List<Person> cc;
        private List<Person> bcc;
        private String subject;

        public Personalizations() {
        }

    }

    @Data
    @AllArgsConstructor
    @Builder
    public static class Person implements Serializable {
        private String email;
        private String name;

        public Person() {
        }
    }

    @Data
    @AllArgsConstructor
    @Builder
    public static class Content implements Serializable {
        @Builder.Default private String type= "text/html"; // text/html / text/plain
        private String value;

        public Content() {
        }
    }

    @Data
    @AllArgsConstructor
    @Builder
    public static class Attachments implements Serializable {
        private String content; // base64
        private String type;
        private String filename;
        private String disposition;
        private String content_id;

        public Attachments() {
        }
    }
}
