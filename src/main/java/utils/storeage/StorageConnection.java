/*
 * Copyright (c) 2019. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package utils.storeage;

import io.minio.MinioClient;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import utils.crypto.SHA512;

import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ConcurrentHashMap;


@Component
public class StorageConnection {
    private static ConcurrentHashMap<String, MinioClient> storageConnection = new ConcurrentHashMap<>();

    public static MinioClient getConnect(StorageConfig storageConfig) throws InvalidPortException, NoSuchAlgorithmException, InvalidEndpointException {
        try {
            String key = SHA512.valueOf(
                    new StringBuilder()
                            .append(storageConfig.connectionURL)
                            .append(storageConfig.accessKey)
                            .append(storageConfig.secretKey)
                            .toString());
            if (!storageConnection.containsKey(key)) {
                storageConnection.put(
                        key,
                        new MinioClient(
                                storageConfig.connectionURL,
                                storageConfig.accessKey,
                                storageConfig.secretKey
                        ));
            }
            return storageConnection.get(key);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}

