/*
 * Copyright (c) 2019. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package utils.storeage;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter

public class StorageConfig {
    String connectionURL;
    String accessKey;
    String secretKey;

    public static final class StorageConfigBuilder {
        String connectionURL;
        String accessKey;
        String secretKey;

        private StorageConfigBuilder() {
        }

        public static StorageConfigBuilder storageConfig() {
            return new StorageConfigBuilder();
        }

        public StorageConfigBuilder withConnectionURL(String connectionURL) {
            this.connectionURL = connectionURL;
            return this;
        }

        public StorageConfigBuilder withAccessKey(String accessKey) {
            this.accessKey = accessKey;
            return this;
        }

        public StorageConfigBuilder withSecretKey(String secretKey) {
            this.secretKey = secretKey;
            return this;
        }

        public StorageConfig build() {
            StorageConfig storageConfig = new StorageConfig();
            storageConfig.accessKey = this.accessKey;
            storageConfig.secretKey = this.secretKey;
            storageConfig.connectionURL = this.connectionURL;
            return storageConfig;
        }
    }
}
