package utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

public class RandomNumberUtils {
    private static final int[] prefix = {1, 2, 3, 4, 5, 6, 7, 8, 9};

    /**
     * Randomly generated a maximum of long type 18 bit data (the maximum value of the long data type is 922337203685477580, a total of 19)
     *
     * @param digit The user specifies the number of random data
     */
    public static String randomLong(int digit) {
        if (digit >= 19 || digit <= 0)
            throw new IllegalArgumentException("digit should between 1 and 18(1<=digit<=18)");
        String s = RandomStringUtils.randomNumeric(digit - 1);
        return getPrefix() + s;
    }

    /**
     * Randomly generated long data in a specified number of bits between, including both value,minDigit<=maxDigit
     *
     * @param minDigit The user specifies the minimum number of minDigit random data>=1
     * @param maxDigit The user specifies the maximum number of maxDigit random data<=18
     */
    public static String randomLong(int minDigit, int maxDigit) {
        if (minDigit > maxDigit) {
            throw new IllegalArgumentException("minDigit > maxDigit");
        }
        if (minDigit <= 0 || maxDigit >= 19) {
            throw new IllegalArgumentException("minDigit <=0 || maxDigit>=19");
        }
        return randomLong(minDigit + getDigit(maxDigit - minDigit));
    }

    private static int getDigit(int max) {
        return RandomUtils.nextInt(0, max + 1);
    }

    /**
     * That is not the first zero
     *
     * @return
     */
    private static String getPrefix() {
        return prefix[RandomUtils.nextInt(0, 9)] + "";
    }
}
