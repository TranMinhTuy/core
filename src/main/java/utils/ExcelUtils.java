package utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {
    private static void processCell(Row row, CellStyle style, int numberCell, String content) {
        Cell cell = row.createCell(numberCell);
        cell.setCellValue(content);
        cell.setCellStyle(style);
    }

    public static void writeContentCell(XSSFWorkbook workbook, XSSFSheet sheet, int rowNum, int numberCell, int fontHeight, Boolean isLocked, String title, String content) {
        XSSFFont font = workbook.createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) fontHeight);

        CellStyle contentStyle = workbook.createCellStyle();
        contentStyle.setFont(font);
        contentStyle.setLocked(isLocked);
        Row row = sheet.createRow(rowNum);
        processCell(row, contentStyle, numberCell++, title);
        processCell(row, contentStyle, numberCell, content);
    }


    public static void writeTitleLine(XSSFSheet sheet, XSSFWorkbook workbook, String title, String mergedRegion, Integer line, Boolean isBold, Integer numberCell) {
        sheet.addMergedRegion(CellRangeAddress.valueOf(mergedRegion));
        Row header = sheet.createRow(line);
        XSSFFont font = workbook.createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 13);
        font.setBold(isBold);

        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER);
        processCell(header, style, numberCell, title);
    }


    public static void main() {
        //Load the input Excel file
//        Workbook workbook;
//        workbook.loadFromFile("Input.xlsx");
//
//        //Fit to page
//        workbook.getConverterSetting().setSheetFitToPage(true);

        //Save as PDF document
//        workbook.saveToFile("ExcelToPDF.pdf", FileFormat.PDF);
    }
}
