package utils.vertx.agent_authen;

import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import org.springframework.stereotype.Component;


@Component
public class AgentAuthenConfigCentral {
    public JWTAuthOptions config = new JWTAuthOptions();

//    public AgentAuthenConfigCentral() {
//        config.setPermissionsClaimKey("resource_access").addPubSecKey(new PubSecKeyOptions()
//                .setAlgorithm("RS256")
//                .setPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuvkCAmzMdGqGXA5Jl4lz\n" +
//                        "ersHfYlo/Y/1LxzZm7czHs5CRCGGCU1EskkQXgOYZA1pCazRUtmABRlcta2aNciA\n" +
//                        "LVlzmk0cBEprcetI/H2XWC4s7d1GgIDujI+zjB3M4Lv7T3OuPpH73D98LwAcZl2d\n" +
//                        "SHsylpz2JhC8g3KzPkD+olX1WnmuTb/2pudS8D2y32g0QCYidue3B4e/yt7KkVAt\n" +
//                        "1iwc7EAtWf4zQbaZnDmPP9y8Ws+v1ErCqBdrdu8VQXu8VXpfe9ZXiuDHl9/IfxH7\n" +
//                        "uZPgSVOASdGeQo9CZQudK+vhqbJ0h0xFDcJS+9SBS1Mo2yfrHRBNV+PZaVyhcpS3\n" +
//                        "kQIDAQAB")
//                .setSecretKey("MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC6+QICbMx0aoZc\n" +
//                        "DkmXiXN6uwd9iWj9j/UvHNmbtzMezkJEIYYJTUSySRBeA5hkDWkJrNFS2YAFGVy1\n" +
//                        "rZo1yIAtWXOaTRwESmtx60j8fZdYLizt3UaAgO6Mj7OMHczgu/tPc64+kfvcP3wv\n" +
//                        "ABxmXZ1IezKWnPYmELyDcrM+QP6iVfVaea5Nv/am51LwPbLfaDRAJiJ257cHh7/K\n" +
//                        "3sqRUC3WLBzsQC1Z/jNBtpmcOY8/3Lxaz6/USsKoF2t27xVBe7xVel971leK4MeX\n" +
//                        "38h/Efu5k+BJU4BJ0Z5Cj0JlC50r6+GpsnSHTEUNwlL71IFLUyjbJ+sdEE1X49lp\n" +
//                        "XKFylLeRAgMBAAECggEBAKs92lhMOyk4UxxGvaH7cndqil6wlib9TQq8OpVCQaCq\n" +
//                        "Ocjahz9HtG4xOrtQQacJHTWOQRzHNVf2NZHzkLRHkRV3wsgEbW+ElUl9u5f8krth\n" +
//                        "dybXrXWOIG0+Q6xFstn1cSMAiQqne2vp/Ruy10/t11qF8vw8f7i3RWCCkaWns08L\n" +
//                        "JVUBOwzfdoroswrzN8VRnONpFxwhz6UfIi0mi+jzfWKcGXax82rPzTuWQq9h/6yD\n" +
//                        "d6f7HuWozH7h8+X2dROw3NTwcdG8YLcp3k5IHEQvHSTBse0hCBonYrUvHONlxmLR\n" +
//                        "iPF0mda/qNeOFhhTFVU2/q1jli1dSbYb7jq3WNx5OpECgYEA97kTTDQ8BHGFXARm\n" +
//                        "W1GLJBKeF7UPohwhnPzUhfDGIvu2AaV9CuJb3XrssarcTDQWe/iaZ1dh3+9VScyI\n" +
//                        "6FjACB9GJGXWoBO8StbHp6zB9lDrdfx6v2eyzp3FdrIPfPRfUAhuQ24baYA6Bpj7\n" +
//                        "U4V3XzrCbuOBJzQ7TK95ShWMne0CgYEAwThMcoLVe7URcxCltO4zT03zM/cJsqHV\n" +
//                        "FuiQxizh9dfopvYavf86IbHOkQkqHqIbj1gx3gz88GwfUJpHsW1ibir/pNy6KjSK\n" +
//                        "pDAofaT5Qq0yl0AGtDPadL6y+AyOZWJ+eVUEoPbkwz7bDeW7nowuXhuT9GI/9s3k\n" +
//                        "93oj8yPja7UCgYEAhOelw9eub4/cRb8Sj2HTbdoTOZLpvUasfgZjmyzPkMtNHdsE\n" +
//                        "mEdTlCM0RCnwIW48texuIo2ONE+6iWqT9VxQQstMnZCJsr6D4f8ThRE5ywYlHdsE\n" +
//                        "nBZtp06cN49WoeqWDGFJjyHO0S57czi2dlv/7HahBE9I5piuS/lE8ItZMw0CgYEA\n" +
//                        "oCe8JgaQUZPKPOgFki0G1qsz9VTE8w31BKIMrlcxjIdETnAbxnSxiJBLOiiwgue3\n" +
//                        "lF90Jiv51IaqhjL7TfPu7IYzlEFpBea7LFDGny6JdVz+XCQz6lY4syAGVFiEB0FW\n" +
//                        "t9dimNFM2RFWnPRZSZfELzkIsD5n6njLWt4gwj5zGSkCgYAkj+cA0ciD4vzAVUL6\n" +
//                        "qYEnHGyz76ewckCMHHpZlbIOK8NDwTmMwO0eTJpOsVmvoWXGNSI7cYisV7+jo6jl\n" +
//                        "/egSoilzzMaTd+R+cwqEen6WK1fz2Mon2tlVA0AD81AYEeu+05WR9VkbM2Q5+v4f\n" +
//                        "5Q5ucx67L4SXdztBN0Ds+EtBNQ==")
//        );
//    }

    public AgentAuthenConfigCentral() {
        config.setPermissionsClaimKey("resource_access").addPubSecKey(new PubSecKeyOptions()
                .setAlgorithm("RS256")
                .setPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArNtuYktnMdsQfQPXwETq\n" +
                        "dfpND7q2KDffxqRch4SGoQB1AbpZMfs8985r1lcziKLgEGMW+7ZXReT6LMu/fS0j\n" +
                        "kn0pyo3RBG4yfy+ri6KMYgIT8Z6026wz9yZbptlPnvxpV6SRkCqb2YBGTHZjJxVN\n" +
                        "mqQigA3VJ9pCLrikq/x0UJFhelGSGQU/FaekR2eMwwj+3I5x/3rHwuETRidytpCf\n" +
                        "T13yvblwfouo8v3AowTagVOXFKnpQhcZL7E1FhshTAgk9br8rfCb18KicUDznnfX\n" +
                        "gK3Rh/Ks20w5EWZUjMuSaa7giZES6vuASdEgV+jExN4R99hd37Xfsd7EERRdmI0b\n" +
                        "ewIDAQAB")
                .setSecretKey("MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCs225iS2cx2xB9\n" +
                        "A9fAROp1+k0PurYoN9/GpFyHhIahAHUBulkx+zz3zmvWVzOIouAQYxb7tldF5Pos\n" +
                        "y799LSOSfSnKjdEEbjJ/L6uLooxiAhPxnrTbrDP3Jlum2U+e/GlXpJGQKpvZgEZM\n" +
                        "dmMnFU2apCKADdUn2kIuuKSr/HRQkWF6UZIZBT8Vp6RHZ4zDCP7cjnH/esfC4RNG\n" +
                        "J3K2kJ9PXfK9uXB+i6jy/cCjBNqBU5cUqelCFxkvsTUWGyFMCCT1uvyt8JvXwqJx\n" +
                        "QPOed9eArdGH8qzbTDkRZlSMy5JpruCJkRLq+4BJ0SBX6MTE3hH32F3ftd+x3sQR\n" +
                        "FF2YjRt7AgMBAAECggEBAIKX0aCPiSpgb+eTRKgtKNnzPc85Z6+g7v6bZJC57iCr\n" +
                        "QLsKd6LMRjaODGjGPtfm/dNDmbSYAP18TgoT8Nr4hFyDFVTDqftn6dyvPjJjnx3Y\n" +
                        "pgqwtcEVK7l4kFvQ6FecKVYerh62IkLDp9jSxHxdmuDPSoVmbfkK619y/m41cAcB\n" +
                        "YDtTFYX1BJrf3ZDhCFZDFVBZBLxW3op5RPhs/C3h12fTcvtm7/bMULoyzxUVpyjZ\n" +
                        "/im0ke7quR/QH58rbB7JGEUlMDo54TgEICXbEl7N//ePJLTMWchg9fl6mADbkx3w\n" +
                        "mHuqbfk9Xw/lS/90b3aVpSoX6zFaqQ9MAIBLt3FuHeECgYEA4+2IC9/cZEb1qzJX\n" +
                        "lZeexvpPWbhkByA8ECRDOlVBOTvStInA8JFVt2YOd4B0u43mPyBHHB92nrTstnAS\n" +
                        "VRdYNVv/0HCh5tWu3FuqzYTVVWq0UGRYvK7zqNNZRlC0ljV70uaEMIj0YyIaiUkl\n" +
                        "1lE6P6WENttZ7GDtMarsq8hYBbECgYEAwiWLOOZ86AX9YWFNJ9TcZcS7ekz5i7Wy\n" +
                        "xf/oLXdXSDQndL7zS+NoAWPNPhodsHKGK+uVCya1Q1hiJaZQNt/Z025gFFmr5oZy\n" +
                        "bo4vG4mouAvF3zLT5msMWGkbWA9ZAXBWXVjUjMi+20c4NNPJPV3nNpzGuzQKfpXQ\n" +
                        "AHQqZRWcgusCgYEAtb7YNq/Aq20xw9sWn8AQ6ZT65Q31JQF9I1nnCYKlyZ98Ol2F\n" +
                        "rc4VzXHho0SY0F0r9bYlPbWQ/qhCAI+I7rT4r0O2zlyhnNFS539oyQxcmAnH0jrR\n" +
                        "z1KqFDrC+yCrfwMSw27whUsPpGSzSDk9pJGMoue2b4+lOqlnOa2Tnr7YAWECgYBa\n" +
                        "PBDlNVtAi+gkZpDxvmFhe3qXcUdaw+x/Ul2bJCiRCox8GnQCBYPFgZlXD9RUuiXa\n" +
                        "K/ht4mPFFUMotI/7sTcQqfBGBRd8YWi2Mad7aRPd1rr07i7GnWJzhQaMezWnEYtS\n" +
                        "f4LqnDL4v7VD+FtEBmz8WQmU4K8v/nlafKNnsA+6WQKBgQCJ9Ec65m/7DNdFVHk9\n" +
                        "o3whZ2kuDk5nDIdyJSn8b5QELFcbKUjnpUZl5CF7+M5+pYz3yLdRmunYFq5p9Q94\n" +
                        "soKPUlxAQucwXHdil6AwpoFarFOZ2ljWrwVaobocxaSp39fA5IjeiNEtmRIDoWvW\n" +
                        "4zgkC6jWKwg22Ew0MsANAlTdpA==")
        );
    }
}
