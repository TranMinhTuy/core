/*
 * Copyright  2019. VIHAT
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  NOTICE: THIS FILE HAS BEEN MODIFIED BY <MY EXAMPLE COMPANY> UNDER COMPLIANCE WITH THE APACHE 2.0 LICENCE FROM THE ORIGINAL WORK
 *  OF THE COMPANY <EXAMPLE PREVIOUS COMPANY>. THE FOLLOWING IS THE COPYRIGHT OF THE ORIGINAL DOCUMENT:
 *
 *  Copyright 2016 Example Previous Company
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  Contributors:
 *
 */

package utils.vertx.agent_authen;

import io.vertx.ext.auth.jwt.JWTAuth;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.vertx.VertxProvider;

@Data

@Component

public class JWTAuthProvider {
    JWTAuth jwtAuth;

    @Autowired
    public JWTAuthProvider(VertxProvider vertxProvider, AgentAuthenConfigCentral agentAuthenConfigCentral) {
        jwtAuth = JWTAuth.create(vertxProvider.getVertx(), agentAuthenConfigCentral.config);
    }
}
