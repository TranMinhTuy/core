package utils.vertx.rest;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.impl.AuthHandlerImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

public class VIHATSaaSBasicAuthHandler extends AuthHandlerImpl {
    private static Logger LOGGER = LogManager.getLogger();
    private static final String USERNAME = "OMI_VIHAT";
    private static final String PASSWORD = "yQttR2fHt~+WC]r+";

    public VIHATSaaSBasicAuthHandler(AuthProvider authProvider) {
        super(authProvider);
    }

    @Override
    public void parseCredentials(RoutingContext context, Handler<AsyncResult<JsonObject>> handler) {

    }

    @Override
    public void handle(RoutingContext routingContext) {
        try {
            HttpServerRequest request = routingContext.request();
            request.pause();
            String username = routingContext.queryParams().get("username");
            String password = routingContext.queryParams().get("password");
            String usernameFromHeader = routingContext.request().getHeader("username");
            String passwordFromHeader = routingContext.request().getHeader("password");
            String passwordDecode = "";

            if(StringUtils.isNotBlank(password)){
                passwordDecode = URLDecoder.decode(password, StandardCharsets.UTF_8.name());
            }
            if ((USERNAME.equals(username) && (PASSWORD.equals(password) || PASSWORD.equals(passwordDecode)))
                    || (USERNAME.equals(usernameFromHeader) && PASSWORD.equals(passwordFromHeader))) {
                request.resume();
                routingContext.next();
            } else {
                routingContext.fail(401);
            }
        } catch (Throwable e) {
            LOGGER.error(e.getMessage());
            routingContext.fail(401);
        }
    }
}
