package utils.vertx.rest;

import ddd.common.context.VIHATSaaSContext;
import ddd.common.context.VIHATSaaSDataContext;
import factory.provider.ObjectMapperProvider;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.impl.AuthHandlerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.redis.JedisMaster;
import utils.vertx.agent_authen.JWTAuthProvider;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Component
public class VIHATSaaSJWTAuthHandler extends AuthHandlerImpl {
    private static Logger LOGGER = LogManager.getLogger();

    @Autowired
    public VIHATSaaSJWTAuthHandler(JWTAuthProvider jwtAuthProvider) {
        super(jwtAuthProvider.getJwtAuth());
    }

    @Autowired
    private JedisMaster jedisMaster;

    @Autowired
    private ObjectMapperProvider objectMapperProvider;


    @Override
    public void parseCredentials(RoutingContext context, Handler<AsyncResult<JsonObject>> handler) {

    }

    @Override
    public void handle(RoutingContext routingContext) {
        HttpServerRequest request = routingContext.request();
        request.pause();
        String authorization = request.headers().get(HttpHeaders.AUTHORIZATION);
        if (authorization == null) {
            routingContext.fail(401);
        } else {
            String[] parts = authorization.split(" ");
            if (parts.length != 2) {
                routingContext.fail(401);
            } else {
                String scheme = parts[0];
                if (!"bearer".equalsIgnoreCase(scheme)) {
                    routingContext.fail(401);
                } else {
                    String token = parts[1];

                    JsonObject credentials = new JsonObject();
                    credentials.put("jwt", token);

                    authProvider.authenticate(credentials, res -> {
                        if (res.succeeded()) {
                            try {
                                JsonObject jsonObject = res.result().principal();

//
//                                VIHATSaaSContext.setObject(VIHATSaaSDataContext
//                                        .builder()
//                                        .agent_id(jsonObject.getString("agent_id"))
//                                        .contact_id(jsonObject.getString("contact_id"))
//                                        .tenant_id(jsonObject.getString("tenant_id"))
//                                        .full_name(jsonObject.getString("full_name"))
//                                        .request_id(UUID.randomUUID().toString())
//                                        .roles(Collections.emptyList())
//                                        .build());

//                                res.result().principal().put("omi-context-tenant_id",jsonObject.getString("tenant_id"));
//                                res.result().principal().put("omi-context-contact_id",jsonObject.getString("contact_id"));
//                                res.result().principal().put("omi-context-agent_id",jsonObject.getString("agent_id"));
//                                res.result().principal().put("omi-context-full_name",jsonObject.getString("full_name"));
//                                res.result().principal().put("omi-context-request_id",UUID.randomUUID().toString());
//                                res.result().principal().put("omi-context-roles",Collections.emptyList());

                                routingContext.setUser(res.result());
                            } catch (Throwable e) {
                                LOGGER.error(e.getMessage());
                            } finally {
                                request.resume();
                                routingContext.next();
                            }
                        } else {
                            routingContext.fail(401);
                        }
                    });
                }
            }
        }
    }
}
