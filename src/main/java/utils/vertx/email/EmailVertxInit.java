package utils.vertx.email;

import io.vertx.core.AbstractVerticle;
import io.vertx.ext.mail.MailClient;
import io.vertx.ext.mail.MailConfig;
import io.vertx.ext.mail.StartTLSOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.vertx.VertxProvider;


@Component

public class EmailVertxInit extends AbstractVerticle {

    public static MailClient mailClient;

    public MailClient getMailClient() {
        return mailClient;
    }

    @Override
    public void start() {
        MailConfig config = new MailConfig();
        config.setHostname("smtp.gmail.com");
        config.setPort(587);
        config.setStarttls(StartTLSOptions.REQUIRED);
        config.setUsername("nguyen.nguyen00012018@gmail.com");
        config.setPassword("@@123456");
        mailClient = MailClient.createShared(vertx, config);
    }

    @Autowired
    public EmailVertxInit(VertxProvider vertxProvider) {
        this.vertx = vertxProvider.getVertx();
        start();
    }
}
