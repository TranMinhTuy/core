package utils.vertx.email;

import io.vertx.ext.mail.MailMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component

public class EmailVertx {
    EmailVertxInit emailVertxInit;

    @Autowired
    public EmailVertx(EmailVertxInit emailVertxInit) {
        this.emailVertxInit = emailVertxInit;
    }

    public void sendEmail(MailMessage message) {
        emailVertxInit.getMailClient().sendMail(message, result -> {
            if (result.succeeded()) {
            } else {
                result.cause().printStackTrace();
            }
        });
    }
}
