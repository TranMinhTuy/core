package utils.vertx.email;

import io.vertx.core.Vertx;
import io.vertx.ext.mail.MailClient;
import io.vertx.ext.mail.MailConfig;
import io.vertx.ext.mail.MailMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.vertx.VertxProvider;


@Component

public class EmailVertxMaster {
    Vertx vertx;

    @Autowired
    public EmailVertxMaster(VertxProvider vertxProvider) {
        this.vertx = vertxProvider.getVertx();
    }

    public void send(MailMessage message, MailConfig config) {
        MailClient mailClient = MailClient.createShared(vertx, config);
        mailClient.sendMail(message, result -> {
            if (result.succeeded()) {
            } else {
                result.cause().printStackTrace();
            }
        });
    }
}
