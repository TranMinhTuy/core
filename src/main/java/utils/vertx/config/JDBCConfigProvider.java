package utils.vertx.config;

import io.vertx.core.json.JsonObject;

import javax.xml.ws.Provider;

public class JDBCConfigProvider implements Provider<JsonObject> {
    @Override
    public JsonObject invoke(JsonObject entries) {
        JsonObject config = new JsonObject()
                .put("url", "jdbc:hsqldb:mem:test?shutdown=true")
                .put("driver_class", "org.hsqldb.jdbcDriver")
                .put("max_pool_size", 30);
        return config;
    }
}
