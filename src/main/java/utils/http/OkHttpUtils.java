package utils.http;

import okhttp3.*;
import org.apache.commons.collections4.CollectionUtils;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class OkHttpUtils {
    private static final long CONNECT_TIMEOUT = 5;
    private static final long READ_TIMEOUT = 5;
    private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");
    private final static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .build();

    private OkHttpUtils() {
    }

    private static Response execute(Request request) throws Throwable {
        return okHttpClient.newCall(request).execute();
    }

    private static void enqueue(Request request) {
        okHttpClient.newCall(request).enqueue(new CallBackokHttp());
    }

    private static void enqueue(Request request, Callback responseCallback) {
        okHttpClient.newCall(request).enqueue(responseCallback);
    }

    public static String synGetString(String url) throws Throwable {
        Request request = new Request.Builder().url(url).build();
        try (Response response = okHttpClient.newCall(request).execute()) {
            return response.body().string();
        } catch (Throwable e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String synGetStringFB(String url) throws Throwable {
        Request request = new Request.Builder().url(url)
                .addHeader("x-rapidapi-key", "20319844bbmshff1cf5d1888a1c3p1af2e2jsn126210f3597b")
                .addHeader("x-rapidapi-host", "api-football-v1.p.rapidapi.com")
                .build();
        Response response = execute(request);
        if (response.isSuccessful()) {
            assert response.body() != null;
            return response.body().string();
        } else {
            return "";
        }
    }

    public static void asynDefaultGet(String url) throws Throwable {
        Request request = new Request.Builder().url(url).build();
        enqueue(request);
    }

    public static void asynGet(String url, Callback responseCallback) throws Throwable {
        Request request = new Request.Builder().url(url).build();
        enqueue(request, responseCallback);
    }

    public static String synPostJson(String url, String json) throws Throwable {
        RequestBody body = null;
        body = RequestBody.create(MEDIA_TYPE_JSON, json);
        Request request = new Request.Builder().url(url).post(body).build();

        Response response = execute(request);
        if (response.isSuccessful()) {
            assert response.body() != null;
            return response.body().string();
        } else {
            throw new Exception("postJson method fail, " + response);
        }
    }

    public static String synPostJson(String url, String userToken, String json) throws Throwable {
        RequestBody body = null;
        body = RequestBody.create(MEDIA_TYPE_JSON, json);
        Request request = new Request.Builder().url(url).addHeader("Authorization", "Bearer " + userToken).post(body).build();

        Response response = execute(request);
        if (response.isSuccessful()) {
            assert response.body() != null;
            return response.body().string();
        } else {
            throw new Exception("postJson method fail, " + response);
        }
    }

    public static String synPostForm(String url, Map<String, String> map) throws Throwable {
        RequestBody body = null;
        body = makeFormEncodingBuilder(map).build();
        Request request = new Request.Builder().url(url).post(body).build();
        Response response = execute(request);
        if (response.isSuccessful()) {
            assert response.body() != null;
            return response.body().string();
        } else {
            throw new IOException("postForm method fail, " + response);
        }
    }

    public static String synPostWebhookJson(String url, String token, String bodyJson) throws Throwable {
        RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, bodyJson);
        Request request = new Request.Builder().url(url)
                .addHeader("token", token)
                .post(body).build();
        Response response = execute(request);
        if (response.isSuccessful()) {
            assert response.body() != null;
            return response.body().string();
        } else {
            throw new Exception("postJson method fail, " + response);
        }
    }

    public static String synGetWebhookJson(String url, String token) throws Throwable {
        Request request = new Request.Builder().url(url)
                .addHeader("token", token)
                .build();
        Response response = execute(request);
        if (response.isSuccessful()) {
            assert response.body() != null;
            return response.body().string();
        } else {
            throw new Exception("postJson method fail, " + response);
        }
    }

    public static String synPostWepay(String url, Map<String, String> map) throws Throwable {
        FormBody body = null;
        body = makeFormEncodingBuilder(map).build();

        Request request = new Request.Builder().url(url).headers(new Headers.Builder()
                .add("content-area", "Merchant")
                .add("env", "production")
                .add("version", "3")
                .add("Content-Type", "application/x-www-form-urlencoded")
                .build())
                .post(body)
                .build();

        Response response = execute(request);
        if (response.isSuccessful()) {
            assert response.body() != null;
            return response.body().string();
        } else {
            throw new IOException("postForm method fail, " + response);
        }
    }

    private static FormBody.Builder makeFormEncodingBuilder(Map<String, String> map) {
        FormBody.Builder formEncodingBuilder = null;
        formEncodingBuilder = new FormBody.Builder();
        Set<Entry<String, String>> set = map.entrySet();
        if (!CollectionUtils.isEmpty(set)) {
            for (Entry<String, String> entry : set) {
                formEncodingBuilder.add(entry.getKey(), entry.getValue());
            }
        }
        return formEncodingBuilder;
    }
}