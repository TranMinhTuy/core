package utils;

import com.google.gson.Gson;
import ioc.bootstrap.configuration.ENVConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.enumeration.TypeEnum;
import utils.http.OkHttpUtils;

import java.util.concurrent.CompletableFuture;

@Component
public class RealTimeUtil {
    @Autowired
    private ENVConfig envConfig;

    public String sendRealtime(String body) {
        try {
            return OkHttpUtils.synPostJson(envConfig.getStringProperty("url_socket"), body);
        } catch (Throwable throwable) {
            return null;
        }
    }


    public String sendMessages(String body) {
        try {
            return OkHttpUtils.synPostJson(envConfig.getStringProperty("url_socket_message"), body);
        } catch (Throwable throwable) {
            return null;
        }
    }

    public String sendMessageGroup(String body) {
        try {
            return OkHttpUtils.synPostJson(envConfig.getStringProperty("url_socket_send_message_group"), body);
        } catch (Throwable throwable) {
            return null;
        }
    }


    public String sendMessageShop(String body) {
        try {
            return OkHttpUtils.synPostJson(envConfig.getStringProperty("url_socket_send_message_shop"), body);
        } catch (Throwable throwable) {
            return null;
        }
    }


    public String updateCurrentLocation(String body) {
        try {
            return OkHttpUtils.synPostJson(envConfig.getStringProperty("url_socket_user"), body);
        } catch (Throwable throwable) {
            return null;
        }
    }

    public String updateOrder(String body) {
        try {
            return OkHttpUtils.synPostJson(envConfig.getStringProperty("url_socket_update_order"), body);
        } catch (Throwable throwable) {
            return null;
        }
    }

    public String onPushRealtime(String body) {
        try {
            return OkHttpUtils.synPostJson(envConfig.getStringProperty("url_socket_on_push_realtime"), body);
        } catch (Throwable throwable) {
            return null;
        }
    }

    public void onProcessRealtime(Object body, int type) {
        try {
            CompletableFuture.runAsync(() -> {
                String json = new Gson().toJson(body);
                switch (type) {
                    case 1:
                        sendRealtime(json);
                        break;
                    case 2:
                        sendMessages(json);
                        break;
                    case 3:
                        sendMessageGroup(json);
                        break;
                    case 4:
                        sendMessageShop(json);
                        break;
                    case 5:
                        updateCurrentLocation(json);
                        break;
                    case 6:
                        updateOrder(json);
                        break;
                    case TypeEnum.ON_PUSH_REAL_TIME:
                        onPushRealtime(json);
                        break;
                }
            });

        } catch (Throwable throwable) {
        }
    }


    public String updateLocationUserVirtual(String body) {
        try {
            return OkHttpUtils.synPostJson(envConfig.getStringProperty("url_socket_user_virtual"), body);
        } catch (Throwable throwable) {
            return null;
        }
    }
}
