package utils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class JobModel {
    private Date job_date;
    private String job_data;
    private String job_class_name;
    private String job_id;
}
