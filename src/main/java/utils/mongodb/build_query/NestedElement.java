package utils.mongodb.build_query;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
public class NestedElement {
    String parent;
    String field;
}