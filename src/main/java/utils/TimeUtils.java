package utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Component
public class TimeUtils {
    private static int ZERO = 0, MAX_HOUR = 23, MAX_MIN = 59, MAX_DATE = 31, MIN_DATE = 1, MAX_MONTH = 11;
    private static String PATTERN = "dd MMM yyyy HH:mm:ss";
    private static long MILLI_SECOND_OF_DATE = 86400000;

    public static String formatTime(Long time) throws ParseException {
        DateFormat simple = new SimpleDateFormat(PATTERN);
        String dateAsString = simple.format(time);
        Date dateAsObj = simple.parse(dateAsString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateAsObj);
        int month = cal.getTime().getMonth() + 1;
        int year = cal.getTime().getYear() + 1900;
        String hours = cal.getTime().getHours() > 9 ? "" + cal.getTime().getHours() : "0" + cal.getTime().getHours();
        String minutes = cal.getTime().getMinutes() > 9 ? "" + cal.getTime().getMinutes() : "0" + cal.getTime().getMinutes();
        String date = cal.getTime().getDate() > 9 ? "" + cal.getTime().getDate() : "0" + cal.getTime().getDate();
        String monthS = month > 9 ? "" + month : "0" + month;
        return hours + ":" + minutes + " " + date + "/" + monthS + "/" + year;
    }


    public static Long getCurrentMillisecondByCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, 8);
        int current_day = calendar.get(Calendar.DAY_OF_WEEK);
        if (current_day == 1) {
            current_day = 6;
        } else {
            current_day = current_day - 2;
        }
        calendar.add(Calendar.DATE, -current_day);
        calendar.set(Calendar.HOUR, ZERO);
        calendar.set(Calendar.MINUTE, ZERO);
        calendar.set(Calendar.SECOND, ZERO);
        calendar.set(Calendar.MILLISECOND, ZERO);
        return calendar.getTimeInMillis();
    }

    public static Long addMonths(Long time, int nbMonths) throws ParseException {
        DateFormat simple = new SimpleDateFormat(PATTERN);
        String dateAsString = simple.format(time);
        Date dateAsObj = simple.parse(dateAsString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateAsObj);
        cal.add(Calendar.MONTH, nbMonths);
        return cal.getTimeInMillis();
    }


    public static Long getMillisLastMonthByTime(Long time) throws ParseException {
        DateFormat simple = new SimpleDateFormat(PATTERN);
        String dateAsString = simple.format(time);
        Date dateAsObj = simple.parse(dateAsString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateAsObj);
        if (cal.getTime().getMonth() == 0) {
            cal.set(cal.getTime().getYear() + 1899, MAX_MONTH, MIN_DATE, ZERO, 1, ZERO);
        } else {
            cal.set(cal.get(Calendar.YEAR), cal.getTime().getMonth() - 1, MIN_DATE, ZERO, ZERO, ZERO);
        }
        return cal.getTimeInMillis();
    }

    public static int getMaxDate() {
        int iYear = 2020;
        int iMonth = Calendar.FEBRUARY; // 1 (months begin with 0)
        int iDay = 1;

// Create a calendar object and set year and month
        Calendar mycal = new GregorianCalendar(iYear, iMonth, iDay);

// Get the number of days in that month
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH); // 28
        return daysInMonth;
    }

    public static int randBetween(int start, int end) {
        return start + (int) Math.round(Math.random() * (end - start));
    }


    public static Long convertStringToMilliseconds(String time, String format) {
        try {
            if (StringUtils.isAnyBlank(time)) {
                return System.currentTimeMillis();
            }
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Date date = sdf.parse(time);
            return date.getTime();
        } catch (Exception e) {

        }
        return System.currentTimeMillis();
    }


    public static Long getTimeMillisMinDayOfYear(Long timeParse) {
        try {
            DateFormat simple = new SimpleDateFormat(PATTERN);
            String dateAsString = simple.format(timeParse);
            Date dateAsObj = simple.parse(dateAsString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAsObj);
            calendar.set(calendar.get(Calendar.YEAR), ZERO, MIN_DATE, ZERO, ZERO, ZERO);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
        }
        return System.currentTimeMillis();
    }

    public static Long getTimeMillisMaxDayOfYear(Long timeParse) {
        try {
            DateFormat simple = new SimpleDateFormat(PATTERN);
            String dateAsString = simple.format(timeParse);
            Date dateAsObj = simple.parse(dateAsString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAsObj);
            calendar.set(calendar.get(Calendar.YEAR), MAX_MONTH, MAX_DATE, MAX_MIN, MAX_MIN, MAX_MIN);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
        }
        return System.currentTimeMillis();
    }

    public static Long getTimeMillisMinDayOfMonth(Long timeParse) {
        try {
            DateFormat simple = new SimpleDateFormat(PATTERN);
            String dateAsString = simple.format(timeParse);
            Date dateAsObj = simple.parse(dateAsString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAsObj);
            int min_day = calendar.getActualMinimum(Calendar.DATE);
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), min_day, ZERO, ZERO, ZERO);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
        }
        return System.currentTimeMillis();
    }

    public static Long getTimeMillisMaxDayOfMonth(Long timeParse) {
        try {
            DateFormat simple = new SimpleDateFormat(PATTERN);
            String dateAsString = simple.format(timeParse);
            Date dateAsObj = simple.parse(dateAsString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAsObj);
            int max_day = calendar.getActualMaximum(Calendar.DATE);
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), max_day, MAX_HOUR, MAX_MIN, MAX_MIN);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
        }
        return System.currentTimeMillis();
    }

    public static Long getTimeMillisMinTimeOfDate(Long timeParse) {
        try {
            DateFormat simple = new SimpleDateFormat(PATTERN);
            String dateAsString = simple.format(timeParse);
            Date dateAsObj = simple.parse(dateAsString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAsObj);
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), ZERO, ZERO, ZERO);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
        }
        return System.currentTimeMillis();
    }

    public static Long getTimeMillisMaxTimeOfDate(Long timeParse) {
        try {
            DateFormat simple = new SimpleDateFormat(PATTERN);
            String dateAsString = simple.format(timeParse);
            Date dateAsObj = simple.parse(dateAsString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAsObj);
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), MAX_HOUR, MAX_MIN, MAX_MIN);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
        }
        return System.currentTimeMillis();
    }

    public static Long getTimeMillisMinTimeOfHour(Long timeParse, int hour) {
        try {
            DateFormat simple = new SimpleDateFormat(PATTERN);
            String dateAsString = simple.format(timeParse);
            Date dateAsObj = simple.parse(dateAsString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAsObj);
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), hour, ZERO, ZERO);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
        }
        return System.currentTimeMillis();
    }

    public static Long getTimeMillisMaxTimeOfHour(Long timeParse, int hour) {
        try {
            DateFormat simple = new SimpleDateFormat(PATTERN);
            String dateAsString = simple.format(timeParse);
            Date dateAsObj = simple.parse(dateAsString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAsObj);
            calendar.set(calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH),
                    hour, MAX_MIN, MAX_MIN);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
        }
        return System.currentTimeMillis();
    }

    public static Long getTimeMillisMinTimeOfDay(Long timeParse, int day) {
        try {
            DateFormat simple = new SimpleDateFormat(PATTERN);
            String dateAsString = simple.format(timeParse);
            Date dateAsObj = simple.parse(dateAsString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAsObj);
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), day, ZERO, ZERO, ZERO);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
        }
        return System.currentTimeMillis();
    }

    public static Long getTimeMillisMaxTimeOfDay(Long timeParse, int day) {
        try {
            DateFormat simple = new SimpleDateFormat(PATTERN);
            String dateAsString = simple.format(timeParse);
            Date dateAsObj = simple.parse(dateAsString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAsObj);
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), day, MAX_HOUR, MAX_MIN, MAX_MIN);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
        }
        return System.currentTimeMillis();
    }

    public static Long getTimeMillisMaxTimeOfDay(String time) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date date = sdf.parse(time);
        long millis = date.getTime();
        try {
            DateFormat simple = new SimpleDateFormat(PATTERN);
            String dateAsString = simple.format(millis);
            Date dateAsObj = simple.parse(dateAsString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAsObj);
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), MAX_HOUR, MAX_MIN, MAX_MIN);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
        }
        return System.currentTimeMillis();
    }

    public static Long getTimeMillisMinTimeOfDay(String time) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date date = sdf.parse(time);
        long millis = date.getTime();
        try {
            DateFormat simple = new SimpleDateFormat(PATTERN);
            String dateAsString = simple.format(millis);
            Date dateAsObj = simple.parse(dateAsString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAsObj);
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), ZERO, ZERO, ZERO);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
        }
        return System.currentTimeMillis();
    }

    public static Long getTimeMillisMaxTimeOfMonth(String time) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date date = sdf.parse(time);
        long millis = date.getTime();
        try {
            DateFormat simple = new SimpleDateFormat(PATTERN);
            String dateAsString = simple.format(millis);
            Date dateAsObj = simple.parse(dateAsString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAsObj);
            int max_day = calendar.getActualMaximum(Calendar.DATE);
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), max_day, MAX_HOUR, MAX_MIN, MAX_MIN);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
        }
        return System.currentTimeMillis();
    }

    public static Long getTimeMillisMinTimeOfMonth(String time) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date date = sdf.parse(time);
        long millis = date.getTime();
        try {
            DateFormat simple = new SimpleDateFormat(PATTERN);
            String dateAsString = simple.format(millis);
            Date dateAsObj = simple.parse(dateAsString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAsObj);
            int min_day = calendar.getActualMinimum(Calendar.DATE);
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), min_day, ZERO, ZERO, ZERO);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
        }
        return System.currentTimeMillis();
    }

    public static long previousTimeApprox(Long time, int date) {
        Long currentTime = System.currentTimeMillis();
        if (time != null) {
            currentTime = time;
        }
        long millis = currentTime - date * MILLI_SECOND_OF_DATE;
        try {
            DateFormat simple = new SimpleDateFormat(PATTERN);
            String dateAsString = simple.format(millis);
            Date dateAsObj = simple.parse(dateAsString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAsObj);
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), ZERO, ZERO, ZERO);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
        }
        return System.currentTimeMillis();
    }

    public static String convertTimeToString(Long time) throws ParseException {
        DateFormat simple = new SimpleDateFormat(PATTERN);
        String dateAsString = simple.format(time);
        Date dateAsObj = simple.parse(dateAsString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateAsObj);
        int month = cal.getTime().getMonth() + 1;
        int year = cal.getTime().getYear() + 1900;
        String date = cal.getTime().getDate() > 9 ? "" + cal.getTime().getDate() : "0" + cal.getTime().getDate();
        String monthS = month > 9 ? "" + month : "0" + month;
        return year + "-" + monthS + "-" + date;
    }

    public static String getYear(Long time) throws ParseException {
        DateFormat simple = new SimpleDateFormat(PATTERN);
        String dateAsString = simple.format(time);
        Date dateAsObj = simple.parse(dateAsString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateAsObj);
        int month = cal.getTime().getMonth() + 1;
        int year = cal.getTime().getYear() + 1900;
        return year + "";
    }
}
