package factory;

public final class ConfigServletBuilder {
    String packageResource;
    int port;
    Class filterClass;

    private ConfigServletBuilder() {
    }

    public static ConfigServletBuilder configServletBuilder() {
        return new ConfigServletBuilder();
    }

    public ConfigServletBuilder withPackageResource(String packageResource) {
        this.packageResource = packageResource;
        return this;
    }

    public ConfigServletBuilder withPort(int port) {
        this.port = port;
        return this;
    }

    public ConfigServletBuilder withFilterClass(Class filterClass) {
        this.filterClass = filterClass;
        return this;
    }

    public ConfigServlet build() {
        ConfigServlet configServlet = new ConfigServlet();
        configServlet.setPackageResource(packageResource);
        configServlet.setPort(port);
        configServlet.setFilterClass(filterClass);
        return configServlet;
    }
}
