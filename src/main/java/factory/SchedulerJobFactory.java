package factory;

import org.quartz.Job;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;


@Component
public class SchedulerJobFactory implements JobFactory {

    @Autowired
    private ApplicationContext context;

    @Override
    public Job newJob(TriggerFiredBundle triggerFiredBundle, Scheduler scheduler) throws SchedulerException {
        Job job = context.getBean(triggerFiredBundle.getJobDetail().getJobClass());
        //injector.getBinding(triggerFiredBundle.getJobDetail().getJobClass()).get
        //injector.injectMembers(job);
        return job;
//        JobDetail jobDetail = triggerFiredBundle.getJobDetail();
//        jobDetail.getJobDataMap().put("Injector", injector);
//        Class jobClass = jobDetail.getJobClass();
//        Job job = (Job) injector.getInstance(jobClass);
//        injector.injectMembers(job);
//        return job;
    }
}
