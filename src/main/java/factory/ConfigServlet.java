package factory;

public class ConfigServlet {
    String packageResource;
    int port;
    Class filterClass;

    public ConfigServlet() {
    }

    public String getPackageResource() {
        return packageResource;
    }

    public void setPackageResource(String packageResource) {
        this.packageResource = packageResource;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Class getFilterClass() {
        return filterClass;
    }

    public void setFilterClass(Class filterClass) {
        this.filterClass = filterClass;
    }

}
