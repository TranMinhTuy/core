package factory.module.resource.factory;

import utils.mongodb.KijiKokoroMongoDB;

public interface PersistenceFactory {
    KijiKokoroMongoDB createMongoDB(KijiKokoroMongoDB.MongoDBConfigBuilder configBuilder);
}
