package factory.provider;

import factory.SchedulerJobFactory;
import lombok.Data;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Data

@Component
public class SchedulerProvider {
    private Scheduler scheduler;

    @Autowired
    public SchedulerProvider(SchedulerJobFactory jobFactory) {
        try {
            scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.setJobFactory(jobFactory);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}
