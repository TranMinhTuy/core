package factory.provider;

import io.vertx.core.json.JsonObject;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Data

@Component
public class JsonObjectProvider {
    private JsonObject jsonObject;

    @Autowired
    public JsonObjectProvider() {
        this.jsonObject = new JsonObject();
    }
}
