package ioc.bootstrap.servlet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import factory.provider.InetAddressProvider;
import ioc.bootstrap.configuration.ENVConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import utils.crypto.AESUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public abstract class AbstractResource {
    private static final String IV = "F27D5C9927726BCEFE7510B1BDD3D137";
    private static final String RESPONSE_SALT = "88b1e2071884c5622dedb5ef5b2c6c9575d4109c2c2e3762";

    @Autowired
    protected ENVConfig applicationConfig;
    @Autowired
    protected InetAddressProvider inetAddressProvider;
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    protected AESUtils aesUtils;

    private String refactorOutput(int code, String message, String soft_key, Object entityDTO) {
        Map<String, Object> result = new HashMap<>();
        result.put("status_code", code);
        result.put("instance_id", applicationConfig.getStringProperty("application.env", UUID.randomUUID().toString()));
        result.put("instance_name", inetAddressProvider.getInetAddress().getHostName());
        if (StringUtils.isNotBlank(message)) {
            result.put("message", message);
        }
        if (StringUtils.isNotBlank(soft_key)) {
            result.put("key_enabled", true);
            result.put("payload", this.encryptData(entityDTO, soft_key));
        } else {
            result.put("key_enabled", false);
            result.put("payload", entityDTO);
        }
        try {
            return objectMapper.writeValueAsString(result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String refactorOutputUpload(int number, String fileName, String url) {
        Map<String, Object> result = new HashMap<>();
        result.put("uploaded", number);
        result.put("fileName", fileName);
        result.put("url", url);
        try {
            return objectMapper.writeValueAsString(result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String encryptData(Object entityDTO, String soft_key) {
        try {
            return aesUtils.encrypt(RESPONSE_SALT, IV, soft_key, objectMapper.writeValueAsString(entityDTO));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }

    protected String outputJson(int code, Object entityDTO) {
        return this.refactorOutput(code, null, null, entityDTO);
    }

    protected String outputJson(int code, String message, Object entityDTO) {
        return this.refactorOutput(code, message, null, entityDTO);
    }

    protected String outputJson(int code, Object entityDTO, String soft_key) {
        return this.refactorOutput(code, null, soft_key, entityDTO);
    }

    protected String outputJson(int upload, String fileName, String url) {
        return this.refactorOutputUpload(upload, fileName, url);
    }
}
