package microservices.publisher;

import microservices.event.DomainEvent;
import microservices.event.aop.MethodArgsLoggable;
import microservices.subscriber.DomainEventSubscriber;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class DomainEventPublisher {
    private static Logger LOGGER = LogManager.getLogger();
    private Queue<DomainEvent> queue = new LinkedList<>();

    private static final ThreadLocal<DomainEventPublisher> instance = new ThreadLocal<DomainEventPublisher>() {
        protected DomainEventPublisher initialValue() {
            return new DomainEventPublisher();
        }
    };

    private List<DomainEventSubscriber<DomainEvent>> subscribers;

    public static DomainEventPublisher instance() {
        return instance.get();
    }

    /**
     * Queue an event for publication later using <code>publishAll()</code>
     * method.
     *
     * To prevent same event to be published more than one, event need
     * to implement <code>equals()</code> method for duplication checking.
     * If specified event has been published before
     * (using <code>equals()</code> method), event will be ignore.
     *
     * @param event The event to be dispatched
     */
    @MethodArgsLoggable
    public <E extends DomainEvent> void publish(final E event) {
        if (queue().contains(event)) {
            LOGGER.warn("Duplicate event detected. Ignore event {}", event);
            return;
        }
        queue().add(event);
        LOGGER.info("Event {} added to queue", event.getType());
    }
    public void reset() {
        this.setSubscribers(null);
        if (this.queue != null) {
            this.queue.clear();
        }
    }
    private DomainEventPublisher() {
        super();
        this.ensureSubscribersMap();
    }

    private void ensureSubscribersMap() {
        if (!this.hasSubscribers()) {
            this.setSubscribers(new ArrayList<DomainEventSubscriber<DomainEvent>>());
        }
    }

    private boolean hasSubscribers() {
        return this.subscribers() != null;
    }

    private List<DomainEventSubscriber<DomainEvent>> subscribers() {
        return this.subscribers;
    }

    private void setSubscribers(List<DomainEventSubscriber<DomainEvent>> subscribers) {
        this.subscribers = subscribers;
    }

    private Queue<DomainEvent> queue() {
        return this.queue;
    }
    @MethodArgsLoggable
    public void publishAll() {

        while (true) {
            DomainEvent event = queue().poll();
            if (event == null) {
                break;
            }
            if (this.hasSubscribers()) {

                LOGGER.debug("Event [{}] created", event.getClass());

                for (DomainEventSubscriber<DomainEvent> handler : subscribers) {
                    if (handler.getSubscribedToEvent() == event.getClass() || handler.getSubscribedToEvent() == DomainEvent.class) {
                        LOGGER.debug("Notifying [{}] about event [{}]", handler.getClass(), event.getClass());
                        handler.handle(event);
                    }
                }
            }
        }
    }

    public void subscribe(DomainEventSubscriber subscriber) {

        this.ensureSubscribersMap();

        LOGGER.debug("[{}] subscribes to event [{}]",
                subscriber.getClass(),
                subscriber.getSubscribedToEvent());

        if (this.subscribers == null) {
            this.subscribers = new ArrayList<>();
        }

        this.subscribers.add(subscriber);
    }
}
