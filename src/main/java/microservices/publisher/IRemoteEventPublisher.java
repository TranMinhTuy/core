package microservices.publisher;

import microservices.event.DomainEvent;

public interface IRemoteEventPublisher {
    void publish(String address, DomainEvent event) throws Throwable;
}
