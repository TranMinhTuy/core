package microservices.subscriber;

import microservices.event.DomainEvent;

public abstract class DomainEventSubscriber<E extends DomainEvent> {

    /**
     * Handle event
     * @param e Domain event to handle
     */
    public abstract void handle(E e);

    public abstract Class<E> getSubscribedToEvent();
}
