package microservices.event;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Abstract class for all domain event
 */
public abstract class DomainEvent {
    @JsonProperty
    private String type;
    @JsonProperty
    private Long occurredOn;

    public DomainEvent() {
        this.occurredOn = System.nanoTime();
        this.type = this.getClass().getCanonicalName();
        if(this.type==null){
            this.type = this.getClass().getName();
        }
    }

    abstract public String getType();

    public Long getOccurredOn() {
        return occurredOn;
    }
}
