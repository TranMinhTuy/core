package microservices.event.aop;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ReadPackageInterceptor implements MethodInterceptor {
    private static Logger LOGGER = LogManager.getLogger();

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        LOGGER.info("[KijiKokoro - PackageInterceptor] of method {}", invocation.getMethod().getName());
        if (invocation.getMethod().getParameterTypes() != null) {
            for (int i = 0; i < invocation.getMethod().getParameters().length; i++) {
                LOGGER.info(
                        "{} of {} with value {}",
                        invocation.getMethod().getParameters()[i].getName(),
                        invocation.getMethod().getParameters()[i].getParameterizedType().getTypeName(),
                        invocation.getMethod().getParameters()[i]
                );

            }
        }
        return invocation.proceed();
    }
}
