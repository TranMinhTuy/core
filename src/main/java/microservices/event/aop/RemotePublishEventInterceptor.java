package microservices.event.aop;

import microservices.event.DomainEvent;
import microservices.publisher.DomainEventPublisher;
import microservices.publisher.IRemoteEventPublisher;
import microservices.subscriber.DomainEventSubscriber;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

public class RemotePublishEventInterceptor implements MethodInterceptor {
    IRemoteEventPublisher eventPublisherProvider;
    private static Logger LOGGER = LogManager.getLogger();

    @Autowired
    public RemotePublishEventInterceptor(IRemoteEventPublisher eventPublisherProvider) {
        this.eventPublisherProvider = eventPublisherProvider;
    }

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Object object = invocation.proceed();
        String address = invocation.getStaticPart().getAnnotation(RemotePublishEvent.class).address();
        if (address == null) {
            throw new NullPointerException("Address Not Null");
        }

        DomainEventPublisher.instance().subscribe(new DomainEventSubscriber<DomainEvent>() {
            @Override
            public void handle(DomainEvent e) {
                try {
                    eventPublisherProvider.publish(address, e);
                } catch (IOException ex) {
                    LOGGER.error("Could no notify event {}", e.getClass());
                    ex.printStackTrace();
                } catch (Throwable throwable) {
                    LOGGER.error("Could no notify event {}", e.getClass());
                    throwable.printStackTrace();
                }
            }

            @Override
            public Class<DomainEvent> getSubscribedToEvent() {
                return DomainEvent.class;
            }
        });
        DomainEventPublisher.instance().publishAll();
        DomainEventPublisher.instance().reset();
        return object;
    }
}
