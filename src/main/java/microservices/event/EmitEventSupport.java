//
//package microservices.event;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import microservices.publisher.DomainEventPublisher;
//import microservices.publisher.IRemoteEventPublisher;
//import microservices.subscriber.DomainEventSubscriber;
//import org.aopalliance.intercept.MethodInterceptor;
//import org.aopalliance.intercept.MethodInvocation;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.aspectj.lang.ProceedingJoinPoint;
//
//import java.io.IOException;
//
//public class EmitEventSupport implements MethodInterceptor {
//    private static Logger LOGGER = LogManager.getLogger();
//
//    @Autowired
//    IRemoteEventPublisher remoteEventPublisher;
//
//    public Object invoke(String address,ProceedingJoinPoint joinPoint) throws Throwable {
//        DomainEventPublisher.instance().reset();
//        DomainEventPublisher.instance().subscribe(new DomainEventSubscriber<DomainEvent>() {
//            @Override
//            public void handle(DomainEvent e) {
//                try {
//                    remoteEventPublisher.publish(address, e);
//                } catch (IOException ex) {
//                    LOGGER.error("Could no notify event {}", e.getClass());
//                } catch (Throwable throwable) {
//                    throwable.printStackTrace();
//                }
//            }
//
//            @Override
//            public Class<DomainEvent> getSubscribedToEvent() {
//                return DomainEvent.class;
//            }
//        });
//
//        Object result =  joinPoint.proceed();
//
//        DomainEventPublisher.instance().publishAll();
//
//        return result;
//    }
//
//    @Override
//    public Object invoke(MethodInvocation invocation) throws Throwable {
//        Object result = invocation.proceed();
//        return result;
//    }
//}
